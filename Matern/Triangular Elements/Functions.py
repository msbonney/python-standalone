def Import_Generic(filename,ftype,ex=[]):
    """
    This function is a generic call to import text files into python. Various
    ftypes are used to identify the formatting.
    
    Created by Matthew Bonney for MARQUESS GOEE - University of Nottingham
    Initially created on: 17/12/2019
    
    Edited on: 11/5/2020 by: Matthew Bonney
        Adjusted comments for publication
        
    =================================Input=====================================
    filename    = A string to denote .mtx file location in LABEL format
    ftype       = A string to denote the type of file use
    =================================Output====================================
        There are various outputs based on the ftype
    (K,DOF)     = For option 'k', global stiffness matrix and DOF list
    (K,DOF)     = For option 'ke', A list of element stiffness matrixes and 
                    a list of DOF list
    (X,Y,Z,E,G) = For option 'gcoord', Arrays of location of Guass integration 
                    points and identifiers
    (X,Y,Z,N)   = For option 'ncoord', Locations and identifier of nodes
    (F/U,DOF)   = For options 'u' or 'f', Force or Displacement vector with DOF
    (J,E,G)     = For option 'j', list of jacobian matrices evaluated at 
                    Gauss integration locations and identifiers
    (B,E,G)     = For option 'b', list of strain-displacement matrices evaluated
                    at Gauss integration locations and identifiers
    (E,N)       = For option 'dof', list of nodes for each elements
    (T,E)       = For option 'r', list of local to global transformation 
                    matrices for each element
    """
    import numpy as np, scipy.sparse as sparse
    # Import Data File
    rd=np.genfromtxt(filename,delimiter=',',comments='#')
    flag=True
    # Determine type
    if ftype=='k' or ftype=='K': # Global Stiffness
        flag=False
        (K,DOF)=Import_GlobalMatrix(filename)
    elif ftype=='ke' or ftype=='KE' or ftype=='Ke' or ftype=='kE': # Element Stiffness
        flag=False
        (K,DOF)=Import_EBEMatrix(filename)
    elif ftype=='GCOORD' or ftype=='gcoord': # Gauss Integration Locations
        (X,Y,Z,ELE,GP)=Import_GCOORDS(filename)
        return((X,Y,Z,ELE,GP))
    elif ftype=='NCOORD' or ftype=='ncoord': # Nodal Locations
        node=rd[:,0]
        X,Y,Z=rd[:,1],rd[:,2],rd[:,3]
        return((X,Y,Z,node))
    elif ftype=='u' or ftype=='U' or ftype=='f' or ftype=='F': # Displacement or Force Vector
        flag=False
        n,dn,K=rd[:,2],rd[:,3],sparse.csr_matrix(rd[:,4]).T
        DOF=n+dn/10
    elif ftype=='j' or ftype=='J': # Jacobian Matrices
        ele,gp,J=rd[:,2],rd[:,3],rd[:,4]
        return((J,ele,gp))
    elif ftype=='b' or ftype=='B': # Strain-Displacement Matrices
        (B,ELE,GP)=Import_BMatrix(filename,ex)
        return((B,ELE,GP))
    elif ftype=='DOF' or ftype=='dof': # Element Connectivity
        ELE=rd[:,0]
        nodes=rd[:,1:]
        return((ELE,nodes))
    elif ftype=='gclabel' or ftype=='GCLABEL':
        (X,Y,Z,ELE,GP)=Import_GCOORDS_Label(filename)
        return((X,Y,Z,ELE,GP))
    elif ftype=='r' or ftype=='R': # Local Element Rotation Matrices
        (T,ELE)=Import_rotation(filename)
        return((T,ELE))
    else:
        raise Exception('Unknown File type (ftype)')
    if flag==False:
        return((K,DOF))
def Import_GCOORDS(filename):
    """
    This subfunction reads in the Gauss Coordinate locations
    
    Created by Matthew Bonney for MARQUESS GOEE - University of Nottingham
    Initially created on: 17/12/2019
    
    Edited on: 11/5/2020 by: Matthew Bonney
        Adjusted comments for publication
    """
    import numpy as np
    rd=np.genfromtxt(filename,delimiter=',',comments='#')
    ele,gp,DOF,val=rd[:,2],rd[:,3],rd[:,4],rd[:,5]
    Xint=np.where(DOF==1)[0]
    Yint=np.where(DOF==2)[0]
    Zint=np.where(DOF==3)[0]
    
    ELE,GP=ele[Xint],gp[Xint]
    X,Y,Z=val[Xint],val[Yint],val[Zint]
    return((X,Y,Z,ELE,GP))
def Import_GCOORDS_Label(filename):
    """
    This subfunction reads in the Gauss Coordinate locations
    
    Created by Matthew Bonney for MARQUESS GOEE - University of Nottingham
    Initially created on: 17/12/2019
    
    Edited on: 11/5/2020 by: Matthew Bonney
        Adjusted comments for publication
    Edited on: 20/08/2020 by: Matthew Bonney
        Added for Label format with X,Y,Z on a single line
    """
    import numpy as np
    rd=np.genfromtxt(filename,delimiter=',',comments='#')
    ELE,GP,X,Y,Z=rd[:,0],rd[:,1],rd[:,2],rd[:,3],rd[:,4]
    return((X,Y,Z,ELE,GP))
def Import_rotation(filename):
    """
    This subfunction reads in rotation matrices for each element
    
    Created by Matthew Bonney for MARQUESS GOEE - University of Nottingham
    Initially created on: 17/12/2019
    
    Edited on: 11/5/2020 by: Matthew Bonney
        Adjusted comments for publication
    """
    import numpy as np, scipy.sparse as sparse
    # Import Data File
    rd=np.genfromtxt(filename,delimiter=',',comments='#')
    ele,row,col,val=rd[:,2],rd[:,3]-1,rd[:,4]-1,rd[:,5]
    ELE=np.unique(ele)
    T=[]
    for i in range(len(ELE)):
        ind=np.where(ele==ELE[i])[0]
        T.append(sparse.csr_matrix((val[ind],(row[ind],col[ind])),shape=(3,3)).todense())
    return(T,ELE)
def Import_BMatrix(filename,ex):
    """
    This subfunction reads in the strain-displacement matrix for each Gauss 
    point location for each element
    
    Created by Matthew Bonney for MARQUESS GOEE - University of Nottingham
    Initially created on: 17/12/2019
    
    Edited on: 11/5/2020 by: Matthew Bonney
        Adjusted comments for publication
    """
    import numpy as np, scipy.sparse as sparse
    # Import Data File
    if ex==[]:
        NpE=4
    else:
        NpE=int(ex)
    rd=np.genfromtxt(filename,delimiter=',',comments='#')
    ele,gp,val=rd[:,2],rd[:,3],rd[:,-1]
    row,col=np.array(rd[:,4],dtype=int)-1,np.array(rd[:,5],dtype=int)-1
    Nrow=int(np.max(row)+1)
    Nele,Ng=int(np.max(ele)),int(np.max(gp))
    B,ELE,GP=[],np.zeros((Nele,Ng)),np.zeros((Nele,Ng))
    for i in range(Nele):
        B.append([])
        for j in range(Ng):
            ind=np.where((ele*(gp==j+1)==i+1)==True)[0]
            B[i].append(sparse.csr_matrix((val[ind],(row[ind],col[ind])),shape=(Nrow,6*NpE)))
            ELE[i,j]=i+1
            GP[i,j]=j+1
    return(B,ELE,GP)
def Import_GlobalMatrix(filename):
    """
    This function takes in the .mtx file from ABAQUS, formated in the LABEL
    method and generates the CSR sparse matrix version of the matrix. This 
    should work with both global and element by element formulations.
    
    Created by Matthew Bonney for MARQUESS GOEE - University of Nottingham
    Initially created on: 06/11/2019
    
    
    Edited on: 11/5/2020 by: Matthew Bonney
        Adjusted comments for publication
        
    =================================Input=====================================
    filename    = A string to denote .mtx file location in LABEL format
    =================================Output====================================
    K           = A matrix, can be mass or stiffness. Either global or element
                        by element formulation
    DOF         = A DOF vector that coincides with the matrix K
    """
    import numpy as np, scipy.sparse as sparse
    # Import Data File
    rd=np.genfromtxt(filename,delimiter=',',comments='#')
    # Separate into arrays
    if rd.shape[1]==8:
        val=rd[:,7]
        n1,dn1=rd[:,3],rd[:,4]
        n2,dn2=rd[:,5],rd[:,6]
    elif rd.shape[1]==7:
        val=rd[:,6]
        n1,dn1=rd[:,2],rd[:,3]
        n2,dn2=rd[:,4],rd[:,5]
    else:
        raise Exception('ERROR: Error in reading .mtx file')
    # Create DOF arrays
    DOF1=n1+dn1/10
    DOF2=n2+dn2/10
    # Get sorting arrays
    nDOF=int(max(np.max(n1),np.max(n2)))
    a=(np.arange(6*nDOF))%6+1.
    DOF=np.sort((np.arange(nDOF)+1.).tolist()*6)+a/10
    # Construct Global Matrix
    row,col=np.zeros(len(n1),dtype=int),np.zeros(len(n1),dtype=int)
    for i in range(len(n1)):
        row[i]=np.where(np.isclose(DOF,DOF1[i]))[0]
        col[i]=np.where(np.isclose(DOF,DOF2[i]))[0]
    K=sparse.csr_matrix((val,(row,col)),shape=(6*nDOF,6*nDOF))
    K=K+K.T-sparse.diags(K.diagonal(),0)
    DOF=DOF[K.getnnz(1)>0]
    K=K[K.getnnz(1)>0][:,K.getnnz(0)>0]
    return((K,DOF))
def Import_EBEMatrix(filename):
    """
    This function takes in the .mtx file from ABAQUS, formated in the LABEL
    method and generates the CSR sparse matrix version of the matrix for each
    element. Also creates a DOF list that can be used for global assembly. 
    
    Created by Matthew Bonney for MARQUESS GOEE - University of Nottingham
    Initially created on: 12/11/2019
    
    
    Edited on: 11/5/2020 by: Matthew Bonney
        Adjusted comments for publication
        
    =================================Input=====================================
    filename    = A string to denote .mtx file location in LABEL format
    =================================Output====================================
    Klist       = A list of sparse matrices with length being the number of 
                    elements
    gDOF        = A list of DOF arrays for each element
    """
    import numpy as np, scipy.sparse as sparse
    # Import Data File
    rd=np.genfromtxt(filename,delimiter=',',comments='#')
    # Separate into arrays
    ele,val=rd[:,2],rd[:,7]
    n1,dn1=rd[:,3],rd[:,4]
    n2,dn2=rd[:,5],rd[:,6]
    # Create DOF arrays
    Nele=int(np.max(ele))
    DOF1=n1+dn1/10
    DOF2=n2+dn2/10
    # Get sorting arrays
    dpn=int(max(np.max(dn1),np.max(dn2)))
    Klist,gDOF=[None]*Nele,[None]*Nele
    # Construct list of matrices
    for i in range(Nele):
        ind=np.where(ele==i+1)[0]
        gDOF[i]=np.unique(DOF1[ind].tolist()+DOF2[ind].tolist())
        ne=len(n1[ind])
        row,col=np.zeros(ne,dtype=int),np.zeros(ne,dtype=int)
        for j in range(ne):
            row[j]=np.where(DOF1[ind[j]]==gDOF[i])[0]
            col[j]=np.where(DOF2[ind[j]]==gDOF[i])[0]
        K=sparse.csr_matrix((val[ind],(row,col)),shape=(dpn*4,dpn*4))
        Klist[i]=K+K.T-sparse.diags(K.diagonal(),0)
    return((Klist,gDOF))
def quad_2Dlocation(n):
    """ 
    This function returns the locations and weights for 2D quasian quadrature.
    This uses natural coordinates where range = [-1,1]
    
    Written by Matthew Bonney for MARQUESS GOEE - University of Nottingham
    Initially created on: 24/09/2019
    
    Edited on: 11/5/2020 by: Matthew Bonney
        Adjusted comments for publication
        
    =================================Input=====================================
    n       = The integration order (only 0-2 are programmed). Future work to 
                    make this for any arbitrary order (outside current scope)
    =================================Output====================================
    xi_i    = An array of the natural 'X' locations for integration
    eta_i   = An array of the natural 'Y' locations for integration
    W       = An array of the weights for each location pair
    """
    import numpy as np
    # Switch case for first 3 orders
    if n==0: # 1 integration point
        xi_i=np.array([0])
        eta_i=np.array([0])
        W=np.array([4])
    elif n==1: # 4 integration points
        xi_i=np.sqrt(3)/3*np.array([-1,1,-1,1])
        eta_i=np.sqrt(3)/3*np.array([-1,-1,1,1])
        W=np.array([1,1,1,1])
    elif n==2: # 9 integration points
        xi_i=np.sqrt(.6)*np.array([-1,1,1,-1,0,1,0,-1,0])
        eta_i=np.sqrt(.6)*np.array([-1,-1,1,1,-1,0,1,0,0])
        W=25/81.*np.array([1,1,1,1,0,0,0,0,0])+40/81.*np.array([0,0,0,0,1,1,1,1,0])+64/81.*np.array([0,0,0,0,0,0,0,0,1])
    else:
        raise Exception('Order is not programmed')
        return()
    return((xi_i,eta_i,W))
    
def tri_2Dlocation(n):
    """
    
    
    """
    import numpy as np
    if n==0: # 1 Integration point
        xi_i=np.array([1./3])
        eta_i=np.array([1./3])
        W=np.array([1./2])
    elif n==1: # 3 Intergration Points
        xi_i=np.array([.5,0.,.5])
        eta_i=np.array([.5,.5,0.])
        W=np.array([1./6,1./6,1./6])
    else:
        raise Exception('Order is not programmed')
        return()
    return((xi_i,eta_i,W))
    
def ZZ_NN3D(Xt,Yt,Zt,Node,Xi,Yi,Zi,E,Ele,nDOF,ind_ele,EleName):
    """
    This function is a generic ZZ recovery method for 1+ quantity of interest. 
    This investigates the closest integration points of the adjoined elements
    and does a length based average for those points. Only takes 1 integration
    point per element.
    
    Note: There are 2 separate naming list that must becarefully input and be 
    consistant. It is easily possible that Node is the same as nDOF (Nn==Nnode)
    
    Created by Matthew Bonney for MARQUESS GOEE - University of Nottingham
    Initially created on: 09/10/2019
    
    Edited on: 18/11/2019  by: Matthew Bonney
    Edited on: 4/12/2019  by: Matthew Bonney to include multi-E
    Edited on: 11/5/2020 by: Matthew Bonney
        Adjusted comments for publication
    =================================Input=====================================
    Xt      = Target X locations (nodes) [Nn,]
    Yt      = Target Y locations (nodes) [Nn,]
    Zt      = Target Z locations (nodes) [Nn,]
    Node    = Node numbering for locations [Nn,]
    Xi      = Integration X locations [Nint,]
    Yi      = Integration Y locations [Nint,]
    Zi      = Integration Z locations [Nint,]
    E       = Integration Values [Nint,Q]
    Ele     = Element numbering for each Integration location [Nint,]
    nDOF    = Full node numbering [Nnode,]
    ind_ele = List of list of indexes for each element: len=Nele
    EleName = Element number for each element [Nele,]
    =================================Output====================================
    ZZ      = The smoothed ZZ recovered values   
    """
    import numpy as np
    Nn,Nint,Nele=len(Xt),len(Xi),len(ind_ele)
    if np.ndim(E)==1:
        ZZ=np.zeros(Nn)
        Nshape=0
    else:
        ZZ=np.zeros((Nn,E.shape[1]))
        Nshape=E.shape[1]
    # Sweep through target locations
    for i in range(Nn):
        x,y,z,n=Xt[i],Yt[i],Zt[i],Node[i]
        # Find adjunct elements
        el_list=[] 
        for j in range(Nele):
            if nDOF[ind_ele[j]].tolist().count(n): # Connected element
                el_list.append(j)
        # Find integration points in each element
        int_list=[]
        for j in range(Nint):
            for k in range(len(el_list)):
                if Ele[j]==EleName[el_list[k]]:
                    int_list.append(j)
        # Reduce arrays
        Xi_r,Yi_r,Zi_r=Xi[int_list],Yi[int_list],Zi[int_list]
        Ele_r=Ele[int_list]
        # Find closest integration point in each element
        ind_i,L_i=[],[]
        for j in range(len(el_list)):
            L,ele=np.inf,EleName[el_list[j]]
            # Find integration points in element
            a=np.where(Ele_r==ele)[0]
            # Find Length
            ind=[]
            for k in range(len(a)):
                Li=np.sqrt((Xi_r[a[k]]-x)**2+(Yi_r[a[k]]-y)**2+(Zi_r[a[k]]-z)**2)
                if Li<L:
                    L=Li
                    ind=k
            ind_i.append(a[ind])
            L_i.append(L)
        # Length average values
        if Nshape==0:
            E_r=E[int_list]
            ZZ[i]=np.sum(E_r[ind_i]*L_i)/np.sum(L_i)
        else:
            E_r=E[int_list,:]
            for k in range(E.shape[1]):
                ZZ[i,k]=np.sum(E_r[ind_i,k]*L_i)/np.sum(L_i)
    return(ZZ)
def ZZ_Gauss3D(Xt,Yt,Zt,Xi,Yi,Zi,E,Sigx,Sigy,Sigz,Lcut=[]):
    """
    This function performs the generic ZZ recovery that is independent of 
    elements. This takes in the integration points and applies a Gaussian
    distribution based on the Euclidean length. There is an additional
    feature of setting a maximum distance to count in the weighted summation.
    
    Note: This is not used in the example system, but can be used. Has issues
    with plane transitions (ex. T intersection as in the example system).
    
    Created by Matthew Bonney for MARQUESS GOEE - University of Nottingham
    Initially created on: 09/10/2019
    
    Edited on: 17/12/2019   by: Matthew Bonney, including multi-E
    Edited on: 11/5/2020 by: Matthew Bonney
        Adjusted comments for publication
        
    =================================Input=====================================
    Xt      = Target X locations (nodes) [Nn,]
    Yt      = Target Y locations (nodes) [Nn,]
    Zt      = Target Z locations (nodes) [Nn,]
    Xi      = Integration X locations [Nint,]
    Yi      = Integration Y locations [Nint,]
    Zi      = Integration Z locations [Nint,]
    E       = Integration Values [Nint,Q]
    Sigx    = X direction standard deviation
    Sigy    = Y direction standard deviation
    Sigz    = Z direction standard deviation
    Lcut    = Optional: maximum distance accounted for in summation
    =================================Output====================================
    ZZ      = The smoothed ZZ recovered values   
    """
    import numpy as np
    # Gaussian function
    W=lambda X,Mu,Sig: np.exp(-1/2*(X-Mu).T*np.linalg.inv(Sig)*(X-Mu))
    Nt,Sig=len(Xt),np.matrix([[Sigx**2,0,0],[0,Sigy**2,0],[0,0,Sigz**2]])
    if np.ndim(E)==1:
        ZZ=np.zeros(Nt)
        Nshape=0
    else:
        ZZ=np.zeros((Nt,E.shape[1]))
        Nshape=E.shape[1]    
    # Arange center-points
    Mu=np.array([Xi,Yi,Zi])
    # Set cut-off distance to inf if none
    if Lcut==[]:
        Lcut=np.inf
    for i in range(Nt):
        # Set location
        X=np.array([[Xt[i]],[Yt[i]],[Zt[i]]])
        # Grabs only diagional weight terms since they are the only physical
        Wx=np.array(W(X,Mu,Sig).diagonal())[0]
        # Calculate Euclidean distance
        Lx=np.linalg.norm(X-Mu,axis=0)
        # Only take points within cut-off distance
        Wx=Wx*(Lx<Lcut)
        # Weighted Mean
        if Nshape==0:
            ZZ[i]=np.sum(E*(Wx/np.sum(Wx)))
        else:
            for k in range(Nshape):
                ZZ[i,k]=np.sum(E[:,k]*(Wx/np.sum(Wx)))
    return(ZZ) 