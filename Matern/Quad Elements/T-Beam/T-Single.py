import numpy as np,matplotlib.pyplot as plt, scipy.sparse as sparse,time
import Functions as FUN,mpl_toolkits.mplot3d
from scipy.sparse.linalg import inv as spinv
"""
This script is an example run script for the Python Standlone of the MARQUESS
GOEE workflow. The system is a general system that represents a simplified
stiffener panel with a T cross-section. Several feature models are generated to
use. The location of this feature is pre-programmed for either 100mm or 400mm
from the loading location. To place in any other location, denote the order of
the pre-programmed location and create new list of nodes that correspond to the
same order, but at different location. DOF vectors are based on Patran ordering.
Each DOF is assigned Node#.dof# (example 102.2) where the dof# is:
1-Displacement X, 2-Displacement Y, 3-Displacement Z, 4-Rotation X, 5-Rotation
Y, 6-Rotation Z. It is assumed that all 6 directions are used.

Most modifications are done in this section. Any modifications furture alone in
the scripts. Also for a new system or BC, change "Solve Dual Problems" and 
"Gaussian Process for GOEE" sections with updated BC.

    Created by Matthew Bonney for MARQUESS GOEE - University of Nottingham
    Ceated on: 09/10/2019
    
    Updated Testing for Matern Prior. Replaced the Squared Exponential prior 
    with a Matern prior to better incorporate physical connectivity.
    Updated on: 25/06/2020 by Matthew Bonney
"""
# Linear Elastic Material Properties
E=73.1e9
nu=0.33
t=0.001
# Locations of Dual centers
Points='Feature' 
# If Points=='Det', Modify Line 152 with XYZ Center (Or find "if Points=='Det':")
NQ=61 # For Rand Max=1299 for node, 1250 for ele. Doesn't matter for Det
L_QOI=.01 # Length hyper-parameter for Guassian spacial distribution for integration
Lcut=np.inf # Set max length of spacial distribution, leave inf to assign weight to every integration point. Used for numerical condition issues
NpE=4
Nmc=10 # Number of samples from GP distribution
# Select Feature Model: Nominal, Large, Slot, SingleHole, TripleHole
Feature='SingleHole'  
# File Locations for Primal Solution
Stiff='DAT Files/ELEM_K.dat'
Mass='DAT Files/ELEM_M.dat'
Element='DAT Files/ELEM_CONNECT.dat'
GCOORD='DAT Files/GAUSS_COORDS.dat'
Ufile='DAT Files/U.dat'
NCOORD='DAT Files/NODE_COORDS.dat'
Bmat='DAT Files/B.dat'
Jaco='DAT Files/ELEM_JAC.dat'
Rot='DAT Files/ELEM_ROT.dat'
#%% Primal Solutions
(Kp,DOFk)=FUN.Import_Generic(Stiff,'k')
(Mp,DOFm)=FUN.Import_Generic(Mass,'k')
#DOFm,Mp=DOFk.copy(),sparse.csr_matrix(np.eye(len(DOFk)))
(U,DOFu)=FUN.Import_Generic(Ufile,'u')
(T,ELE_Rot)=FUN.Import_Generic(Rot,'r')
(Xn,Yn,Zn,Noden)=FUN.Import_Generic(NCOORD,'ncoord')
# Setup Interpolation functions
N1=lambda xi,eta: .25*(1-xi)*(1-eta)
N2=lambda xi,eta: .25*(1+xi)*(1-eta)
N3=lambda xi,eta: .25*(1+xi)*(1+eta)
N4=lambda xi,eta: .25*(1-xi)*(1+eta)
N=lambda xi,eta: np.matrix([N1(xi,eta),N2(xi,eta),N3(xi,eta),N4(xi,eta)])
# Setup Material Information
a=E/(1-nu**2)
Cmemb=a*np.matrix([[1,nu,0],[nu,1,0],[0,0,(1-nu)/2]])
Cbend=t**2/12*Cmemb
Csher=5/6.*a*np.matrix([[(1-nu)/2,0],[0,(1-nu)/2]])
C=np.matrix(np.zeros((6,6)))
Bend_dof=[0,1,5]
Sher_dof=[3,4]
for i in range(3):
    for j in range(3):
        C[Bend_dof[i],Bend_dof[j]]+=Cmemb[i,j]+Cbend[i,j]
for i in range(2):
    for j in range(2):
        C[Sher_dof[i],Sher_dof[j]]+=Csher[i,j]
# Settup global DOF vector
gDOF=np.unique(DOFk.tolist()+DOFu.tolist())
# Ensure all 6 DOF are used in calculation
test=gDOF-np.array(gDOF,dtype=int)
if len(test)%6==0:
    a=test.reshape((-1,6))
    b=np.zeros(len(test)/6-1)
    for i in range(a.shape[0]-1):
        b[i]=np.allclose(a[i],a[i+1])
    if not np.all(b):
        a,b=np.max(gDOF)-np.max(gDOF)%1,np.min(gDOF)-np.min(gDOF)%1
        c,d=np.arange(b,a+1),np.arange(1,7)
        gDOF=np.sort(np.array(c.tolist()*len(d)))+np.array(d.tolist()*len(c))/10.
else:
    a,b=np.max(gDOF)-np.max(gDOF)%1,np.min(gDOF)-np.min(gDOF)%1
    c,d=np.arange(b,a+1),np.arange(1,7)
    gDOF=np.sort(np.array(c.tolist()*len(d)))+np.array(d.tolist()*len(c))/10.
nDOF=len(gDOF)
# Ensure sizes are correct
if not np.all(np.in1d(gDOF,DOFu)): # Displacement Vector
    nnz=U.nnz
    row=np.zeros(nnz,dtype=int)
    for i in range(nnz):
        row[i]=np.where(gDOF==DOFu[i])[0]
    Uh=sparse.csr_matrix((np.array(U[U.nonzero()]).flatten(),(row,np.zeros(nnz,dtype=int))),shape=(nDOF,1))
else:
    Uh=U
if not np.all(np.in1d(gDOF,DOFk)): # Stiffness Matrix
    nnz=Kp.nnz
    (row0,col0)=Kp.nonzero()
    row,col=np.zeros(nnz,dtype=int),np.zeros(nnz,dtype=int)
    for i in range(nnz):
        row[i]=np.where(gDOF==DOFk[row0[i]])[0]
        col[i]=np.where(gDOF==DOFk[col0[i]])[0]
    K=sparse.csr_matrix((np.array(Kp[Kp.nonzero()]).flatten(),(row,col)),shape=(nDOF,nDOF))
else:
    K=Kp
if not np.all(np.in1d(gDOF,DOFm)): # Stiffness Matrix
    nnz=Mp.nnz
    (row0,col0)=Mp.nonzero()
    row,col=np.zeros(nnz,dtype=int),np.zeros(nnz,dtype=int)
    for i in range(nnz):
        row[i]=np.where(gDOF==DOFm[row0[i]])[0]
        col[i]=np.where(gDOF==DOFm[col0[i]])[0]
    M=sparse.csr_matrix((np.array(Mp[Mp.nonzero()]).flatten(),(row,col)),shape=(nDOF,nDOF))
else:
    M=Mp
Uz=Uh[np.where(np.isclose(gDOF%1,0.3))[0]]
# Import Integration values/ B matrix
(B,ELE,GP)=FUN.Import_Generic(Bmat,'b',NpE)
(J,ELEj,GPj)=FUN.Import_Generic(Jaco,'j')
(Nele,NGP)=GP.shape

# Rotate B matrix to global CS
O=np.matrix(np.zeros((3,3)))
for i in range(Nele):
    Tf=np.concatenate((np.concatenate((T[i],O),axis=0),np.concatenate((O,T[i]),axis=0)),axis=1)
    # Bending
    indx,indy=np.where(ELE==ELE_Rot[i])
    for j in range(len(indy)):
        B[indx[0]][indy[j]]=sparse.csr_matrix(Tf*B[indx[0]][indy[j]],shape=(6,6*NpE))

# Calculate ehat
(ELE_dof,Node_dof)=FUN.Import_Generic(Element,'dof')
Nodal_DOF=np.unique(np.array(gDOF,dtype=int))
ehat,ind_ele=[],[]
for i in range(Nele):
    a=np.matrix(np.zeros((6*NpE,nDOF)))
    ind_ele.append([])
    for j in range(NGP):
        ind=np.where(np.array(gDOF,dtype=int)==Node_dof[i][j])[0]
        ind_ele[i].append(int(np.where(Nodal_DOF==Node_dof[i][j])[0]))
        for k in range(6):
            a[6*j+k,ind[k]]=1.
    ehat.append(sparse.csr_matrix(a))
# Find Centroids
Xc,Yc,Zc=np.zeros(Nele),np.zeros(Nele),np.zeros(Nele)
for i in range(Nele):
    Xc[i],Yc[i],Zc[i]=np.mean(Xn[ind_ele[i]]),np.mean(Yn[ind_ele[i]]),np.mean(Zn[ind_ele[i]])
# Setup Dual Locations
if Points=='Feature':
#    ind_nq=np.where((Xn>=0.4)*(Xn<=0.44))[0]
    ind_nq=[550,601,652,703,754,805,856,907,958,1009,1060,499,448,397,346,295,244,193,142,91,40,1111,1162,1213,1264,1315,1061,41,1316,1062,42,1317,1063,43,1318,554,605,656,707,758,809,860,911,962,1013,1064,503,452,401,350,299,248,197,146,95,44,1115,1166,1217,1268,1319]
#    ind_QOI=np.random.choice(len(ind_nq),size=(NQ),replace=False)
    ind_QOI=np.arange(0,len(ind_nq))
    QOI_Center=[]
    for i in range(NQ):
        QOI_Center.append([Xn[ind_nq[ind_QOI[i]]],Yn[ind_nq[ind_QOI[i]]],Zn[ind_nq[ind_QOI[i]]]])
elif Points=='Node':
    ind=np.where(Xn!=0.5)[0]
    xr,yr,zr=Xn[ind],Yn[ind],Zn[ind]
    ind_QOI=np.random.choice(len(xr),size=(NQ),replace=False)
    QOI_Center=(np.array([xr[ind_QOI],yr[ind_QOI],zr[ind_QOI]]).T).tolist()
    NQ=len(QOI_Center)
#%% Post-Process Primal Problem
# Calculate strains
ep_u=[]
for i in range(Nele):
    ep_u.append([])
    for j in range(NGP):
        (indx,indy)=np.where((GP==j+1)*(ELE==i+1))
        ep_u[i].append((B[indx[0]][indy[0]]*ehat[indx[0]]*Uh).todense())

# Separate into individual arrays
(X,Y,Z,ELE_ar,GP_ar)=FUN.Import_Generic(GCOORD,'gclabel')     
Ngpt=len(ELE_ar)
EB1,EB2,EB3=np.zeros((Ngpt,1)),np.zeros((Ngpt,1)),np.zeros((Ngpt,1))
EB4,EB5,EB6=np.zeros((Ngpt,1)),np.zeros((Ngpt,1)),np.zeros((Ngpt,1))

for i in range(Ngpt):
    (indx,indy)=np.where((ELE==ELE_ar[i])*(GP==GP_ar[i]))
    EB1[i],EB2[i],EB3[i]=ep_u[indx[0]][indy[0]][0],ep_u[indx[0]][indy[0]][1],ep_u[indx[0]][indy[0]][2]
    EB4[i],EB5[i],EB6[i]=ep_u[indx[0]][indy[0]][3],ep_u[indx[0]][indy[0]][4],ep_u[indx[0]][indy[0]][5]

ET=np.concatenate((EB1,EB2,EB3,EB4,EB5,EB6),axis=1)    
# Perform ZZ recovery (Currently NN approach)
Nnode=len(Noden)
Nodal_DOF=np.unique(np.array(gDOF,dtype=int))

A=FUN.ZZ_NN3D(Xn,Yn,Zn,Noden,X,Y,Z,ET,ELE_ar,Nodal_DOF,ind_ele,ELE[:,0].reshape(-1))
EB1zz,EB2zz,EB3zz=A[:,0].reshape(-1),A[:,1].reshape(-1),A[:,2].reshape(-1)
EB4zz,EB5zz,EB6zz=A[:,3].reshape(-1),A[:,4].reshape(-1),A[:,5].reshape(-1)

print('End of Primal')
VonMises=lambda s11,s22,s33,s23,s13,s12: np.sqrt(.5*((s11-s22)**2+(s22-s33)**2+(s33-s11)**2+6*(s23**2+s13**2+s12**2)))
#%% Setup Dual Problem/ Calculate Q vector
SE=lambda x,y,z,i: np.exp(-((x-QOI_Center[i][0])**2+(y-QOI_Center[i][1])**2+(z-QOI_Center[i][2])**2)/(2*L_QOI**2))
Dist=lambda x,y,z,i: np.sqrt((x-QOI_Center[i][0])**2+(y-QOI_Center[i][1])**2+(z-QOI_Center[i][2])**2)
Equl=lambda x,y,z,i: 1.
# Calculate summation weighting
Hat=[]
H=np.matrix(np.zeros((6*NQ,nDOF)))
for k in range(NQ):
    Xint,Yint,Zint,What=np.zeros((Nele,NGP)),np.zeros((Nele,NGP)),np.zeros((Nele,NGP)),np.zeros((Nele,NGP))
    # Determine Nodal weights
    Wn=np.zeros(Nnode)
    for i in range(Nnode):
        l=Dist(Xn[i],Yn[i],Zn[i],k)
        if l<Lcut:
            Wn[i]=SE(Xn[i],Yn[i],Zn[i],k)
    # Interpolate
    (xi_i,eta_i,Wi)=FUN.quad_2Dlocation(1)
    for i in range(Nele):
        for j in range(NGP):
            Xint[i,j]=N(xi_i[j],eta_i[j])*np.matrix(Xn[ind_ele[i]]).T
            Yint[i,j]=N(xi_i[j],eta_i[j])*np.matrix(Yn[ind_ele[i]]).T
            Zint[i,j]=N(xi_i[j],eta_i[j])*np.matrix(Zn[ind_ele[i]]).T
            What[i,j]=N(xi_i[j],eta_i[j])*np.matrix(Wn[ind_ele[i]]).T
    What=What/np.sum(What)
    Hat.append(What)
    for p in range(6):
    # Calculate Q vector
        Q_ele,Ar=[],np.zeros((Nele,NGP))
        for i in range(Nele):
            Q_ele.append(np.matrix(np.zeros((1,nDOF))))
            for j in range(NGP):
                indj=np.where((GPj==j+1)*(ELEj==i+1))[0][0]
                Ar[i,j]=J[indj]*Wi[j]*(What[i,j]!=0)
                N1x,N2x=np.matrix(N1(xi_i[j],eta_i[j])*np.eye(6)),np.matrix(N2(xi_i[j],eta_i[j])*np.eye(6))
                N3x,N4x=np.matrix(N3(xi_i[j],eta_i[j])*np.eye(6)),np.matrix(N4(xi_i[j],eta_i[j])*np.eye(6))
                Nt=np.concatenate((N1x,N2x,N3x,N4x),axis=1)
                Q_ele[i]=Q_ele[i]+Nt[p,:]*ehat[i]*Ar[i,j]*What[i,j]
        # Assemble 
        Q=np.matrix(np.sum(np.array(Q_ele),axis=0))
        H[NQ*p+k,:]=Q/(np.sum(Ar))*len(np.nonzero(What)[0])   
#%% Solve Dual Problem
# Load in BC - Y Directions
a=np.where(np.isclose(Xn,.5))[0]
indy=np.zeros(3*len(a),dtype=int)
ind=np.arange(0,len(gDOF))
for i in range(len(a)):
    indy[3*i]=np.where(gDOF==Noden[a[i]]+.1)[0]
    indy[3*i+1]=np.where(gDOF==Noden[a[i]]+.2)[0]
    indy[3*i+2]=np.where(gDOF==Noden[a[i]]+.3)[0]
ind=np.delete(ind,indy)
Kred=K[ind][:,ind]
Mred=M[ind][:,ind]
DOFred=np.delete(gDOF,indy)
Qred=np.delete(H,indy,axis=1)

# Eliminate zero stiffness DOF
indz=[]
ind=np.arange(0,len(DOFred))
for i in range(len(DOFred)):
    if np.all(Kred[:,i].todense()==0.):
        indz.append(i)
indz=np.array(indz).reshape(-1)
ind=np.delete(ind,indz)
Kred=Kred[ind][:,ind]
Mred=Mred[ind][:,ind]
DOFred=np.delete(DOFred,indz)
Qred=np.delete(Qred,indz,axis=1)   

# Solve System
Zdualred=spinv(Kred.tocsc())*sparse.csc_matrix(Qred.T)
# Expand to full dimentionality
Zh=np.zeros((nDOF,len(Qred)))
for i in range(nDOF):
    if gDOF[i] in DOFred:
        Zh[i,:]=Zdualred[np.where(DOFred==gDOF[i])[0],:].todense()
Zh=sparse.csr_matrix(Zh)
# Separate into Directions
Zsp=[]
for i in range(6):
    Zsp.append(Zh[:,NQ*i:NQ*(i+1)])
print('End of Dual Calc')
#%% Post-Process Dual Problem
# Calculate strains
ep_z=[]
ZEB1,ZEB2,ZEB3,ZEB4,ZEB5,ZEB6=[],[],[],[],[],[]
ZEB1zz,ZEB2zz,ZEB3zz,ZEB4zz,ZEB5zz,ZEB6zz=[],[],[],[],[],[]
for p in range(6):
    ep_z.append([])
    for i in range(Nele):
        ep_z[p].append([])
        for j in range(NGP):
            (indx,indy)=np.where((GP==j+1)*(ELE==i+1))
            ep_z[p][i].append((B[indx[0]][indy[0]]*ehat[indx[0]]*Zsp[p]).todense())
    # Separate into individual arrays
    ZEB1a,ZEB2a,ZEB3a=np.zeros((Ngpt,NQ)),np.zeros((Ngpt,NQ)),np.zeros((Ngpt,NQ))
    ZEB4a,ZEB5a,ZEB6a=np.zeros((Ngpt,NQ)),np.zeros((Ngpt,NQ)),np.zeros((Ngpt,NQ))  
    for i in range(Ngpt):
        (indx,indy)=np.where((ELE==ELE_ar[i])*(GP==GP_ar[i]))
        ZEB1a[i,:],ZEB2a[i,:],ZEB3a[i,:]=ep_z[p][indx[0]][indy[0]][0],ep_z[p][indx[0]][indy[0]][1],ep_z[p][indx[0]][indy[0]][2]
        ZEB4a[i,:],ZEB5a[i,:],ZEB6a[i,:]=ep_z[p][indx[0]][indy[0]][3],ep_z[p][indx[0]][indy[0]][4],ep_z[p][indx[0]][indy[0]][5]
    ZEB1.append(ZEB1a),ZEB2.append(ZEB2a),ZEB3.append(ZEB3a)
    ZEB4.append(ZEB4a),ZEB5.append(ZEB5a),ZEB6.append(ZEB6a)
    
    ZET=np.concatenate((ZEB1a,ZEB2a,ZEB3a,ZEB4a,ZEB5a,ZEB6a),axis=1)  
    # Perform ZZ recovery (Currently NN approach)
    A=FUN.ZZ_NN3D(Xn,Yn,Zn,Noden,X,Y,Z,ZET,ELE_ar,Nodal_DOF,ind_ele,ELE[:,0].reshape(-1))
    ZEB1zz.append(A[:,:NQ]),ZEB2zz.append(A[:,1*NQ:2*NQ]),ZEB3zz.append(A[:,2*NQ:3*NQ])
    ZEB4zz.append(A[:,3*NQ:4*NQ]),ZEB5zz.append(A[:,4*NQ:5*NQ]),ZEB6zz.append(A[:,5*NQ:6*NQ])
print('End of Dual ZZ')
#%% GOEE
GOEE=[]
for p in range(6):
    # Bending
    GOEE_b=np.zeros((Nele,NQ))
    for k in range(NQ):
        for i in range(Nele):    
            # Bending
            a=np.zeros(NGP)
            Tf=np.concatenate((np.concatenate((T[i],O),axis=0),np.concatenate((O,T[i]),axis=0)),axis=1)
            Cb=Tf*C*Tf.T
            P1,P2,P3=np.matrix(EB1zz[ind_ele[i]]).reshape((NpE,1)),np.matrix(EB2zz[ind_ele[i]]).reshape((NpE,1)),np.matrix(EB3zz[ind_ele[i]]).reshape((NpE,1))
            P4,P5,P6=np.matrix(EB4zz[ind_ele[i]]).reshape((NpE,1)),np.matrix(EB5zz[ind_ele[i]]).reshape((NpE,1)),np.matrix(EB6zz[ind_ele[i]]).reshape((NpE,1))
            # Dual
            D1,D2,D3=np.matrix(ZEB1zz[p][ind_ele[i],k]).reshape((NpE,1)),np.matrix(ZEB2zz[p][ind_ele[i],k]).reshape((NpE,1)),np.matrix(ZEB3zz[p][ind_ele[i],k]).reshape((NpE,1))
            D4,D5,D6=np.matrix(ZEB4zz[p][ind_ele[i],k]).reshape((NpE,1)),np.matrix(ZEB5zz[p][ind_ele[i],k]).reshape((NpE,1)),np.matrix(ZEB6zz[p][ind_ele[i],k]).reshape((NpE,1))
            # Ease of Use
            P=np.concatenate((P1,P2,P3,P4,P5,P6),axis=1)
            D=np.concatenate((D1,D2,D3,D4,D5,D6),axis=1)
            for j in range(NGP):
                # Primal
                PrimeZZ=(N(xi_i[j],eta_i[j])*P).T
                Prime=ep_u[i][j]
                # Dual
                DualZZ=(N(xi_i[j],eta_i[j])*D).T
                Dual=ep_z[p][i][j][:,k]
                # Calculate
                a[j]=(PrimeZZ-Prime).T*Cb*(DualZZ-Dual)*Ar[i,j]*t
            GOEE_b[i,k]=np.sum(a)
    # Total
    GOEE.append(GOEE_b)
print('End of GOEE')
#%% Import [M] matrix for Multi-Scale Propegation
# Feature model, Nominal, Large, Slot, SingleHole, TripleHole
if Feature=='Nominal':
    Mmat=np.matrix(np.load('HoleFeatures/Nominal/TSection.npy'))
    a=np.array(np.load('HoleFeatures/Nominal/TSectionCoords.npy'))
elif Feature=='Large':
    Mmat=np.matrix(np.load('HoleFeatures/LargeHole/TSection.npy'))
    a=np.array(np.load('HoleFeatures/LargeHole/TSectionCoords.npy'))
elif Feature=='Slot':
    Mmat=np.matrix(np.load('HoleFeatures/Slot/TSection.npy'))
    a=np.array(np.load('HoleFeatures/Slot/TSectionCoords.npy'))
elif Feature=='SingleHole':
    Mmat=np.matrix(np.load('HoleFeatures/Web1Hole/TSection.npy'))
    a=np.array(np.load('HoleFeatures/Web1Hole/TSectionCoords.npy'))
elif Feature=='TripleHole':
    Mmat=np.matrix(np.load('HoleFeatures/Web3Hole/TSection.npy'))
    a=np.array(np.load('HoleFeatures/Web3Hole/TSectionCoords.npy'))
else:
    raise(Exception('Unknown Feature Model'))
# Specifiy Feature locations
     # Feature at 100 mm
#Node_id=[520,571,622,673,724,775,826,877,928,979,1030,469,418,367,316,265,214,163,112,61,10,1081,1132,1183,1234,1285,1031,11,1286,1032,12,1287,1033,13,1288,524,575,626,677,728,779,830,881,932,983,1034,473,422,371,320,269,218,167,116,65,14,1085,1136,1187,1238,1289] # T-Section
     # Feature at 400 mm
Node_id=[550,601,652,703,754,805,856,907,958,1009,1060,499,448,397,346,295,244,193,142,91,40,1111,1162,1213,1264,1315,1061,41,1316,1062,42,1317,1063,43,1318,554,605,656,707,758,809,860,911,962,1013,1064,503,452,401,350,299,248,197,146,95,44,1115,1166,1217,1268,1319]
# Local to global transfermation matrix
T=np.eye(6) # Based on Mmat generation directions
# Split feature centroid coordinates into X,Y,Z
Xp,Yp,Zp=a[:,0],a[:,1],a[:,2]
#%% Nominal Stress Propagation
# Convert to global matrix and convert unit system: Nominal Stress
Nele_p=len(Xp)
FMdof,Utr=np.zeros(6*len(Node_id)),np.zeros((6*len(Node_id),1))
for k in range(len(Node_id)):
    b=np.matrix(np.zeros((6,1)))
    for i in range(6):
        ind=np.where(gDOF==Noden[Node_id[k]]+(i+1)/10.)[0]
        FMdof[6*k+i]=gDOF[ind]
        if i<3:
            b[i]=Uh[ind].todense()*1e3
        else:
            b[i]=Uh[ind].todense()
    Utr[6*k:6*(k+1),0]=(T.T*b).reshape(-1)
Sig_pnom=np.array(Mmat*Utr).reshape(6,Nele_p)  
# Convert into von Mises
Svm=VonMises(Sig_pnom[0,:],Sig_pnom[1,:],Sig_pnom[2,:],Sig_pnom[3,:],Sig_pnom[4,:],Sig_pnom[5,:])
#%% Gaussian Process for GOEE
tm=time.time()
# Setup Matrices
nu,L_opt=5.,.04
# Apply BC
a=np.where(np.isclose(Xn,.5))[0]
indy=np.zeros(3*len(a),dtype=int)
for i in range(len(a)):
    indy[3*i]=np.where(gDOF==Noden[a[i]]+.1)[0]
    indy[3*i+1]=np.where(gDOF==Noden[a[i]]+.2)[0]
    indy[3*i+2]=np.where(gDOF==Noden[a[i]]+.3)[0]
DOFred=np.delete(gDOF,indy)
Nbc=len(indy)
# Calculate Prior
def kernel(X1,Y1,Z1,X2,Y2,Z2,nu,l):
    import numpy as np,scipy.special as special
    dx = np.sum(X1**2, 1).reshape(-1, 1) + np.sum(X2**2, 1)-2*np.dot(X1,X2.T)
    dy = np.sum(Y1**2, 1).reshape(-1, 1) + np.sum(Y2**2, 1)-2*np.dot(Y1,Y2.T)
    dz = np.sum(Z1**2, 1).reshape(-1, 1) + np.sum(Z2**2, 1)-2*np.dot(Z1,Z2.T)
    r=np.sqrt(dx+dy+dz)
    A=2**(1-nu)/special.gamma(nu)*(np.sqrt(2*nu)*r/l)**nu*special.kv(nu,np.sqrt(2*nu)*r/l)
    A[r==0]=1
    return(A)

X1,Y1,Z1=Xn.reshape(-1,1),Yn.reshape(-1,1),Zn.reshape(-1,1)

# Setup identifying matrix
Hr=np.matrix(np.zeros((NQ+len(a),len(Xn))))
for i in range(NQ):
    Hr[i,ind_nq[ind_QOI[i]]]=1.
for i in range(len(a)):
    Hr[NQ+i,a[i]]=1.
    
    
    
# Predictive Distribution
Fmean,Fcov,Ytrain,=[],[],[]
Ktt=np.matrix(kernel(X1,Y1,Z1,X1,Y1,Z1,nu,L_opt))
Kinv=np.linalg.inv(Hr*Ktt*Hr.T)
for p in range(6):
    Ytrain.append(np.matrix(np.concatenate((np.sum(GOEE[p],axis=0),np.zeros(len(a))),axis=0)).reshape((-1,1)))
    Fmean.append(np.array(Ktt*Hr.T*Kinv*Ytrain[p]).reshape(-1))
    Fcov.append(Ktt-Ktt*Hr.T*Kinv*Hr*Ktt)

print('Time for GP = %.1f Minutes'%((time.time()-tm)/60))
#%% Visualize in 3D
d=2
ind=np.where((Zn==0)*(Xn>=0.4)*(Xn<=.44))[0]
A=np.array(QOI_Center)
xt,yt,zt=A[:,0],A[:,1],A[:,2]
indt=np.where(zt==0)[0]
plt.figure()
ax=plt.axes(projection='3d')
ax.plot_trisurf(Xn[ind],Yn[ind],Fmean[d][ind]*1e3,color='b',label='GP Mean')
ax.plot3D(xt[indt],yt[indt],np.array(Ytrain[d][indt]*1e3).reshape(-1),'ro',label='GOEE Solutions')
plt.xlabel('X-Direction')
plt.ylabel('Y-Direction')
plt.xlim([.4,.44])
plt.title('GP Mean Validation')
plt.show()

#plt.figure()
#ax=plt.axes(projection='3d')
#ax.plot_trisurf(Xn[ind],Yn[ind],Fmean[d][ind]*1e3,color='b')
#ax.plot_trisurf(Xn[ind],Yn[ind],Fmean[d][ind]*1e3+np.sqrt(np.abs(np.diag(Fcov[d]))[ind])*1e3,color='g')
#ax.plot_trisurf(Xn[ind],Yn[ind],Fmean[d][ind]*1e3-np.sqrt(np.abs(np.diag(Fcov[d]))[ind])*1e3,color='g')
#plt.xlabel('X-Direction')
#plt.ylabel('Y-Direction')
#plt.title('Predictive Mean [mm]')
#plt.show()

#%% Propagate UQ into Feature Model
# Convert to global matrix and convert unit system: GOEE
Emean=np.array(np.zeros((6*len(Node_id),1)))
for k in range(len(Node_id)):
    b=np.matrix(np.zeros((6,1)))
    for i in range(6):
        if i<3:
            b[i]=Fmean[i][Node_id[k]]*1e3
        else:
            b[i]=Fmean[i][Node_id[k]]
    Emean[6*k:6*(k+1),0]=(T.T*b).reshape(-1)
Sig_Mean=np.array(Mmat*Emean).reshape(6,Nele_p) 
# Convert into von Mises error
ESvm=VonMises(Sig_Mean[0,:],Sig_Mean[1,:],Sig_Mean[2,:],Sig_Mean[3,:],Sig_Mean[4,:],Sig_Mean[5,:])
# Error of von Mises Stress
ErrSvm=VonMises(Sig_pnom[0,:]+Sig_Mean[0,:],Sig_pnom[1,:]+Sig_Mean[1,:],Sig_pnom[2,:]+Sig_Mean[2,:],Sig_pnom[3,:]+Sig_Mean[3,:],Sig_pnom[4,:]+Sig_Mean[4,:],Sig_pnom[5,:]+Sig_Mean[5,:])-Svm