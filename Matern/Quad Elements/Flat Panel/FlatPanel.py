import numpy as np,matplotlib.pyplot as plt, scipy.sparse as sparse,time
import Functions as FUN,mpl_toolkits.mplot3d
from scipy.sparse.linalg import inv as spinv
"""
This script is an example run script for the Python Standlone of the MARQUESS
GOEE workflow. The system is a general system that represents a simplified
stiffener panel with a T cross-section. Several feature models are generated to
use. The location of this feature is pre-programmed for either 100mm or 400mm
from the loading location. To place in any other location, denote the order of
the pre-programmed location and create new list of nodes that correspond to the
same order, but at different location. DOF vectors are based on Patran ordering.
Each DOF is assigned Node#.dof# (example 102.2) where the dof# is:
1-Displacement X, 2-Displacement Y, 3-Displacement Z, 4-Rotation X, 5-Rotation
Y, 6-Rotation Z. It is assumed that all 6 directions are used.

Most modifications are done in this section. Any modifications furture alone in
♪the scripts. Also for a new system or BC, change "Solve Dual Problems" and 
"Gaussian Process for GOEE" sections with updated BC.

    Created by Matthew Bonney for MARQUESS GOEE - University of Nottingham
    Ceated on: 09/10/2019
    
    Updated Testing for Matern Prior. Replaced the Squared Exponential prior 
    with a Matern prior to better incorporate physical connectivity.
    Updated on: 25/06/2020 by Matthew Bonney
"""
# Parameters for bending, membrane, and sheer DOF in local directions
Bend_dof=[0,1,5]
Memb_dof=[0,1,5]
Sher_dof=[3,4]
# Linear Elastic Material Properties
E=73.1e9
nu=0.33
t=0.001
# Locations of Dual centers
Points='Feature' # 'Node' or 'Ele' or 'Det';
DUAL='Disp' # 'Stress' or 'Disp'
# If Points=='Det', Modify Line 152 with XYZ Center (Or find "if Points=='Det':")
NQ=110 # For Rand Max=1275 for node, 1250 for ele. Doesn't matter for Det
L_QOI=.01 # Length hyper-parameter for Guassian spacial distribution for integration
Lcut=np.inf # Set max length of spacial distribution, leave inf to assign weight to every integration point. Used for numerical condition issues
# Gaussian Process
alpha=1e-2
beta=1e-4
Nmc=10 # Number of samples from GP distribution
# File Locations for Primal Solution
Stiff='Dat Files/ELEM_K.dat'
Mass='Dat Files/ELEM_M.dat'
Element='Dat Files/ELEM_CONNECT.dat'
GCOORD='Dat Files/GAUSS_COORDS.dat'
Ufile='Dat Files/U.dat'
NCOORD='Dat Files/NODE_COORDS.dat'
Bbend='Dat Files/B_B.dat'
Bmemb='Dat Files/B_M.dat'
Bsher='Dat Files/B_S.dat'
Jaco='Dat Files/ELEM_JAC.dat'
Rot='Dat Files/ELEM_ROT.dat'
#%% Primal Solutions
(Kp,DOFk)=FUN.Import_Generic(Stiff,'k')
(Mp,DOFm)=FUN.Import_Generic(Mass,'k')
#DOFm,Mp=DOFk.copy(),sparse.csr_matrix(np.eye(len(DOFk)))
(U,DOFu)=FUN.Import_Generic(Ufile,'u')
(T,ELE_Rot)=FUN.Import_Generic(Rot,'r')
(Xn,Yn,Zn,Noden)=FUN.Import_Generic(NCOORD,'ncoord')
# Setup Interpolation functions
N1=lambda xi,eta: .25*(1-xi)*(1-eta)
N2=lambda xi,eta: .25*(1+xi)*(1-eta)
N3=lambda xi,eta: .25*(1+xi)*(1+eta)
N4=lambda xi,eta: .25*(1-xi)*(1+eta)
N=lambda xi,eta: np.matrix([N1(xi,eta),N2(xi,eta),N3(xi,eta),N4(xi,eta)])
# Setup Material Information
a=E/(1-nu**2)
Cmemb=a*np.matrix([[1,nu,0],[nu,1,0],[0,0,(1-nu)/2]])
Cbend=t**2/12*Cmemb
Csher=5/6.*a*np.matrix([[(1-nu)/2,0],[0,(1-nu)/2]])
# Settup global DOF vector
gDOF=np.unique(DOFk.tolist()+DOFu.tolist())
# Ensure all 6 DOF are used in calculation
test=gDOF-np.array(gDOF,dtype=int)
if len(test)%6==0:
    a=test.reshape((-1,6))
    b=np.zeros(int(len(test)/6-1))
    for i in range(a.shape[0]-1):
        b[i]=np.allclose(a[i],a[i+1])
    if not np.all(b):
        a,b=np.max(gDOF)-np.max(gDOF)%1,np.min(gDOF)-np.min(gDOF)%1
        c,d=np.arange(b,a+1),np.arange(1,7)
        gDOF=np.sort(np.array(c.tolist()*len(d)))+np.array(d.tolist()*len(c))/10.
else:
    a,b=np.max(gDOF)-np.max(gDOF)%1,np.min(gDOF)-np.min(gDOF)%1
    c,d=np.arange(b,a+1),np.arange(1,7)
    gDOF=np.sort(np.array(c.tolist()*len(d)))+np.array(d.tolist()*len(c))/10.
nDOF=len(gDOF)
# Ensure sizes are correct
if not np.all(np.in1d(gDOF,DOFu)): # Displacement Vector
    nnz=U.nnz
    row=np.zeros(nnz,dtype=int)
    for i in range(nnz):
        row[i]=np.where(gDOF==DOFu[i])[0]
    Uh=sparse.csr_matrix((np.array(U[U.nonzero()]).flatten(),(row,np.zeros(nnz,dtype=int))),shape=(nDOF,1))
else:
    Uh=U
if not np.all(np.in1d(gDOF,DOFk)): # Stiffness Matrix
    nnz=Kp.nnz
    (row0,col0)=Kp.nonzero()
    row,col=np.zeros(nnz,dtype=int),np.zeros(nnz,dtype=int)
    for i in range(nnz):
        row[i]=np.where(gDOF==DOFk[row0[i]])[0]
        col[i]=np.where(gDOF==DOFk[col0[i]])[0]
    K=sparse.csr_matrix((np.array(Kp[Kp.nonzero()]).flatten(),(row,col)),shape=(nDOF,nDOF))
else:
    K=Kp
if not np.all(np.in1d(gDOF,DOFm)): # Stiffness Matrix
    nnz=Mp.nnz
    (row0,col0)=Mp.nonzero()
    row,col=np.zeros(nnz,dtype=int),np.zeros(nnz,dtype=int)
    for i in range(nnz):
        row[i]=np.where(gDOF==DOFm[row0[i]])[0]
        col[i]=np.where(gDOF==DOFm[col0[i]])[0]
    M=sparse.csr_matrix((np.array(Mp[Mp.nonzero()]).flatten(),(row,col)),shape=(nDOF,nDOF))
else:
    M=Mp
Uz=Uh[np.where(np.isclose(gDOF%1,0.3))[0]]
# Import Integration values/ B matrix
(Bb,ELEb,GPb)=FUN.Import_Generic(Bbend,'b')
(Bm,ELEm,GPm)=FUN.Import_Generic(Bmemb,'b')
(Bs,ELEs,GPs)=FUN.Import_Generic(Bsher,'b')
(J,ELEj,GPj)=FUN.Import_Generic(Jaco,'j')
(Nele,NGP)=GPb.shape

# Rotate B matrix to global CS
O=np.matrix(np.zeros((3,3)))
for i in range(Nele):
    Tf=np.concatenate((np.concatenate((T[i],O),axis=0),np.concatenate((O,T[i]),axis=0)),axis=1)
    # Bending
    indx,indy=np.where(ELEb==ELE_Rot[i])
    for j in range(len(indy)):
        Bb[indx[0]][indy[j]]=sparse.csr_matrix(Tf[:,Bend_dof]*Bb[indx[0]][indy[j]],shape=(6,24))
    # Membrane
    indx,indy=np.where(ELEm==ELE_Rot[i])
    for j in range(len(indy)):
        Bm[indx[0]][indy[j]]=sparse.csr_matrix(Tf[:,Memb_dof]*Bm[indx[0]][indy[j]],shape=(6,24))
    # Shear
    indx,indy=np.where(ELEs==ELE_Rot[i])
    for j in range(len(indy)):
        Bs[indx[0]][indy[j]]=sparse.csr_matrix(Tf[:,Sher_dof]*Bs[indx[0]][indy[j]],shape=(6,24))

# Calculate ehat
(ELE_dof,Node_dof)=FUN.Import_Generic(Element,'dof')
Nodal_DOF=np.unique(np.array(gDOF,dtype=int))
ehat,ind_ele=[],[]
for i in range(Nele):
    a=np.matrix(np.zeros((24,nDOF)))
    ind_ele.append([])
    for j in range(NGP):
        ind=np.where(np.array(gDOF,dtype=int)==Node_dof[i][j])[0]
        ind_ele[i].append(int(np.where(Nodal_DOF==Node_dof[i][j])[0]))
        for k in range(6):
            a[6*j+k,ind[k]]=1.
    ehat.append(sparse.csr_matrix(a))
# Find Centroids
Xc,Yc,Zc=np.zeros(Nele),np.zeros(Nele),np.zeros(Nele)
for i in range(Nele):
    Xc[i],Yc[i],Zc[i]=N(0,0)*np.matrix(Xn[ind_ele[i]]).T,N(0,0)*np.matrix(Yn[ind_ele[i]]).T,N(0,0)*np.matrix(Zn[ind_ele[i]]).T
# Setup Dual Locations
if Points=='Det':
    QOI_Center=[[0.1 , 0.1 , 0.  ],[0.1 , 0.11, 0.  ],   [0.1 , 0.12, 0.  ],   [0.1 , 0.13, 0.  ],   [0.1 , 0.14, 0.  ],   [0.1 , 0.15, 0.  ],   [0.1 , 0.16, 0.  ],   [0.1 , 0.17, 0.  ],   [0.1 , 0.18, 0.  ],   [0.1 , 0.19, 0.  ],   [0.1 , 0.2 , 0.  ],   [0.1 , 0.09, 0.  ],   [0.1 , 0.08, 0.  ],   [0.1 , 0.07, 0.  ],   [0.1 , 0.06, 0.  ],   [0.1 , 0.05, 0.  ],   [0.1 , 0.04, 0.  ],   [0.1 , 0.03, 0.  ],   [0.1 , 0.02, 0.  ],   [0.1 , 0.01, 0.  ],   [0.1 , 0.  , 0.  ],   [0.1 , 0.1 , 0.01],   [0.1 , 0.1 , 0.02],   [0.1 , 0.1 , 0.03],   [0.1 , 0.1 , 0.04],   [0.1 , 0.1 , 0.05],   [0.11, 0.2 , 0.  ],   [0.11, 0.  , 0.  ],   [0.11, 0.1 , 0.05],   [0.12, 0.2 , 0.  ],   [0.12, 0.  , 0.  ],   [0.12, 0.1 , 0.05],   [0.13, 0.2 , 0.  ],   [0.13, 0.  , 0.  ],   [0.13, 0.1 , 0.05],   [0.14, 0.1 , 0.  ],   [0.14, 0.11, 0.  ],   [0.14, 0.12, 0.  ],   [0.14, 0.13, 0.  ],   [0.14, 0.14, 0.  ],   [0.14, 0.15, 0.  ],   [0.14, 0.16, 0.  ],   [0.14, 0.17, 0.  ],   [0.14, 0.18, 0.  ],   [0.14, 0.19, 0.  ],   [0.14, 0.2 , 0.  ],   [0.14, 0.09, 0.  ],   [0.14, 0.08, 0.  ],   [0.14, 0.07, 0.  ],   [0.14, 0.06, 0.  ],   [0.14, 0.05, 0.  ],   [0.14, 0.04, 0.  ],   [0.14, 0.03, 0.  ],   [0.14, 0.02, 0.  ],   [0.14, 0.01, 0.  ],   [0.14, 0.  , 0.  ],   [0.14, 0.1 , 0.01],   [0.14, 0.1 , 0.02],   [0.14, 0.1 , 0.03],   [0.14, 0.1 , 0.04],   [0.14, 0.1 , 0.05]]
    NQ=len(QOI_Center)
elif Points=='Ele':
    ind_QOI=np.random.choice(Nele,size=(NQ),replace=False)
    QOI_Center=np.concatenate((Xc[ind_QOI].reshape(-1,1),Yc[ind_QOI].reshape(-1,1),Zc[ind_QOI].reshape(-1,1)),axis=1).tolist()
elif Points=='Feature':
    ind=np.concatenate((np.where((Xn==0)*(Yn>=.1)*(Yn<=.15))[0],np.where((Xn==.5)*(Yn>=.1)*(Yn<=.15))[0],np.where((Xn>0)*(Xn<.5)*(Yn==.1))[0],np.where((Xn>0)*(Xn<.5)*(Yn==.15))[0]))
#    ind_QOI=np.random.choice(len(ind_QOI),size=(NQ),replace=False)
    ind_QOI=np.arange(len(ind))
    QOI_Center=[]
    for i in range(NQ):
        QOI_Center.append([Xn[ind[ind_QOI[i]]],Yn[ind[ind_QOI[i]]],Zn[ind[ind_QOI[i]]]])
elif Points=='Node':
    ind=np.where(Yn!=0.0)[0]
    xr,yr,zr=Xn[ind],Yn[ind],Zn[ind]
    ind_QOI=np.random.choice(len(xr),size=(NQ),replace=False)
    QOI_Center=(np.array([xr[ind_QOI],yr[ind_QOI],zr[ind_QOI]]).T).tolist()
    NQ=len(QOI_Center)
#%% Post-Process Primal Problem
# Calculate strains
ep_ub,ep_um,ep_us=[],[],[]
for i in range(Nele):
    ep_ub.append([])
    ep_um.append([])
    ep_us.append([])
    for j in range(NGP):
        (indx,indy)=np.where((GPb==j+1)*(ELEb==i+1))
        ep_ub[i].append((Bb[indx[0]][indy[0]]*ehat[indx[0]]*Uh).todense())
        ep_um[i].append((Bm[indx[0]][indy[0]]*ehat[indx[0]]*Uh).todense())
        ep_us[i].append((Bs[indx[0]][indy[0]]*ehat[indx[0]]*Uh).todense())
# Separate into individual arrays
(X,Y,Z,ELE_ar,GP_ar)=FUN.Import_Generic(GCOORD,'gclabel')     
Ngpt=len(ELE_ar)
EB1,EB2,EB3=np.zeros((Ngpt,1)),np.zeros((Ngpt,1)),np.zeros((Ngpt,1))
EB4,EB5,EB6=np.zeros((Ngpt,1)),np.zeros((Ngpt,1)),np.zeros((Ngpt,1))
EM1,EM2,EM3=np.zeros((Ngpt,1)),np.zeros((Ngpt,1)),np.zeros((Ngpt,1))
EM4,EM5,EM6=np.zeros((Ngpt,1)),np.zeros((Ngpt,1)),np.zeros((Ngpt,1))
ES1,ES2,ES3=np.zeros((Ngpt,1)),np.zeros((Ngpt,1)),np.zeros((Ngpt,1))
ES4,ES5,ES6=np.zeros((Ngpt,1)),np.zeros((Ngpt,1)),np.zeros((Ngpt,1))
for i in range(Ngpt):
    (indx,indy)=np.where((ELEb==ELE_ar[i])*(GPb==GP_ar[i]))
    EB1[i],EB2[i],EB3[i]=ep_ub[indx[0]][indy[0]][0],ep_ub[indx[0]][indy[0]][1],ep_ub[indx[0]][indy[0]][2]
    EB4[i],EB5[i],EB6[i]=ep_ub[indx[0]][indy[0]][3],ep_ub[indx[0]][indy[0]][4],ep_ub[indx[0]][indy[0]][5]
    EM1[i],EM2[i],EM3[i]=ep_um[indx[0]][indy[0]][0],ep_um[indx[0]][indy[0]][1],ep_um[indx[0]][indy[0]][2]
    EM4[i],EM5[i],EM6[i]=ep_um[indx[0]][indy[0]][3],ep_um[indx[0]][indy[0]][4],ep_um[indx[0]][indy[0]][5]
    ES1[i],ES2[i],ES3[i]=ep_us[indx[0]][indy[0]][0],ep_us[indx[0]][indy[0]][1],ep_us[indx[0]][indy[0]][2]
    ES4[i],ES5[i],ES6[i]=ep_us[indx[0]][indy[0]][3],ep_us[indx[0]][indy[0]][4],ep_us[indx[0]][indy[0]][5]
 
EB=np.concatenate((EB1,EB2,EB3,EB4,EB5,EB6),axis=1)    
EM=np.concatenate((EM1,EM2,EM3,EM4,EM5,EM6),axis=1)
ES=np.concatenate((ES1,ES2,ES3,ES4,ES5,ES6),axis=1)
ET=np.concatenate((EB,EM,ES),axis=1)

# Perform ZZ recovery (Currently NN approach)
Nnode=len(Noden)
Nodal_DOF=np.unique(np.array(gDOF,dtype=int))

A=FUN.ZZ_NN3D(Xn,Yn,Zn,Noden,X,Y,Z,ET,ELE_ar,Nodal_DOF,ind_ele,ELEb[:,0].reshape(-1))
EB1zz,EB2zz,EB3zz=A[:,0].reshape(-1),A[:,1].reshape(-1),A[:,2].reshape(-1)
EB4zz,EB5zz,EB6zz=A[:,3].reshape(-1),A[:,4].reshape(-1),A[:,5].reshape(-1)
EM1zz,EM2zz,EM3zz=A[:,6].reshape(-1),A[:,7].reshape(-1),A[:,8].reshape(-1)
EM4zz,EM5zz,EM6zz=A[:,9].reshape(-1),A[:,10].reshape(-1),A[:,11].reshape(-1)
ES1zz,ES2zz,ES3zz=A[:,12].reshape(-1),A[:,13].reshape(-1),A[:,14].reshape(-1)
ES4zz,ES5zz,ES6zz=A[:,15].reshape(-1),A[:,16].reshape(-1),A[:,17].reshape(-1)

print('End of Primal')
VonMises=lambda s11,s22,s33,s23,s13,s12: np.sqrt(.5*((s11-s22)**2+(s22-s33)**2+(s33-s11)**2+6*(s23**2+s13**2+s12**2)))
#%% Setup Dual Problem/ Calculate Q vector
SE=lambda x,y,z,i: np.exp(-((x-QOI_Center[i][0])**2+(y-QOI_Center[i][1])**2+(z-QOI_Center[i][2])**2)/(2*L_QOI**2))
Dist=lambda x,y,z,i: np.sqrt((x-QOI_Center[i][0])**2+(y-QOI_Center[i][1])**2+(z-QOI_Center[i][2])**2)
Equl=lambda x,y,z,i: 1.
# Calculate summation weighting
Hat=[]
H=np.matrix(np.zeros((6*NQ,nDOF)))
for k in range(NQ):
    Xint,Yint,Zint,What=np.zeros((Nele,NGP)),np.zeros((Nele,NGP)),np.zeros((Nele,NGP)),np.zeros((Nele,NGP))
    # Determine Nodal weights
    Wn=np.zeros(Nnode)
    for i in range(Nnode):
        l=Dist(Xn[i],Yn[i],Zn[i],k)
        if l<Lcut:
            Wn[i]=SE(Xn[i],Yn[i],Zn[i],k)
    # Interpolate
    (xi_i,eta_i,Wi)=FUN.quad_2Dlocation(1)
    for i in range(Nele):
        for j in range(NGP):
            Xint[i,j]=N(xi_i[j],eta_i[j])*np.matrix(Xn[ind_ele[i]]).T
            Yint[i,j]=N(xi_i[j],eta_i[j])*np.matrix(Yn[ind_ele[i]]).T
            Zint[i,j]=N(xi_i[j],eta_i[j])*np.matrix(Zn[ind_ele[i]]).T
            What[i,j]=N(xi_i[j],eta_i[j])*np.matrix(Wn[ind_ele[i]]).T
    What=What/np.sum(What)
    Hat.append(What)
    for p in range(6):
    # Calculate Q vector
        Q_ele,Ar=[],np.zeros((Nele,NGP))
        for i in range(Nele):
            Q_ele.append(np.matrix(np.zeros((1,nDOF))))
            Tf=np.concatenate((np.concatenate((T[i],O),axis=0),np.concatenate((O,T[i]),axis=0)),axis=1)
            Cb=Tf[:,Bend_dof]*Cbend*Tf[:,Bend_dof].T
            Cm=Tf[:,Memb_dof]*Cmemb*Tf[:,Memb_dof].T
            Cs=Tf[:,Sher_dof]*Csher*Tf[:,Sher_dof].T
            for j in range(NGP):
                indj=np.where((GPj==j+1)*(ELEj==i+1))[0][0]
                Ar[i,j]=J[indj]*Wi[j]*(What[i,j]!=0)
                
                if DUAL=='Stress':
                    (indbx,indby)=np.where((GPb==j+1)*(ELEb==i+1))
                    Qb=Cb[p,:]*Bb[indbx[0]][indby[0]]*ehat[i]*Ar[i,j]*What[i,j]
                    (indmx,indmy)=np.where((GPm==j+1)*(ELEm==i+1))
                    Qm=Cm[p,:]*Bm[indmx[0]][indmy[0]]*ehat[i]*Ar[i,j]*What[i,j]
                    (indsx,indsy)=np.where((GPs==j+1)*(ELEs==i+1))
                    Qs=Cs[p,:]*Bs[indsx[0]][indsy[0]]*ehat[i]*Ar[i,j]*What[i,j]
                    Q_ele[i]=Q_ele[i]+Qb+Qm+Qs
                elif DUAL=='Disp':
                    N1x,N2x=np.matrix(N1(xi_i[j],eta_i[j])*np.eye(6)),np.matrix(N2(xi_i[j],eta_i[j])*np.eye(6))
                    N3x,N4x=np.matrix(N3(xi_i[j],eta_i[j])*np.eye(6)),np.matrix(N4(xi_i[j],eta_i[j])*np.eye(6))
                    Nt=np.concatenate((N1x,N2x,N3x,N4x),axis=1)
                    Q_ele[i]=Q_ele[i]+Nt[p,:]*ehat[i]*Ar[i,j]*What[i,j]
                else:
                    raise(Exception('Error on Dual Formulation'))
        # Assemble 
        Q=np.matrix(np.sum(np.array(Q_ele),axis=0))
        H[NQ*p+k,:]=Q/(np.sum(Ar))*len(np.nonzero(What)[0])  
#%% Solve Dual Problem
# Load in BC - Y Directions
a=np.where(np.isclose(Yn,0.))[0]
indy=np.zeros(3*len(a),dtype=int)
ind=np.arange(0,len(gDOF))
for i in range(len(a)):
    indy[3*i]=np.where(gDOF==Noden[a[i]]+.1)[0]
    indy[3*i+1]=np.where(gDOF==Noden[a[i]]+.2)[0]
    indy[3*i+2]=np.where(gDOF==Noden[a[i]]+.3)[0]
ind=np.delete(ind,indy)
Kred=K[ind][:,ind]
Mred=M[ind][:,ind]
DOFred=np.delete(gDOF,indy)
Qred=np.delete(H,indy,axis=1)

# Eliminate zero stiffness DOF
indz=[]
ind=np.arange(0,len(DOFred))
for i in range(len(DOFred)):
    if np.all(Kred[:,i].todense()==0.):
        indz.append(i)
indz=np.array(indz).reshape(-1)
ind=np.delete(ind,indz)
Kred=Kred[ind][:,ind]
Mred=Mred[ind][:,ind]
DOFred=np.delete(DOFred,indz)
Qred=np.delete(Qred,indz,axis=1)   

# Solve System
Zdualred=spinv(Kred.tocsc())*sparse.csc_matrix(Qred.T)
# Expand to full dimentionality
Zh=np.zeros((nDOF,len(Qred)))
for i in range(nDOF):
    if gDOF[i] in DOFred:
        Zh[i,:]=Zdualred[np.where(DOFred==gDOF[i])[0],:].todense()
Zh=sparse.csr_matrix(Zh)
# Separate into Directions
Zsp=[]
for i in range(6):
    Zsp.append(Zh[:,NQ*i:NQ*(i+1)])
print('End of Dual Calc')
#%% Post-Process Dual Problem
# Calculate strains
ep_zb,ep_zm,ep_zs=[],[],[]
ZEB1,ZEB2,ZEB3,ZEB4,ZEB5,ZEB6=[],[],[],[],[],[]
ZEM1,ZEM2,ZEM3,ZEM4,ZEM5,ZEM6=[],[],[],[],[],[]
ZES1,ZES2,ZES3,ZES4,ZES5,ZES6=[],[],[],[],[],[]
ZEB1zz,ZEB2zz,ZEB3zz,ZEB4zz,ZEB5zz,ZEB6zz=[],[],[],[],[],[]
ZEM1zz,ZEM2zz,ZEM3zz,ZEM4zz,ZEM5zz,ZEM6zz=[],[],[],[],[],[]
ZES1zz,ZES2zz,ZES3zz,ZES4zz,ZES5zz,ZES6zz=[],[],[],[],[],[]
for p in range(6):
    ep_zb.append([]),ep_zm.append([]),ep_zs.append([])
    for i in range(Nele):
        ep_zb[p].append([])
        ep_zm[p].append([])
        ep_zs[p].append([])
        for j in range(NGP):
            (indx,indy)=np.where((GPb==j+1)*(ELEb==i+1))
            ep_zb[p][i].append((Bb[indx[0]][indy[0]]*ehat[indx[0]]*Zsp[p]).todense())
            ep_zm[p][i].append((Bm[indx[0]][indy[0]]*ehat[indx[0]]*Zsp[p]).todense())
            ep_zs[p][i].append((Bs[indx[0]][indy[0]]*ehat[indx[0]]*Zsp[p]).todense())
    # Separate into individual arrays
    ZEB1a,ZEB2a,ZEB3a=np.zeros((Ngpt,NQ)),np.zeros((Ngpt,NQ)),np.zeros((Ngpt,NQ))
    ZEB4a,ZEB5a,ZEB6a=np.zeros((Ngpt,NQ)),np.zeros((Ngpt,NQ)),np.zeros((Ngpt,NQ))
    ZEM1a,ZEM2a,ZEM3a=np.zeros((Ngpt,NQ)),np.zeros((Ngpt,NQ)),np.zeros((Ngpt,NQ))
    ZEM4a,ZEM5a,ZEM6a=np.zeros((Ngpt,NQ)),np.zeros((Ngpt,NQ)),np.zeros((Ngpt,NQ))
    ZES1a,ZES2a,ZES3a=np.zeros((Ngpt,NQ)),np.zeros((Ngpt,NQ)),np.zeros((Ngpt,NQ))
    ZES4a,ZES5a,ZES6a=np.zeros((Ngpt,NQ)),np.zeros((Ngpt,NQ)),np.zeros((Ngpt,NQ))   
    for i in range(Ngpt):
        (indx,indy)=np.where((ELEb==ELE_ar[i])*(GPb==GP_ar[i]))
        ZEB1a[i,:],ZEB2a[i,:],ZEB3a[i,:]=ep_zb[p][indx[0]][indy[0]][0],ep_zb[p][indx[0]][indy[0]][1],ep_zb[p][indx[0]][indy[0]][2]
        ZEB4a[i,:],ZEB5a[i,:],ZEB6a[i,:]=ep_zb[p][indx[0]][indy[0]][3],ep_zb[p][indx[0]][indy[0]][4],ep_zb[p][indx[0]][indy[0]][5]
        ZEM1a[i,:],ZEM2a[i,:],ZEM3a[i,:]=ep_zm[p][indx[0]][indy[0]][0],ep_zm[p][indx[0]][indy[0]][1],ep_zm[p][indx[0]][indy[0]][2]
        ZEM4a[i,:],ZEM5a[i,:],ZEM6a[i,:]=ep_zm[p][indx[0]][indy[0]][3],ep_zm[p][indx[0]][indy[0]][4],ep_zm[p][indx[0]][indy[0]][5]
        ZES1a[i,:],ZES2a[i,:],ZES3a[i,:]=ep_zs[p][indx[0]][indy[0]][0],ep_zs[p][indx[0]][indy[0]][1],ep_zs[p][indx[0]][indy[0]][2]
        ZES4a[i,:],ZES5a[i,:],ZES6a[i,:]=ep_zs[p][indx[0]][indy[0]][3],ep_zs[p][indx[0]][indy[0]][4],ep_zs[p][indx[0]][indy[0]][5]
    ZEB1.append(ZEB1a),ZEB2.append(ZEB2a),ZEB3.append(ZEB3a)
    ZEB4.append(ZEB4a),ZEB5.append(ZEB5a),ZEB6.append(ZEB6a)
    ZEM1.append(ZEM1a),ZEM2.append(ZEM2a),ZEM3.append(ZEM3a)
    ZEM4.append(ZEM4a),ZEM5.append(ZEM5a),ZEM6.append(ZEM6a)
    ZES1.append(ZES1a),ZES2.append(ZES2a),ZES3.append(ZES3a)
    ZES4.append(ZES4a),ZES5.append(ZES5a),ZES6.append(ZES6a)
    
    ZEB=np.concatenate((ZEB1a,ZEB2a,ZEB3a,ZEB4a,ZEB5a,ZEB6a),axis=1)    
    ZEM=np.concatenate((ZEM1a,ZEM2a,ZEM3a,ZEM4a,ZEM5a,ZEM6a),axis=1)
    ZES=np.concatenate((ZES1a,ZES2a,ZES3a,ZES4a,ZES5a,ZES6a),axis=1)
    ZET=np.concatenate((ZEB,ZEM,ZES),axis=1)
    
    # Perform ZZ recovery (Currently NN approach)
    A=FUN.ZZ_NN3D(Xn,Yn,Zn,Noden,X,Y,Z,ZET,ELE_ar,Nodal_DOF,ind_ele,ELEb[:,0].reshape(-1))
    ZEB1zz.append(A[:,:NQ]),ZEB2zz.append(A[:,1*NQ:2*NQ]),ZEB3zz.append(A[:,2*NQ:3*NQ])
    ZEB4zz.append(A[:,3*NQ:4*NQ]),ZEB5zz.append(A[:,4*NQ:5*NQ]),ZEB6zz.append(A[:,5*NQ:6*NQ])
    ZEM1zz.append(A[:,6*NQ:7*NQ]),ZEM2zz.append(A[:,7*NQ:8*NQ]),ZEM3zz.append(A[:,8*NQ:9*NQ])
    ZEM4zz.append(A[:,9*NQ:10*NQ]),ZEM5zz.append(A[:,10*NQ:11*NQ]),ZEM6zz.append(A[:,11*NQ:12*NQ])
    ZES1zz.append(A[:,12*NQ:13*NQ]),ZES2zz.append(A[:,13*NQ:14*NQ]),ZES3zz.append(A[:,14*NQ:15*NQ])
    ZES4zz.append(A[:,15*NQ:16*NQ]),ZES5zz.append(A[:,16*NQ:17*NQ]),ZES6zz.append(A[:,17*NQ:18*NQ])
print('End of Dual ZZ')
#%% GOEE
GOEE=[]
for p in range(6):
    # Bending
    GOEE_b,GOEE_m,GOEE_s=np.zeros((Nele,NQ)),np.zeros((Nele,NQ)),np.zeros((Nele,NQ))
    for k in range(NQ):
        for i in range(Nele):    
            # Bending
            a=np.zeros(NGP)
            Tf=np.concatenate((np.concatenate((T[i],O),axis=0),np.concatenate((O,T[i]),axis=0)),axis=1)
            Cb=Tf[:,Bend_dof]*Cbend*Tf[:,Bend_dof].T
            # Primal
            P1,P2,P3=np.matrix(EB1zz[ind_ele[i]]).reshape((4,1)),np.matrix(EB2zz[ind_ele[i]]).reshape((4,1)),np.matrix(EB3zz[ind_ele[i]]).reshape((4,1))
            P4,P5,P6=np.matrix(EB4zz[ind_ele[i]]).reshape((4,1)),np.matrix(EB5zz[ind_ele[i]]).reshape((4,1)),np.matrix(EB6zz[ind_ele[i]]).reshape((4,1))
            # Dual
            D1,D2,D3=np.matrix(ZEB1zz[p][ind_ele[i],k]).reshape((4,1)),np.matrix(ZEB2zz[p][ind_ele[i],k]).reshape((4,1)),np.matrix(ZEB3zz[p][ind_ele[i],k]).reshape((4,1))
            D4,D5,D6=np.matrix(ZEB4zz[p][ind_ele[i],k]).reshape((4,1)),np.matrix(ZEB5zz[p][ind_ele[i],k]).reshape((4,1)),np.matrix(ZEB6zz[p][ind_ele[i],k]).reshape((4,1))
            # Ease of Use
            P=np.concatenate((P1,P2,P3,P4,P5,P6),axis=1)
            D=np.concatenate((D1,D2,D3,D4,D5,D6),axis=1)
            for j in range(NGP):
                # Primal
                PrimeZZ=(N(xi_i[j],eta_i[j])*P).T
                Prime=ep_ub[i][j]
                # Dual
                DualZZ=(N(xi_i[j],eta_i[j])*D).T
                Dual=ep_zb[p][i][j][:,k]
                # Calculate
                a[j]=(PrimeZZ-Prime).T*Cb*(DualZZ-Dual)*Ar[i,j]*t
            GOEE_b[i,k]=np.sum(a)
            # Membrane
            a=np.zeros(NGP)
            Cm=Tf[:,Memb_dof]*Cmemb*Tf[:,Memb_dof].T
            # Primal
            P1,P2,P3=np.matrix(EM1zz[ind_ele[i]]).reshape((4,1)),np.matrix(EM2zz[ind_ele[i]]).reshape((4,1)),np.matrix(EM3zz[ind_ele[i]]).reshape((4,1))
            P4,P5,P6=np.matrix(EM4zz[ind_ele[i]]).reshape((4,1)),np.matrix(EM5zz[ind_ele[i]]).reshape((4,1)),np.matrix(EM6zz[ind_ele[i]]).reshape((4,1))
            # Dual
            D1,D2,D3=np.matrix(ZEM1zz[p][ind_ele[i],k]).reshape((4,1)),np.matrix(ZEM2zz[p][ind_ele[i],k]).reshape((4,1)),np.matrix(ZEM3zz[p][ind_ele[i],k]).reshape((4,1))
            D4,D5,D6=np.matrix(ZEM4zz[p][ind_ele[i],k]).reshape((4,1)),np.matrix(ZEM5zz[p][ind_ele[i],k]).reshape((4,1)),np.matrix(ZEM6zz[p][ind_ele[i],k]).reshape((4,1))
            # Ease of Use
            P=np.concatenate((P1,P2,P3,P4,P5,P6),axis=1)
            D=np.concatenate((D1,D2,D3,D4,D5,D6),axis=1)
            for j in range(NGP):
                # Primal
                PrimeZZ=(N(xi_i[j],eta_i[j])*P).T
                Prime=ep_um[i][j]
                # Dual
                DualZZ=(N(xi_i[j],eta_i[j])*D).T
                Dual=ep_zm[p][i][j][:,k]
                # Calculate
                a[j]=(PrimeZZ-Prime).T*Cm*(DualZZ-Dual)*Ar[i,j]*t
            GOEE_m[i,k]=np.sum(a)
            # Shear
            a=np.zeros(NGP)
            Cs=Tf[:,Sher_dof]*Csher*Tf[:,Sher_dof].T
            # Primal
            P1,P2,P3=np.matrix(ES1zz[ind_ele[i]]).reshape((4,1)),np.matrix(ES2zz[ind_ele[i]]).reshape((4,1)),np.matrix(ES3zz[ind_ele[i]]).reshape((4,1))
            P4,P5,P6=np.matrix(ES4zz[ind_ele[i]]).reshape((4,1)),np.matrix(ES5zz[ind_ele[i]]).reshape((4,1)),np.matrix(ES6zz[ind_ele[i]]).reshape((4,1))
            # Dual
            D1,D2,D3=np.matrix(ZES1zz[p][ind_ele[i],k]).reshape((4,1)),np.matrix(ZES2zz[p][ind_ele[i],k]).reshape((4,1)),np.matrix(ZES3zz[p][ind_ele[i],k]).reshape((4,1))
            D4,D5,D6=np.matrix(ZES4zz[p][ind_ele[i],k]).reshape((4,1)),np.matrix(ZES5zz[p][ind_ele[i],k]).reshape((4,1)),np.matrix(ZES6zz[p][ind_ele[i],k]).reshape((4,1))
            # Ease of Use
            P=np.concatenate((P1,P2,P3,P4,P5,P6),axis=1)
            D=np.concatenate((D1,D2,D3,D4,D5,D6),axis=1)
            for j in range(NGP):
                # Primal
                PrimeZZ=(N(xi_i[j],eta_i[j])*P).T
                Prime=ep_us[i][j]
                # Dual
                DualZZ=(N(xi_i[j],eta_i[j])*D).T
                Dual=ep_zs[p][i][j][:,k]
                # Calculate
                a[j]=(PrimeZZ-Prime).T*Cs*(DualZZ-Dual)*Ar[i,j]*t
            GOEE_s[i,k]=np.sum(a)
    # Total
    GOEE.append(GOEE_b+GOEE_m+GOEE_s)
print('End of GOEE')
#%% Gaussian Process for GOEE
tm=time.time()
GPDOF=5
# Setup Matrices
# Remove zero stiffness DOF
ind,indz=np.arange(0,len(gDOF)),[]
for i in range(len(gDOF)):
    if np.all(K[:,i].todense()==0.):
        indz.append(i)
indz=np.array(indz).reshape(-1)
ind=np.delete(ind,indz)
Kred=K[ind][:,ind]
Mred=M[ind][:,ind]
DOFred=np.delete(gDOF,indz)
Qred=np.delete(H[:GPDOF*NQ,:],indz,axis=1)  
# Apply BC
a=np.where(np.isclose(Yn,0.))[0]
indy=np.zeros(3*len(a),dtype=int)
ind=np.arange(0,len(DOFred))
for i in range(len(a)):
    indy[3*i]=np.where(DOFred==Noden[a[i]]+.1)[0]
    indy[3*i+1]=np.where(DOFred==Noden[a[i]]+.2)[0]
    indy[3*i+2]=np.where(DOFred==Noden[a[i]]+.3)[0]
ind=np.delete(ind,indy)
Kred=Kred[ind][:,ind]
Mred=Mred[ind][:,ind]
DOFred=np.delete(DOFred,indy)
Qred=np.delete(Qred,indy,axis=1)  
# Calculate Prior
A=spinv((Mred+beta**2*Kred).tocsc())
Sigma=(alpha**2*A*Mred*A).todense()
# Arange Training values to match Q vectors
Ytrain,Train=np.matrix(np.zeros((GPDOF*NQ,1))),[]
for k in range(GPDOF):
    Ytrain[NQ*k:NQ*(k+1),:]=np.matrix(np.sum(GOEE[k],axis=0)).T
    Train.append(np.sum(GOEE[k],axis=0))
# Setup for conditional distribution calculation
Hr=sparse.csc_matrix(Qred)
Sig_eps=sparse.csc_matrix(np.diag((0.*np.array(np.sum(np.abs(Zh),axis=0)).reshape(-1)**2)[:GPDOF*NQ]))
Kinv=np.linalg.inv(Hr*Sigma*Hr.T+Sig_eps)
# Calculate Conditional Distribution
Fmean=np.array(Sigma*Hr.T*Kinv*Ytrain).reshape(-1)
Fcov=Sigma-Sigma*Hr.T*Kinv*Hr*Sigma
# Expand from reduced to full coordinates
FMeanNodal,FCovNodal=[],[]
GPMean,GPCov,Sig_expand=np.zeros(nDOF),np.zeros((nDOF,nDOF)),np.zeros((nDOF,nDOF))
a,c=[],[]
for i in range(nDOF):
    if gDOF[i] in DOFred:
        ind=np.where(DOFred==gDOF[i])[0]
        a.append(ind)
        c.append(i)
        GPMean[i]=Fmean[ind]
b,d=np.array(a,dtype=int).reshape(-1),np.array(c,dtype=int).reshape(-1)
for i in range(len(b)):
    GPCov[d[i],d]=np.array(Fcov[b[i],:]).reshape(-1)
    Sig_expand[d[i],d]=np.array(Sigma[b[i],:]).reshape(-1)
# Separate into directions
for k in range(6):
    FMeanNodal.append(GPMean[k::6])
    FCovNodal.append(GPCov[k::6][:,k::6])
print('Time for GP = %.1f Minutes'%((time.time()-tm)/60))
#%% Visualize in 3D
d=1
ind=np.where(Zn==0)[0]
A=np.array(QOI_Center)
xt,yt,zt=A[:,0],A[:,1],A[:,2]
indt=np.where(zt==0)[0]
plt.figure()
ax=plt.axes(projection='3d')
ax.plot_trisurf(Xn[ind],Yn[ind],FMeanNodal[d][ind]*1e3,color='b',label='GP Mean')
ax.plot3D(xt[indt],yt[indt],np.array(Train[d][indt]*1e3).reshape(-1),'ro',label='GOEE Solutions')
plt.xlabel('X-Direction')
plt.ylabel('Y-Direction')
plt.title('GP Mean Validation [mm]')
plt.show()

#plt.figure()
#ax=plt.axes(projection='3d')
#ax.plot_trisurf(Xn[ind],Yn[ind],FMeanNodal[d][ind]*1e3,color='b')
#ax.plot_trisurf(Xn[ind],Yn[ind],FMeanNodal[d][ind]*1e3+np.sqrt(np.abs(np.diag(FCovNodal[d]))[ind])*1e3,color='g')
#ax.plot_trisurf(Xn[ind],Yn[ind],FMeanNodal[d][ind]*1e3-np.sqrt(np.abs(np.diag(FCovNodal[d]))[ind])*1e3,color='g')
#plt.xlabel('X-Direction')
#plt.ylabel('Y-Direction')
#plt.title('Predictive Mean [mm]')
#plt.show()

