# README #

The Python Standalone of the MARQUESS GOEE is a version of the work done for MARQUESS GOEE designed to be independent of the MARQUESS Plug-in for ABAQUS. This repository contains the central functions, an example runscript, and an example system for illustrative purposes. The example system is designed with several feature models to adjust and to better understand the multi-scale aspects of the standalone. More details are available in the User's Guide also available in this repository.

This requires a python installation, such as anaconda. This was designed in Python 2.7. It has been tested on a few systems that contain:
1) Python 2.7.16, Numpy 1.13.1, Scipy 0.19.1
2) Python 2.7.16, Numpy 1.16.6, Scipy 1.2.1
3) Python 2.7.16, Numpy 1.16.5, Scipy 1.2.1

There are two priors used in this work. The folder names signify the priors used in the analysis. As a note, there are two versions of the Matern prior. The tested version is Matern-DistanceBased.py that uses the classical definition of the Matern prior. The other version, Matern_MassStiffBased.py, is a test for utilizing the mass and stiffness matrices in the definition of the prior. This accounts for geometric dependencies, as is expected to be more realistic.

This also attempts to use triangular elements and a single strain-displacement matrix for a flat plat. These run scripts validate to there being no difference between using a single or triple component version of the GOEE. There is a slight difference between triangular and quadrilateral elements, but this is mainly due to the difference in elements. This would be equivalent to replacing a S4 element with 2 S3R elements.