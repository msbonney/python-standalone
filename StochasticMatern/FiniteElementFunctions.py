"""
This file contains all the classes and functions related to setting up the 2D
flat plate model for Matern Kernel study

Created on Friday 29 Mar 2021
last Updated: 
    
Version Notes: 
Designed to work with 3D FE meshes imported from abaqus

@author: MK
"""
import ModelDefinitions as MD
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import scipy.sparse.linalg
from scipy.sparse import csc_matrix,lil_matrix

class Node:
    
    def __init__(self,coordinates,label):
        
        # An object of type node has a unique label and coordinates
        self.coordinates = coordinates
        self.label = label
        
        
class ElementQ4:
    
    def __init__(self, node1, node2, node3, node4, label):
        
        # A Q4 element object has 4 nodes and a unique label
        self.node1 = node1
        self.node2 = node2
        self.node3 = node3
        self.node4 = node4
        self.label = label


class SolidElement:
    
    def __init__(self, node1, node2, node3, node4, node5, node6, node7, node8, label):
        
        # A solid element object has 8 nodes and a unique label
        self.node1 = node1
        self.node2 = node2
        self.node3 = node3
        self.node4 = node4
        self.node5 = node5
        self.node6 = node6
        self.node7 = node7
        self.node8 = node8
        self.label = label
        
    def Volume(self):
        
        # Gauss point location and weights for 2 by 2 by 2 rule
        coord = np.array([-1./np.sqrt(3), 1./np.sqrt(3)]); w = np.array([1., 1.]);
        
        # Calculation of triple integral from Peter I. Kattan Eq. 16.13
        vol = 0.;
        for k in range(2):
            for l in range(2):
                for j in range(2):
                    # Gauss coordinate
                    xi = coord[k]; eta = coord[l]; zeta = coord[j]
                    # Gauss weights 
                    wk = w[k]; wl = w[l]; wj = w[j]
                    # Jacobian matrix
                    J = JacobianMatrix(self,xi,eta,zeta)
                    detJ = np.linalg.det(J)
                    # Volume
                    vol = vol + wk*wl*wj*detJ
                    
        return vol


def RunPrimal(nodes,elements):
    
    # Number of degrees of freedom
    dofs = MD.dim*len(nodes)
    
    # Caclulate constitutive matrix
    C = MD.material.ConstitutiveMatrix(MD.dim)
    
    # Calculate stiffness matrix
    print('Assembling stiffness matrix')
    K = GlobalStiffnessMatrix(elements,dofs,C)
    
    # Calculate mass matrix
    M = ConsistentMassMatrix(elements,dofs) 
    
    # Apply boundary conditions
    print('Applying boundary conditions')
    # Apply BCs
    redK,mapDofs = FixDofs(K,dofs,'delete')
    redM = FixDofs(M,dofs,'delete')[0]
    
    # Forces for flat panel 
    f = np.zeros([dofs,1])
    f = ApplyAbaqusForce(f)
    f = np.delete(f,MD.fixedDofs)
    
    # Calculate solution
    print('Computing solution')
    u = scipy.sparse.linalg.spsolve(redK,f)
    #res = np.linalg.norm(redK*u - f);
    #print(res)
    
    u = ExpandDofs(u,dofs,mapDofs)
    #PlotDisplacements(nodes,u)
    
    # Postprocess results
    print('Postprocessing')
    strain,stress = StressRecovery(elements,C,u)
    
    
    return redK,redM,strain,u,mapDofs


def ShapeFunctions(xi,eta,zeta=0):
    
    # Model definition
    dim = MD.dim
    
    if dim==2:
        # Shape functions for bilinear quadrilateral
        N1 = (1/4.)*(1 - xi)*(1 - eta)
        N2 = (1/4.)*(1 + xi)*(1 - eta)
        N3 = (1/4.)*(1 + xi)*(1 + eta)
        N4 = (1/4.)*(1 - xi)*(1 + eta)
        
        N = np.array([[N1, 0., N2, 0., N3, 0., N4, 0.],
                      [0., N1, 0., N2, 0., N3, 0., N4]])
    
    else:
        # Shape functions for solid element
        N1 = (1-xi)*(1-eta)*(1+zeta)/8;
        N2 = (1-xi)*(1-eta)*(1-zeta)/8;
        N3 = (1-xi)*(1+eta)*(1-zeta)/8;
        N4 = (1-xi)*(1+eta)*(1+zeta)/8;
        N5 = (1+xi)*(1-eta)*(1+zeta)/8;
        N6 = (1+xi)*(1-eta)*(1-zeta)/8;
        N7 = (1+xi)*(1+eta)*(1-zeta)/8;
        N8 = (1+xi)*(1+eta)*(1+zeta)/8;
        
        N = np.array([[N1, 0., 0., N2, 0., 0., N3, 0., 0., N4, 0., 0., N5, 0., 0., N6, 0., 0., N7, 0., 0., N8, 0., 0.],
                      [0., N1, 0., 0., N2, 0., 0., N3, 0., 0., N4, 0., 0., N5, 0., 0., N6, 0., 0., N7, 0., 0., N8, 0.],
                      [0., 0., N1, 0., 0., N2, 0., 0., N3, 0., 0., N4, 0., 0., N5, 0., 0., N6, 0., 0., N7, 0., 0., N8]])
    
    return N


def ShapeFunctionDerivatives(xi,eta,zeta):
    
    dim = MD.dim
    
    if dim==2:
        # Derivatives of shape functions of bilinear quadrilateral
        dN1dxi  = (1/4.)*(-1)*(1 - eta)
        dN1deta = (1/4.)*(1 - xi)*(-1)
        
        dN2dxi  = (1/4.)*(1)*(1 - eta)
        dN2deta = (1/4.)*(1 + xi)*(-1)
        
        dN3dxi  = (1/4.)*(1)*(1 + eta)
        dN3deta = (1/4.)*(1 + xi)*(1)
        
        dN4dxi  = (1/4.)*(-1)*(1 + eta)
        dN4deta = (1/4.)*(1 - xi)*(1)
        
        P = np.array([[ dN1dxi,  dN2dxi,  dN3dxi,  dN4dxi],
                      [dN1deta, dN2deta, dN3deta, dN4deta]])
    
    else:
        # Derivatives of shape functions of solid element
        dN1dxi   = (  -1)*(1-eta)*(1+zeta)/8;
        dN1deta  = (1-xi)*(   -1)*(1+zeta)/8;
        dN1dzeta = (1-xi)*(1-eta)*(     1)/8;
        
        dN2dxi   = (  -1)*(1-eta)*(1-zeta)/8;
        dN2deta  = (1-xi)*(   -1)*(1-zeta)/8;
        dN2dzeta = (1-xi)*(1-eta)*(    -1)/8;
        
        dN3dxi   = (  -1)*(1+eta)*(1-zeta)/8;
        dN3deta  = (1-xi)*(    1)*(1-zeta)/8;
        dN3dzeta = (1-xi)*(1+eta)*(    -1)/8;
        
        dN4dxi   = (  -1)*(1+eta)*(1+zeta)/8;
        dN4deta  = (1-xi)*(    1)*(1+zeta)/8;
        dN4dzeta = (1-xi)*(1+eta)*(     1)/8;
        
        dN5dxi   = (   1)*(1-eta)*(1+zeta)/8;
        dN5deta  = (1+xi)*(   -1)*(1+zeta)/8;
        dN5dzeta = (1+xi)*(1-eta)*(     1)/8;
        
        dN6dxi   = (   1)*(1-eta)*(1-zeta)/8;
        dN6deta  = (1+xi)*(   -1)*(1-zeta)/8;
        dN6dzeta = (1+xi)*(1-eta)*(    -1)/8;
        
        dN7dxi   = (   1)*(1+eta)*(1-zeta)/8;
        dN7deta  = (1+xi)*(    1)*(1-zeta)/8;
        dN7dzeta = (1+xi)*(1+eta)*(    -1)/8;
        
        dN8dxi   = (   1)*(1+eta)*(1+zeta)/8;
        dN8deta  = (1+xi)*(    1)*(1+zeta)/8;
        dN8dzeta = (1+xi)*(1+eta)*(     1)/8;
        
        P = np.array([[  dN1dxi,   dN2dxi,   dN3dxi,   dN4dxi,   dN5dxi,   dN6dxi,   dN7dxi,   dN8dxi ],
                      [ dN1deta,  dN2deta,  dN3deta,  dN4deta,  dN5deta,  dN6deta,  dN7deta,  dN8deta ],
                      [dN1dzeta, dN2dzeta, dN3dzeta, dN4dzeta, dN5dzeta, dN6dzeta, dN7dzeta, dN8dzeta ]])
        

    return P


def JacobianMatrix(element,xi,eta,zeta=0):
    
    dim = MD.dim
    
    # Calculate P matrix (derivatives of shape function)
    P = ShapeFunctionDerivatives(xi,eta,zeta)  
    
    # X - matrix (coordinates of nodes)
    if dim==2:
        #  Nodal coordinates for bilinear quadrilateral
        X = np.array([element.node1.coordinates,
                      element.node2.coordinates,
                      element.node3.coordinates,
                      element.node4.coordinates])
    else:
        # Nodal coordinates for 3D solid element
        X = np.array([element.node1.coordinates,
                      element.node2.coordinates,
                      element.node3.coordinates,
                      element.node4.coordinates,
                      element.node5.coordinates,
                      element.node6.coordinates,
                      element.node7.coordinates,
                      element.node8.coordinates])
    
    J = np.dot(P,X)
    
    return J
    
def StrainDisplacementMatrix(element,xi,eta,zeta=0,Nbar=0):
    
    dim = MD.dim
    J = JacobianMatrix(element,xi,eta,zeta)
    detJ = np.linalg.det(J)
    invJ = np.linalg.inv(J)
    
    P = ShapeFunctionDerivatives(xi,eta,zeta)
    dNdxy = np.dot(invJ,P)
    
    if dim==2:
        # Strain displacement matrix for bilinear quadrilateral
        dN1dx = dNdxy[0,0]; dN2dx = dNdxy[0,1]; dN3dx = dNdxy[0,2]; dN4dx = dNdxy[0,3];
        dN1dy = dNdxy[1,0]; dN2dy = dNdxy[1,1]; dN3dy = dNdxy[1,2]; dN4dy = dNdxy[1,3];
        
        Bbar = np.array([[dN1dx,    0,dN2dx,    0,dN3dx,    0,dN4dx,    0],
                      [    0,dN1dy,    0,dN2dy,    0,dN3dy,    0,dN4dy],
                      [dN1dy,dN1dx,dN2dy,dN2dx,dN3dy,dN3dx,dN4dy,dN4dx]])
    else:
        # Strain displacement matrix for 3D solid element - B-bar formulation - Thomas J.R.Hughes Section 4.5.2
        dN1dx = dNdxy[0,0]; dN2dx = dNdxy[0,1]; dN3dx = dNdxy[0,2]; dN4dx = dNdxy[0,3]; dN5dx = dNdxy[0,4]; dN6dx = dNdxy[0,5]; dN7dx = dNdxy[0,6]; dN8dx = dNdxy[0,7];
        dN1dy = dNdxy[1,0]; dN2dy = dNdxy[1,1]; dN3dy = dNdxy[1,2]; dN4dy = dNdxy[1,3]; dN5dy = dNdxy[1,4]; dN6dy = dNdxy[1,5]; dN7dy = dNdxy[1,6]; dN8dy = dNdxy[1,7];
        dN1dz = dNdxy[2,0]; dN2dz = dNdxy[2,1]; dN3dz = dNdxy[2,2]; dN4dz = dNdxy[2,3]; dN5dz = dNdxy[2,4]; dN6dz = dNdxy[2,5]; dN7dz = dNdxy[2,6]; dN8dz = dNdxy[2,7];
        # Standard strain displacement matrix for solid element 
        B = np.array([[dN1dx,     0,     0, dN2dx,     0,     0, dN3dx,     0,     0, dN4dx,     0,     0, dN5dx,     0,     0, dN6dx,     0,     0, dN7dx,     0,     0, dN8dx,     0,     0],
                      [    0, dN1dy,     0,     0, dN2dy,     0,     0, dN3dy,     0,     0, dN4dy,     0,     0, dN5dy,     0,     0, dN6dy,     0,     0, dN7dy,     0,     0, dN8dy,     0],
                      [    0,     0, dN1dz,     0,     0, dN2dz,     0,     0, dN3dz,     0,     0, dN4dz,     0,     0, dN5dz,     0,     0, dN6dz,     0,     0, dN7dz,     0,     0, dN8dz],
                      [dN1dy, dN1dx,     0, dN2dy, dN2dx,     0, dN3dy, dN3dx,     0, dN4dy, dN4dx,     0, dN5dy, dN5dx,     0, dN6dy, dN6dx,     0, dN7dy, dN7dx,     0, dN8dy, dN8dx,     0],
                      [    0, dN1dz, dN1dy,     0, dN2dz, dN2dy,     0, dN3dz, dN3dy,     0, dN4dz, dN4dy,     0, dN5dz, dN5dy,     0, dN6dz, dN6dy,     0, dN7dz, dN7dy,     0, dN8dz, dN8dy],
                      [dN1dz,     0, dN1dx, dN2dz,     0, dN2dx, dN3dz,     0, dN3dx, dN4dz,     0, dN4dx, dN5dz,     0, dN5dx, dN6dz,     0, dN6dx, dN7dz,     0, dN7dx, dN8dz,     0, dN8dx]])
                    
    
        # Derivatives of shape functions integrated over the element and normalised by element volume
        dN1dxbar = Nbar[0,0]; dN2dxbar = Nbar[0,1]; dN3dxbar = Nbar[0,2]; dN4dxbar = Nbar[0,3]; dN5dxbar = Nbar[0,4]; dN6dxbar = Nbar[0,5]; dN7dxbar = Nbar[0,6]; dN8dxbar = Nbar[0,7];
        dN1dybar = Nbar[1,0]; dN2dybar = Nbar[1,1]; dN3dybar = Nbar[1,2]; dN4dybar = Nbar[1,3]; dN5dybar = Nbar[1,4]; dN6dybar = Nbar[1,5]; dN7dybar = Nbar[1,6]; dN8dybar = Nbar[1,7];
        dN1dzbar = Nbar[2,0]; dN2dzbar = Nbar[2,1]; dN3dzbar = Nbar[2,2]; dN4dzbar = Nbar[2,3]; dN5dzbar = Nbar[2,4]; dN6dzbar = Nbar[2,5]; dN7dzbar = Nbar[2,6]; dN8dzbar = Nbar[2,7];
        # Dilatational part of B-bar matrix for 
        BbarDil = np.array([[dN1dxbar, dN1dybar, dN1dzbar, dN2dxbar, dN2dybar, dN2dzbar, dN3dxbar, dN3dybar, dN3dzbar, dN4dxbar, dN4dybar, dN4dzbar, dN5dxbar, dN5dybar, dN5dzbar, dN6dxbar, dN6dybar, dN6dzbar, dN7dxbar, dN7dybar, dN7dzbar, dN8dxbar, dN8dybar, dN8dzbar],
                            [dN1dxbar, dN1dybar, dN1dzbar, dN2dxbar, dN2dybar, dN2dzbar, dN3dxbar, dN3dybar, dN3dzbar, dN4dxbar, dN4dybar, dN4dzbar, dN5dxbar, dN5dybar, dN5dzbar, dN6dxbar, dN6dybar, dN6dzbar, dN7dxbar, dN7dybar, dN7dzbar, dN8dxbar, dN8dybar, dN8dzbar],
                            [dN1dxbar, dN1dybar, dN1dzbar, dN2dxbar, dN2dybar, dN2dzbar, dN3dxbar, dN3dybar, dN3dzbar, dN4dxbar, dN4dybar, dN4dzbar, dN5dxbar, dN5dybar, dN5dzbar, dN6dxbar, dN6dybar, dN6dzbar, dN7dxbar, dN7dybar, dN7dzbar, dN8dxbar, dN8dybar, dN8dzbar],
                            [       0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0],
                            [       0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0],
                            [       0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0]])
        
        # Dilatational part of standard B-matrix
        BDil = np.array([[dN1dx, dN1dy, dN1dz, dN2dx, dN2dy, dN2dz, dN3dx, dN3dy, dN3dz, dN4dx, dN4dy, dN4dz, dN5dx, dN5dy, dN5dz, dN6dx, dN6dy, dN6dz, dN7dx, dN7dy, dN7dz, dN8dx, dN8dy, dN8dz],
                         [dN1dx, dN1dy, dN1dz, dN2dx, dN2dy, dN2dz, dN3dx, dN3dy, dN3dz, dN4dx, dN4dy, dN4dz, dN5dx, dN5dy, dN5dz, dN6dx, dN6dy, dN6dz, dN7dx, dN7dy, dN7dz, dN8dx, dN8dy, dN8dz],
                         [dN1dx, dN1dy, dN1dz, dN2dx, dN2dy, dN2dz, dN3dx, dN3dy, dN3dz, dN4dx, dN4dy, dN4dz, dN5dx, dN5dy, dN5dz, dN6dx, dN6dy, dN6dz, dN7dx, dN7dy, dN7dz, dN8dx, dN8dy, dN8dz],
                         [    0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0],
                         [    0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0],
                         [    0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0]])

        # Bbar = deviatoric part of standard B + dilatational part of B-bar
        Bbar = B - (1./3.)*BDil + (1./3.)*BbarDil 
    
       
    return Bbar,detJ

def NbarDilatational(element):
    
    # Initialise N-bar matrix
    Nbar = np.zeros([3,8])
    
    # Gauss quadrature locations and weights
    coord = np.array([1./np.sqrt(3), -1./np.sqrt(3)]); w = np.array([1., 1.]);
    # Loop through gauss points and integrate the derivatives of shape functions
    for k in range(2):
        for l in range(2):
            for j in range(2):
                xi = coord[k]; eta = coord[l]; zeta = coord[j]
                wk = w[k]; wl = w[l]; wj = w[j]
                # Calculate Jacobian
                J = JacobianMatrix(element,xi,eta,zeta)
                detJ = np.linalg.det(J)
                invJ = np.linalg.inv(J)
                # Calculate derivative of shape functions with respect to physical coordinates
                P = ShapeFunctionDerivatives(xi,eta,zeta)
                dNdxy = np.dot(invJ,P)
                # Sum up contribution at each Gauss point
                Nbar = Nbar + dNdxy*detJ*wk*wl*wj
    
    # Divide by the volume of the element            
    Nbar = Nbar/element.Volume()
    
    return Nbar

def ElementStiffnessMatrix2D(element,C):
    
    # Gauss point location and weights for 2 by 2 rule
    coord = np.array([1./np.sqrt(3), -1./np.sqrt(3)]); w = np.array([1., 1.]);
    h = MD.modelDim.lz;
    
    # Loop through Gauss points and integrate
    BtrCB = np.zeros([8,8]);
    for k in range(2):
        for l in range(2):
            xi = coord[k]; eta = coord[l]
            wk = w[k]; wl = w[l]
            
            B,detJ = StrainDisplacementMatrix(element,xi,eta)
            BtrCB = BtrCB + np.dot(np.dot(np.transpose(B),C),B)*wk*wl*detJ
            
    elemK = h*BtrCB;
    
    return elemK


def ElementStiffnessMatrix3D(element,C):
    
    # Gauss point location and weights for 2 by 2 by 2 rule
    coord = np.array([1./np.sqrt(3), -1./np.sqrt(3)]); w = np.array([1., 1.]);
    
    # Shape function derivatives for B-bar formulation
    Nbar = NbarDilatational(element);
    
    # Loop through Gauss points and integrate
    BtrCB = np.zeros([24,24]);
    for k in range(2):
        for l in range(2):
            for j in range(2):
                xi = coord[k]; eta = coord[l]; zeta = coord[j]
                wk = w[k]; wl = w[l]; wj = w[j]
                
                B,detJ = StrainDisplacementMatrix(element,xi,eta,zeta,Nbar)
                BtrCB = BtrCB + np.dot(np.dot(np.transpose(B),C),B)*wk*wl*wj*detJ
            
    elemK = BtrCB;
    
    return elemK

    
def GlobalStiffnessMatrix(elements,dofs,C):
    
    dim = MD.dim
    
    # Initialise global stiffness matrix
    globalK = lil_matrix((dofs, dofs), dtype=np.float64)
    
    if dim==2:
        # Loop through all elements and calculate element stiffness matrices
        for element in elements:
            
            elemK = ElementStiffnessMatrix2D(element,C)
            
            # Element freedom table mapping local dofs to global dofs
            eft = np. array([element.node1.label*2,
                             element.node1.label*2+1,
                             element.node2.label*2,
                             element.node2.label*2+1,
                             element.node3.label*2,
                             element.node3.label*2+1,
                             element.node4.label*2,
                             element.node4.label*2+1])
            
            # Assembling element stiffness in global stiffness matrix
            for i in range(8):
                p = eft[i];
                for j in range(8):
                    q = eft[j];
                    globalK[p,q] = globalK[p,q] + elemK[i,j]
                    
    else:
        
        # Loop through all elements and calculate element stiffness matrices
        for element in elements:
            
            elemK = ElementStiffnessMatrix3D(element,C)
            
            # Element freedom table mapping local dofs to global dofs
            eft = np.array([element.node1.label*3,
                            element.node1.label*3+1,
                            element.node1.label*3+2,
                            element.node2.label*3,
                            element.node2.label*3+1,
                            element.node2.label*3+2,
                            element.node3.label*3,
                            element.node3.label*3+1,
                            element.node3.label*3+2,
                            element.node4.label*3,
                            element.node4.label*3+1,
                            element.node4.label*3+2,
                            element.node5.label*3,
                            element.node5.label*3+1,
                            element.node5.label*3+2,
                            element.node6.label*3,
                            element.node6.label*3+1,
                            element.node6.label*3+2,
                            element.node7.label*3,
                            element.node7.label*3+1,
                            element.node7.label*3+2,
                            element.node8.label*3,
                            element.node8.label*3+1,
                            element.node8.label*3+2])
            
            # Assembling element stiffness in global stiffness matrix
            for i in range(24):
                p = eft[i];
                for j in range(24):
                    q = eft[j];
                    globalK[p,q] = globalK[p,q] + elemK[i,j]
        
    
    return globalK
    
def ConsistentMassMatrix(elements,dofs):
    
    # Model definition data
    dim = MD.dim;
    
    # Initialise mass matrix
    consistentM = lil_matrix((dofs, dofs), dtype=np.float64)
    
    if dim == 2:
        # Loop through all elements
        for element in elements:
            
            elemM = ElementMassMatrix2D(element)
            
            # Element freedom table mapping local dofs to global dofs
            eft = np. array([element.node1.label*2,
                             element.node1.label*2+1,
                             element.node2.label*2,
                             element.node2.label*2+1,
                             element.node3.label*2,
                             element.node3.label*2+1,
                             element.node4.label*2,
                             element.node4.label*2+1])
        
            # Assembling element mass in global mass matrix
            for i in range(8):
                p = eft[i];
                for j in range(8):
                    q = eft[j];
                    consistentM[p,q] = consistentM[p,q] + elemM[i,j]
    else:
        # Loop through all elements and calculate element stiffness matrices
        for element in elements:
            
            elemM = ElementMassMatrix3D(element)
            
            # Element freedom table mapping local dofs to global dofs
            eft = np.array([element.node1.label*3,
                            element.node1.label*3+1,
                            element.node1.label*3+2,
                            element.node2.label*3,
                            element.node2.label*3+1,
                            element.node2.label*3+2,
                            element.node3.label*3,
                            element.node3.label*3+1,
                            element.node3.label*3+2,
                            element.node4.label*3,
                            element.node4.label*3+1,
                            element.node4.label*3+2,
                            element.node5.label*3,
                            element.node5.label*3+1,
                            element.node5.label*3+2,
                            element.node6.label*3,
                            element.node6.label*3+1,
                            element.node6.label*3+2,
                            element.node7.label*3,
                            element.node7.label*3+1,
                            element.node7.label*3+2,
                            element.node8.label*3,
                            element.node8.label*3+1,
                            element.node8.label*3+2])
            
            # Assembling element stiffness in global stiffness matrix
            for i in range(24):
                p = eft[i];
                for j in range(24):
                    q = eft[j];
                    consistentM[p,q] = consistentM[p,q] + elemM[i,j]
            
    return consistentM
    

def ElementMassMatrix2D(element):
    
    # Model definition
    h = MD.modelDim.lz; rho = MD.material.rho;
    
    # Gauss point location and weights for 2 by 2 rule
    coord = np.array([1./np.sqrt(3), -1./np.sqrt(3)]); w = np.array([1., 1.]);
    
    # Loop through Gauss points and integrate
    NtrN = np.zeros([8,8]);
    for k in range(2):
        for l in range(2):
            xi = coord[k]; eta = coord[l]
            wk = w[k]; wl = w[l]
            
            J = JacobianMatrix(element,xi,eta)
            detJ = np.linalg.det(J)
            N = ShapeFunctions(xi,eta)
            NtrN = NtrN + np.dot(np.transpose(N),N)*wk*wl*detJ
            
    elemM = h*rho*NtrN;
    
    return elemM


def ElementMassMatrix3D(element):
    
    # Model definition
    rho = MD.material.rho;
    
    # Gauss point location and weights for 2 by 2 rule
    coord = np.array([1./np.sqrt(3), -1./np.sqrt(3)]); w = np.array([1., 1.]);
    
    # Loop through Gauss points and integrate
    NtrN = np.zeros([24,24]);
    for k in range(2):
        for l in range(2):
            for j in range(2):
                xi = coord[k]; eta = coord[l]; zeta = coord[j]
                wk = w[k]; wl = w[l]; wj = w[j]
            
                J = JacobianMatrix(element,xi,eta,zeta)
                detJ = np.linalg.det(J)
                N = ShapeFunctions(xi,eta,zeta)
                NtrN = NtrN + np.dot(np.transpose(N),N)*wk*wl*wj*detJ
            
    elemM = rho*NtrN;
    
    return elemM
    
    
def SuppressDof(nodes,K,M,nodeLabelBC):
    
    dim = MD.dim
    component = 'xyz'; dofsList = [];
    for i in range(dim):
        # Get node objects from the labels
        nodeBC = []
        for node in nodes:
            if node.label in nodeLabelBC[i]:
                nodeBC.append(node)
        
        # Get dofs corresponding to the nodes
        dofsList.append(GetDofs(nodeBC,component[i]))
    
    # Flatten out the list of dofs
    flatDofsList = []
    for sublist in dofsList:
        for item in sublist:
            flatDofsList.append(item)
    
    # Convert to array
    dofsToRemove = np.asarray(flatDofsList)
    postBCInd = [i for i in range(max(M.shape)) if i not in dofsToRemove]
    
    # Remove rigid body dofs from mass
    tempM = np.delete(M,dofsToRemove,axis=1)
    mass  = np.delete(tempM.T,dofsToRemove,axis=1)
    
    # Remove rigid body dofs from stiffness
    tempK = np.delete(K,dofsToRemove,axis=1)
    stiff  = np.delete(tempK.T,dofsToRemove,axis=1)
    
    return mass.T, stiff.T, postBCInd
    

def ApplyForce(cForce,loadDofs,force):
    
    for dof in loadDofs:
        force[dof] = cForce
    
    return np.squeeze(force)

def ApplyAbaqusForce(force):
    
    meshFile = np.load(MD.meshFileName)
    forceArray = meshFile['force']
    for i in range(forceArray.shape[0]):
        node = forceArray[i,0]
        comp = forceArray[i,1]
        dof = int(node*MD.dim + (comp-1))
        force[dof] = forceArray[i,2]
    
    meshFile.close()    
    return force

def FixDofs(K,dofs,method):
    
    fixedDofs = MD.fixedDofs
    
    # List of dofs
    dofsList = np.arange(dofs)
    dofsFree = [dof for dof in dofsList if dof not in fixedDofs]
    mapDofs = np.delete(dofsList,fixedDofs)    
    
    if method=='preconditioning':
        precond = np.sum(K.diagonal())/len(K.diagonal())
        for dof in fixedDofs:
            redK = K.copy()
            redK[dof,:] = 0; redK[:,dof] = 0; 
            redK[dof,dof] = precond
        
    
    elif method=='delete':
        # Create a matrix to identify free dofs
        identifyFree = lil_matrix((dofs-len(fixedDofs),dofs),dtype=np.float64)
        row = 0
        for dof in dofsFree:
            identifyFree[row,dof] = 1.0
            row += 1
        
        # Delete rows
        redK = identifyFree*K*identifyFree.transpose()
        
    return redK.tocsc(), mapDofs
    
    
def GetNodesByBoundingBox(nodes,xMin,xMax,yMin,yMax,zMin=0,zMax=0):
    
    dim = MD.dim
    nodesFound = [];
    
    if dim == 3:
        for node in nodes:
            if xMin < node.coordinates[0] < xMax:
                if yMin < node.coordinates[1] < yMax:
                    if zMin < node.coordinates[2] < zMax:
                        nodesFound.append(node)
    else:
        for node in nodes:
            if xMin < node.coordinates[0] < xMax:
                if yMin < node.coordinates[1] < yMax:
                    nodesFound.append(node)
                    
    return nodesFound


def GetClosestNode(nodes,coordinates):
    
    # tolerance for search
    tol = 1e-6;
    
    # Bounds for search
    xLowBound  = coordinates[0] - tol;
    xHighBound = coordinates[0] + tol;
    yLowBound  = coordinates[1] - tol;
    yHighBound = coordinates[1] + tol;
    zLowBound  = coordinates[2] - tol;
    zHighBound = coordinates[2] + tol;
    for node in nodes:
        
        if xLowBound < node.coordinates[0] < xHighBound:
            if yLowBound < node.coordinates[1] < yHighBound:
                if zLowBound < node.coordinates[2] < zHighBound:
                    return node
    
    return None


def GetDofs(nodes,component):
    
    dim = MD.dim
    
    dofs = []
    if dim == 2:
        if component == 'x':
            for node in nodes:    
                dofs.append(node.label*2)    
        elif  component == 'y':
            for node in nodes:    
                dofs.append(node.label*2+1)
        elif component == 'xy':
            for node in nodes:    
                dofs.append(node.label*2)
                dofs.append(node.label*2+1)
    elif dim == 3:
        if component == 'x':
            for node in nodes:    
                dofs.append(node.label*3)    
        elif  component == 'y':
            for node in nodes:    
                dofs.append(node.label*3+1)
        elif component == 'z':
            for node in nodes:    
                dofs.append(node.label*3+2)
        elif component == 'xy':
            for node in nodes:    
                dofs.append(node.label*3)
                dofs.append(node.label*3+1)
        elif component == 'xz':
            for node in nodes:    
                dofs.append(node.label*3)
                dofs.append(node.label*3+2)
        elif component == 'yz':
            for node in nodes:    
                dofs.append(node.label*3+1)
                dofs.append(node.label*3+2)
        elif component == 'xyz':
            for node in nodes:    
                dofs.append(node.label*3)
                dofs.append(node.label*3+1)
                dofs.append(node.label*3+2)
            
    return dofs

def ImportMesh():
    
    meshFile = np.load(MD.meshFileName)
    nodeArray = meshFile['nodes']
    elementArray = meshFile['elements']
    
    numNodes = (nodeArray.shape)[0]
    numElements = (elementArray.shape)[0]
    
    if MD.dim==2:
        nodes = []; coord = [];
        for i in range(numNodes):
            label = int(nodeArray[i,0])-1
            x     = nodeArray[i,1]
            y     = nodeArray[i,2]
            nodes.append(Node(coordinates=(x,y),label=label))
            coord.append([x,y])
            
        elements = []
        for i in range(numElements):
            label = elementArray[i,0]-1
            node1 = nodes[elementArray[i,1]-1]
            node2 = nodes[elementArray[i,2]-1]
            node3 = nodes[elementArray[i,3]-1]
            node4 = nodes[elementArray[i,4]-1]
            elements.append(ElementQ4(node1,node2,node3,node4,label=label))
            
        #MeshVisualiser(elements)
        
    elif MD.dim==3:
        nodes = []; coord = [];
        for i in range(numNodes):
            label = int(nodeArray[i,0])-1
            x     = nodeArray[i,1]
            y     = nodeArray[i,2]
            z     = nodeArray[i,3]
            nodes.append(Node(coordinates=(x,y,z),label=label))
            coord.append([x,y,z])
            
        elements = []
        for i in range(numElements):
            label = elementArray[i,0]-1
            node1 = nodes[elementArray[i,1]-1]
            node2 = nodes[elementArray[i,2]-1]
            node3 = nodes[elementArray[i,3]-1]
            node4 = nodes[elementArray[i,4]-1]
            node5 = nodes[elementArray[i,5]-1]
            node6 = nodes[elementArray[i,6]-1]
            node7 = nodes[elementArray[i,7]-1]
            node8 = nodes[elementArray[i,8]-1]
            elements.append(SolidElement(node1,node2,node3,node4,node5,node6,node7,node8,label=label))
        
    
    meshFile.close()
    
    return nodes,elements,np.asarray(coord)

def MeshVisualiser(elements):
    
    # Open a new figure
    plt.figure()
    
    # Gauss point location and weights for 2 by 2 rule
    coordNat = np.array([1./np.sqrt(3), -1./np.sqrt(3)]);
    
    for i in range(len(elements)):
        coord = [elements[i].node1.coordinates,
                 elements[i].node2.coordinates,
                 elements[i].node3.coordinates,
                 elements[i].node4.coordinates,
                 elements[i].node1.coordinates]
        
        x,y = zip(*coord)
        plt.scatter(x,y, c='k',s=40)
        plt.plot(x,y,'c')
        nodeLabels = [elements[i].node1.label,
                      elements[i].node2.label,
                      elements[i].node3.label,
                      elements[i].node4.label]
        
        for k, txt in enumerate(nodeLabels):
            plt.annotate(txt, (x[k], y[k]),color='crimson')
       
        # Element center
        xi = 0.; eta = 0;
        N = ShapeFunctions(xi,eta)
        xy = np.array([coord[0][0],coord[0][1],coord[1][0],coord[1][1],
                       coord[2][0],coord[2][1],coord[3][0],coord[3][1]]) 
        centroid = np.dot(N,xy).T
        plt.annotate(i,centroid,color='darkorange')
        
        
        gcoord = np.zeros([4,2]); j=0;
        for k in range(2):
            for l in range(2):
                # Gauss coordinates
                xi = coordNat[k]; eta = coordNat[l];
                N = ShapeFunctions(xi,eta)
                xy = np.array([coord[0][0],coord[0][1],coord[1][0],coord[1][1],coord[2][0],
                               coord[2][1],coord[3][0],coord[3][1]])
                gcoord[j] = np.dot(N,xy).T
                #plt.annotate(j,gcoord[j],color='darkgreen')
                j+=1

        plt.scatter(gcoord[:,0],gcoord[:,1],marker='x',c='k')
    
    plt.show()
    
def StressRecovery(elements,C,u):
    
    # Model definition
    dim = MD.dim
    
    # Gauss point location and weights for 2 by 2 rule
    coord = np.array([1./np.sqrt(3), -1./np.sqrt(3)]); #w = np.array([1., 1.]);
    
    if dim==2:
        # Loop through all elements
        strain = np.zeros([len(elements),4,3])
        stress = np.zeros([len(elements),4,3])
        stressVM = np.zeros([len(elements),4])
        i=0;
        for element in elements:
            # Get the element nodal displacements
            elementNodes = [element.node1,element.node2,element.node3,element.node4]
            globalDofs = GetDofs(elementNodes,'xy')
            ue = u[globalDofs]
            # Loop through Gauss points and integrate
            j=0;
            for k in range(2):
                for l in range(2):
                    # Gauss coordinates
                    xi = coord[k]; eta = coord[l]; 
                    # Strain displacement matrix a t a gauss point
                    B = StrainDisplacementMatrix(element,xi,eta)[0]
                    # Strain and stress at a gauss point
                    strain[i,j,:] = np.dot(B,ue)
                    stress[i,j,:] = np.dot(C,strain[i,j,:])
                    stressVM[i,j] = np.sqrt( stress[i,j,0]**2 + stress[i,j,1]**2 - 
                                    stress[i,j,0]*stress[i,j,1] + 3*stress[i,j,2]**2)
                    j += 1
            
            i += 1
            
    elif dim==3:
        # Loop through all elements
        strain = np.zeros([len(elements),8,6])
        stress = np.zeros([len(elements),8,6])
        i=0;
        for element in elements:
            # Get the element nodal displacements
            elementNodes = [element.node1,element.node2,element.node3,element.node4,
                            element.node5,element.node6,element.node7,element.node8]
            globalDofs = GetDofs(elementNodes,'xyz')
            ue = u[globalDofs]
            # Shape function derivatives for B-bar formulation
            Nbar = NbarDilatational(element);
            # Loop through Gauss points and integrate
            j=0;
            for k in range(2):
                for l in range(2):
                    for m in range(2):
                        # Gauss coordinates
                        xi = coord[k]; eta = coord[l]; zeta = coord[m]
                        # Strain displacement matrix a t a gauss point
                        B = StrainDisplacementMatrix(element,xi,eta,zeta,Nbar)[0]
                        # Strain and stress at a gauss point
                        strain[i,j,:] = np.dot(B,ue)
                        stress[i,j,:] = np.dot(C,strain[i,j,:])
                        j += 1
            i += 1
            
    
    return strain,stress

def ExpandDofs(redU,dofs,mapDofs):
    
    # Full length of dofs
    u = np.zeros(dofs)
    
    # Map dofs from reduced vector to full displacement vector u
    u[mapDofs] = redU
    
    return u

def PlotDisplacements(nodes,u):
    
    lx = MD.modelDim.lx; ly = MD.modelDim.ly; eps=MD.eps
    if MD.dim==3:
        
        planarNodes = GetNodesByBoundingBox(nodes,-120-eps,-30+eps,5-eps,35+eps,88-eps,88+eps)
        planarXDofs = GetDofs(planarNodes,'x'); ux = u[planarXDofs];
        planarYDofs = GetDofs(planarNodes,'y'); uy = u[planarYDofs];
        planarZDofs = GetDofs(planarNodes,'z'); uz = u[planarZDofs];
        X=np.zeros([len(planarNodes)]); Y=np.zeros([len(planarNodes)]);
        i=0;
        for node in planarNodes:
            X[i] = node.coordinates[0]
            Y[i] = node.coordinates[1]
            i += 1
        
    
        cm1 = plt.cm.get_cmap('Spectral')
        
        uxLevels = np.linspace(min(ux),max(ux),num=100,endpoint=True)
        plt.figure(figsize=(8,6))
        plt.tricontourf(X,Y,ux,levels=uxLevels,cmap=cm1)
        cbar = plt.colorbar()
        cbar.ax.tick_params(labelsize=14) 
        plt.xlabel('x',fontsize=16)
        plt.ylabel('y',fontsize=16)
        plt.show()
        
        uyLevels = np.linspace(min(uy),max(uy),num=100,endpoint=True)
        plt.figure(figsize=(8,6))
        plt.tricontourf(X,Y,uy,levels=uyLevels,cmap=cm1)
        cbar = plt.colorbar()
        cbar.ax.tick_params(labelsize=14) 
        plt.xlabel('x',fontsize=16)
        plt.ylabel('y',fontsize=16)
        plt.show()
        
        uzLevels = np.linspace(min(uz),max(uz),num=100,endpoint=True)
        plt.figure(figsize=(8,6))
        plt.tricontourf(X,Y,uz,levels=uzLevels,cmap=cm1)
        cbar = plt.colorbar()
        cbar.ax.tick_params(labelsize=14) 
        plt.xlabel('x',fontsize=16)
        plt.ylabel('y',fontsize=16)
        plt.show()
        
    elif MD.dim==2:
      
        uy = u[1::2]; ux = u[0::2];
        X = np.zeros(len(nodes)); Y = np.zeros(len(nodes))
        for i in range(len(nodes)):
            X[i] = nodes[i].coordinates[0]
            Y[i] = nodes[i].coordinates[1]
        
        cm1 = plt.cm.get_cmap('Spectral')
        
        uxLevels = np.linspace(min(ux),max(ux),num=100,endpoint=True)
        plt.figure(figsize=(8,6))
        plt.tricontourf(X,Y,ux,levels=uxLevels,cmap=cm1)
        cbar = plt.colorbar()
        cbar.ax.tick_params(labelsize=14) 
        plt.xlabel('x',fontsize=16)
        plt.ylabel('y',fontsize=16)
        plt.show()
        
        uyLevels = np.linspace(min(uy),max(uy),num=100,endpoint=True)
        plt.figure(figsize=(8,6))
        plt.tricontourf(X,Y,uy,levels=uyLevels,cmap=cm1)
        cbar = plt.colorbar()
        cbar.ax.tick_params(labelsize=14) 
        plt.xlabel('x',fontsize=16)
        plt.ylabel('y',fontsize=16)
        plt.show()
    