"""
This file contains the code related to Gaussian processes and kernels for the
Matern Kernel study

@author: MK
"""

import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
from scipy import optimize
import random
import FiniteElementFunctions as FE
import ModelDefinitions as MD
from mpl_toolkits.mplot3d import Axes3D
import GOEEFunctions as GOEE
from scipy.sparse.linalg import inv as spinv
from scipy.sparse.linalg import spsolve
from scipy.sparse import csc_matrix,lil_matrix
import math

def GaussianProcess(kernel,nodeLabelTrain,yTrain,theta,*args):
    
    if kernel == 'StandardMatern':
        # unfold input variables
        nu = args[0]
        #Calculate posterior distribution
        postMu,postCov,thetaOpt,nodeLabelTest,nodesTrain = StandardMatern(nodeLabelTrain,yTrain,theta,nu)
        
        return postMu,postCov,thetaOpt,nodeLabelTest,nodesTrain

    elif kernel == 'StochasticMatern':
        # unfold input variables
        K = args[0]; M = args[1]; mapDofs = args[2]
        # Calculate posterior distribution
        postMu,postCov,thetaOpt,trainingDofs,testDofs = StochasticMatern(K,M,nodeLabelTrain,yTrain,theta,mapDofs)
            
        return postMu,postCov,thetaOpt,trainingDofs,testDofs

    else:
        # Error
        raise ValueError('Invalid Kernel')
        

def StandardMatern(nodeLabelTrain,yTrain,theta,nu):
    
    # Labels of test nodes
    nodeLabelTest = [node for node in MD.nodesList if node not in nodeLabelTrain]
    # location of train nodes
    nodesTrain = MD.nodes[nodeLabelTrain]
    
    # Find optimal hyper-parameters
    print('Computing optimal hyper-parameters')
    x0,fval,grid,jout = optimize.brute(NLL('StandardMatern',yTrain,theta,nodesTrain,nu),
                        ranges=((0.01,0.27),(6e-8,10e-5)),Ns=200,full_output=True)
    
    # x0,fval,grid,jout = optimize.brute(NLL('StandardMatern',yTrain,theta,nodesTrain,nu),
                        # ranges=((0.01,0.28),(1e-8,10e-5)),Ns=200,full_output=True)
    
    thetaOpt = x0
    
    # Create full covariance matrix
    cov = Kernel('StandardMatern',MD.nodes,MD.nodes,thetaOpt,nu)
    
    # Identify different parts of covariance matrix
    K,Ks,Kss = IdentifyCov('StandardMatern',cov,nodeLabelTrain,nodeLabelTest)
    
    # Sample from prior distribution
    priorMu = np.zeros(len(nodeLabelTest))
    
    # Calculate posterior mean and covariance
    postMu,postCov = CalculatePosterior(K,Ks,Kss,yTrain,priorMu)
    
    return postMu,postCov,thetaOpt,nodeLabelTest,nodesTrain

def StochasticMatern(K,M,nodeLabelTrain,yTrain,theta,mapDofs):
    
    # Get the training and test degrees of freedom
    errTrain,trainingDofs,testDofs,trainPostBC,testPostBC = TrainAndTestDofs(yTrain,nodeLabelTrain,mapDofs)
 
    # Optimal hyper-parameters
    # print('Computing optimal hyper-parameters')
    x0,fval,grid,jout = optimize.brute(NLL('StochasticMatern',errTrain,theta,K,M,trainPostBC,testPostBC),
                      ranges=((1e-5,10),(6e-4,10)),Ns=20,full_output=True)
    
    thetaOpt = theta
    
    print('Calculating posterior')
    # Create full covariance matrix
    cov = Kernel('StochasticMatern',K,M,thetaOpt)
    
    # Identify different parts of covariance matrix
    K,Ks,Kss = IdentifyCov('StochasticMatern',cov,trainPostBC,testPostBC)
    
    # Sample from prior distribution
    priorMu = np.zeros(len(testDofs))
    
    # Calculate posterior mean and covariance
    postMu,postCov = CalculatePosterior(K,Ks,Kss,errTrain,priorMu)
    
    
    return postMu,postCov,thetaOpt,trainingDofs,testDofs
            

def Kernel(kernel,*args):
    
    if kernel=='SquaredExponential':
        
        # Inputs
        x1 = args[0]; x2 = args[1]
        # Hyper-parameters
        l = args[2][0]; sig = args[2][1]
        # Calculate covariance matrix
        cov = np.zeros([len(x1),len(x2)])
        for i in range(len(x1)):
            sqDist = np.sum((x1[i]-x2)**2,axis=1)
            cov[i] = np.exp(-0.5*sqDist/l**2)
            
        return sig**2*cov
    
    elif kernel == 'StandardMatern':
        
        # Inputs
        x1 = args[0]; x2 = args[1];
        # Hyper-parameters
        l = args[2][0]; sig = args[2][1]; nu = args[3]
        # Initialise sparse covariance matrix
        cov = lil_matrix((len(x1), len(x2)), dtype=np.float64)
        if nu == 0.5:
            for i in range(len(x1)):
                dist = np.sqrt(np.sum((x1[i]-x2)**2,axis=1))
                cov[i] = np.exp(-dist/l)
    
        elif nu == 1.5: 
            for i in range(len(x1)):
                dist = np.sqrt(np.sum((x1[i]-x2)**2,axis=1))
                covTemp = (1. + np.sqrt(3.)*dist/l)*np.exp(-np.sqrt(3.)*dist/l)
                covTemp[covTemp<1e-6] = 0.
                covTemp = sig**2*covTemp
                cov[i] = covTemp
                
        elif nu == 2.5:
            for i in range(len(x1)):
                dist = np.sqrt(np.sum((x1[i]-x2)**2,axis=1))
                cov[i] = (1. + np.sqrt(5)*dist/l + (5.*dist**2)/(3.*l))*np.exp(-np.sqrt(5)*dist/l)
                    
                
        return cov.tocsc()

    elif kernel == 'StochasticMatern':
    
        # Hyper-parameters
        alpha = args[2][0]; beta = args[2][1];
        # Stiffness and mass matrices
        K = args[0]; M = args[1];
        # Calculate covariance matrix
        invH = spinv(M + beta**2*K)
        invH = invH.toarray(); M = M.toarray()
        cov = alpha**2*np.dot(np.dot(invH,M),invH)
        
        return csc_matrix(cov)
    
    else:
        
        raise ValueError('Kernel undefined')
        
def NLL(kernel,yTrain,theta,*args):
    
    # kernel,xTrain,yTrain,nodeLabelTrain,nodeLabelTest,nu,stiff,mass,trainPostBC,testPostBC
    
    if kernel=='StochasticMatern':
        
        def FunNLL(theta):
            # unfold variables
            K = args[0]; M = args[1]; 
            trainPostBC = args[2]; testPostBC = args[3]
            
            cov = Kernel('StochasticMatern',K,M,theta)
            K = IdentifyCov(kernel,cov,trainPostBC,testPostBC)[0]
            L = np.linalg.cholesky(K.toarray()) 
            alpha = np.linalg.solve(L.T,np.linalg.solve(L,yTrain))
            
            # Negative log-likelihood approximation (Rasmussen Sec. 2.3 pg. 19 )
            nll = np.sum(np.log(np.diagonal(L))) + \
                  0.5*np.dot(yTrain.T,alpha) + \
                  0.5*len(yTrain)*np.log(2*np.pi);
                  
            
            return nll
    else:
                
        def FunNLL(theta):
            # unfold variables
            nodesTrain = args[0]; nu = args[1];
            # Calculate train-train covariance matrix
            K = Kernel(kernel,nodesTrain,nodesTrain,theta,nu)
            L = np.linalg.cholesky(K.toarray())
            alpha = np.linalg.solve(L.T,np.linalg.solve(L,yTrain))
            
            # Negative log-likelihood approximation (Rasmussen Sec. 2.3 pg. 19 )
            nll = np.sum(np.log(np.diagonal(L))) + \
                  0.5*np.dot(yTrain.T,alpha) + \
                  0.5*len(nodesTrain)*np.log(2*np.pi);
                  
            return nll
    
    return FunNLL

def IdentifyCov(kernel,cov,*args):
    
    
    if kernel=='StochasticMatern':
        
        # unfold variables
        trainPostBC = args[0]; testPostBC = args[1];
        
        # Identify train-train
        identifyK = lil_matrix((len(trainPostBC),len(trainPostBC+testPostBC)),dtype=np.float64)
        row = 0
        for dof in trainPostBC:
            identifyK[row,dof] = 1.0
            row += 1
            
        identifyK = identifyK.tocsc()
        K = identifyK*cov*identifyK.transpose()
        
        # Identify test-test
        identifyKss = lil_matrix((len(testPostBC),len(trainPostBC+testPostBC)),dtype=np.float64)
        row = 0
        for dof in testPostBC:
            identifyKss[row,dof] = 1.0
            row += 1
        
        identifyKss = identifyKss.tocsc()
        Kss = identifyKss*cov*identifyKss.transpose()
        
        # Identify train-test
        Ks = identifyK*cov*identifyKss.transpose()
    
    else:
    
        # unfold variables
        nodeLabelTrain = args[0]; nodeLabelTest = args[1]
    
        # Identify train-train
        identifyK = lil_matrix((len(nodeLabelTrain),len(nodeLabelTrain)+len(nodeLabelTest)),dtype=np.float64)
        row = 0
        for node in nodeLabelTrain:
            identifyK[row,node] = 1.0
            row += 1
            
        identifyK = identifyK.tocsc()
        K = identifyK*cov*identifyK.transpose()
        
        # Identify test-test
        identifyKss = lil_matrix((len(nodeLabelTest),len(nodeLabelTrain)+len(nodeLabelTest)),dtype=np.float64)
        row = 0
        for node in nodeLabelTest:
            identifyKss[row,node] = 1.0
            row += 1
        
        identifyKss = identifyKss.tocsc()
        Kss = identifyKss*cov*identifyKss.transpose()
        
        # Identify train-test
        Ks = identifyK*cov*identifyKss.transpose()
        
    
    return K,Ks,Kss

def CalculatePosterior(K,Ks,Kss,yTrain,priorMu):
    
    # Estimate posterior (Rasmussen Algorithm 2.1 pg. 19)             
    L = csc_matrix(np.linalg.cholesky(K.toarray()))
    Lk = spsolve(L,Ks)
    m = spsolve(L,csc_matrix(yTrain.reshape(-1,1)))
    alpha = Lk.transpose()*m
    postMu = priorMu + alpha
    postCov = Kss - Lk.transpose()*Lk
    
    
    return postMu,postCov


def TrainAndTestDofs(error,nodeLabelTrain,mapDofs):
    
    # Generate the test dofs list
    postBCInd = mapDofs.tolist()
     
    # Training set
    if MD.dim == 2:
        # Training set less constrained nodes
        xNodes = np.setdiff1d(nodeLabelTrain[0],MD.xFixedNodes)
        yNodes = np.setdiff1d(nodeLabelTrain[1],MD.yFixedNodes)
        
        #Training degrees of freedom
        trainingDofs = []
        for node in xNodes:
            trainingDofs.append(node*2)
        for node in yNodes:
            trainingDofs.append(node*2+1)
        trainingDofs.sort()
        
        # Test degrees of freedom
        testDofs = []
        for dof in postBCInd:
            if dof not in trainingDofs:
                testDofs.append(dof)
        

        # Build training set
        dofsLabelTrain = [xNodes*2,yNodes*2+np.ones(len(yNodes))]
        dofsFlat = np.concatenate((dofsLabelTrain[0],dofsLabelTrain[1]))
        errorFlat = np.concatenate((error[0],error[1]))
        arr1inds = dofsFlat.argsort()
        yTrain = errorFlat[arr1inds]
        
        
        trainPostBC = [ postBCInd.index(i) for i in trainingDofs ];
        testPostBC  = [ postBCInd.index(i) for i in testDofs     ];
                                                                          
    elif MD.dim == 3:
        # Training set less constrained nodes
        xNodes = np.setdiff1d(nodeLabelTrain[0],MD.xFixedNodes)
        yNodes = np.setdiff1d(nodeLabelTrain[1],MD.yFixedNodes)
        zNodes = np.setdiff1d(nodeLabelTrain[2],MD.zFixedNodes)
        
        #Training degrees of freedom
        trainingDofs = []
        for node in xNodes:
            trainingDofs.append(node*3)
        for node in yNodes:
            trainingDofs.append(node*3+1)
        for node in zNodes:
            trainingDofs.append(node*3+2)
        trainingDofs.sort()
        
        # Test degrees of freedom
        testDofs = []
        for dof in postBCInd:
            if dof not in trainingDofs:
                testDofs.append(dof)
        
        # Build training set
        dofsLabelTrain = [xNodes*3,yNodes*3+np.ones(len(yNodes)),zNodes*3+2*np.ones(len(zNodes))]
        dofsFlat = np.concatenate((dofsLabelTrain[0],dofsLabelTrain[1],dofsLabelTrain[2]))
        errorFlat = np.concatenate((error[0],error[1],error[2]))
        arr1inds = dofsFlat.argsort()
        yTrain = errorFlat[arr1inds]
        
        trainPostBC = [ postBCInd.index(i) for i in trainingDofs ];
        testPostBC  = [ postBCInd.index(i) for i in testDofs     ];
    
    return yTrain,trainingDofs,testDofs,trainPostBC,testPostBC
  
#GP.PlotStandardPosterior(muSdz,covSdz,dualLocZ,goeeUz,coord,numDual)

  
def PlotStandardPosterior(postMu,postCov,nodeLabelTrain,yTrain,coord,numDual):
    
    if MD.dim == 2:
        # Test plus training points
        # Labels of test nodes
        nodeLabelTest = [node for node in MD.nodesList if node not in nodeLabelTrain]
        # location of train nodes
        nodesTrain = MD.nodes[nodeLabelTrain]
        nodesTest = MD.nodes[nodeLabelTest]
        x1 = np.concatenate([np.squeeze(nodesTest[:,0]),np.squeeze(nodesTrain[:,0])])
        x2 = np.concatenate([np.squeeze(nodesTest[:,1]),np.squeeze(nodesTrain[:,1])])
        mu = np.concatenate([np.squeeze(postMu),np.squeeze(yTrain)])
        
        # Plot of posterior mean
        fig = plt.figure(figsize=(8,6))
        ax = fig.gca(projection='3d')
        ax.plot_trisurf(x1,x2,mu,color='b',label='',cmap=cm.rainbow,alpha=0.5)
        plt.xlabel('x',fontsize=16)
        plt.ylabel('y',fontsize=16)
        ax.set_zlabel('error',fontsize=16)
        plt.title('Posterior mean',fontsize=22)
        
        # Training points
        ax.scatter(nodesTrain[:,0], nodesTrain[:,1], yTrain, c='r',s=40)
        
        # Standard deviation for test points
        sigTest = np.sqrt(postCov.diagonal())
        sigTrain = np.zeros([len(yTrain),1])
        sig = np.concatenate([np.squeeze(sigTest),np.squeeze(sigTrain)])
        
        # Plus minus 3 std. deviation
        upper = mu + 3*sig
        lower = mu - 3*sig
        ax.plot_trisurf(x1,x2,upper,color='lightgray',alpha = 0.3)
        ax.plot_trisurf(x1,x2,lower,color='lightgray',alpha = 0.3)
        
        #plt.savefig('StdMatern'+str(len(nodeLabelTrain))+'.png', bbox_inches='tight',dpi=200)
    else:
        
        
        unfold = coord
        
        for x in range(0,2197):  
            if coord[x,2] == 86:
                r = math.sqrt((coord[x,1] - 0)**2 + (coord[x,2] - 81)**2);
                
                unfold[x,1] = 0;
                unfold[x,2] = 81+r;
                
            
            elif coord[x,2] == 88:
                r = math.sqrt((coord[x,1] - 0)**2 + (coord[x,2] - 81)**2);
                
                unfold[x,1] = -2;
                unfold[x,2] = 81+r;
            
            if coord[x,1]>0 and coord[x,1]<4:
                if coord[x,2]>81 and coord[x,2]<86:
                    r = math.sqrt((coord[x,1] - 0)**2 + (coord[x,2] - 81)**2); 
                    unfold[x,1] = 0;
                    unfold[x,2] = 81+r;
            
            if coord[x,1]>-1 and coord[x,1]<0:
                if coord[x,2]>86 and coord[x,2]<87:
                    r = math.sqrt((coord[x,1] - 0)**2 + (coord[x,2] - 81)**2); 
                    unfold[x,1] = -2;
                    unfold[x,2]= 81+r;
  
            if coord[x,0] == 0:
                r = math.sqrt((coord[x,0]--5)**2 + (coord[x,1] - 0)**2);
                unfold[x,0] = -5 + r;
                unfold[x,1] = 0;
            
            if coord[x,0] == 2:
                r = math.sqrt((coord[x,0]--5)**2 + (coord[x,1] - 0)**2);
                unfold[x,0] = -5 + r;
                unfold[x,1] = -2;
            
            if coord[x,0]>-4 and coord[x,0]<0:
                if coord[x,1]>0 and coord[x,1]<4:
                    r = math.sqrt((coord[x,0]--5)**2 + (coord[x,1] - 0)**2);
                    unfold[x,0] = -5 + r;
                    unfold[x,1] = 0;
            
            if coord[x,0]>0 and coord[x,0]<1:
                if coord[x,1]>-1 and coord[x,1]<0:
                    r = math.sqrt((coord[x,0]--5)**2 + (coord[x,1] - 0)**2);
                    unfold[x,0] = -5 + r;
                    unfold[x,1] = -2;
                    
        # Test plus training points
        # Labels of test nodes
        nodeLabelTest = [node for node in MD.nodesList if node not in nodeLabelTrain]
        # location of train nodes
        nodesTrain = MD.nodes[nodeLabelTrain]
        nodesTest = MD.nodes[nodeLabelTest]
        x1 = np.concatenate([np.squeeze(nodesTest[:,0]),np.squeeze(nodesTrain[:,0])])
        x2 = np.concatenate([np.squeeze(nodesTest[:,1]),np.squeeze(nodesTrain[:,1])])
        mu = np.concatenate([np.squeeze(postMu),np.squeeze(yTrain)])
        
        # # Plot of posterior mean
        # fig = plt.figure(figsize=(8,6))
        # ax = fig.gca(projection='3d')
        # ax.plot_trisurf(x1,x2,mu,color='b',label='',cmap=cm.rainbow,alpha=0.5)
        # plt.xlabel('x',fontsize=16)
        # plt.ylabel('y',fontsize=16)
        # ax.set_zlabel('error',fontsize=16)
        # plt.title('Posterior mean',fontsize=22)
        
        # Training points
        # ax.scatter(nodesTrain[:,0], nodesTrain[:,1], yTrain, c='r',s=40)
        
        # Standard deviation for test points
        sigTest = np.sqrt(postCov.diagonal())
        sigTrain = np.zeros([len(yTrain),1])
        sig = np.concatenate([np.squeeze(sigTest),np.squeeze(sigTrain)])
        
        # Plus minus 3 std. deviation
        upper = mu + 3*sig
        lower = mu - 3*sig
        # ax.plot_trisurf(x1,x2,upper,color='lightgray',alpha = 0.3)
        # ax.plot_trisurf(x1,x2,lower,color='lightgray',alpha = 0.3)

        nodeTrainUnfold = np.zeros((numDual, 3))
        for x in range(0,numDual):
            nodeTrainUnfold[x,:] = unfold[nodeLabelTrain[x],:]
            
        nodeTestUnfold = np.zeros((2197-numDual, 3))
        for x in range(0,2197-numDual):
            nodeTestUnfold[x,:] = unfold[nodeLabelTest[x],:]
        
        nodeTrainInner = nodeTrainUnfold[np.where(nodeTrainUnfold[:,1]==0)]
        
        nodeTestInner = nodeTestUnfold[np.where(nodeTestUnfold[:,1]==0)]
        
        postMuInner = postMu[np.where(nodeTestUnfold[:,1]==0)]
        
        yTrainInner = yTrain[np.where(nodeTrainUnfold[:,1]==0)]
        
        sigTestInner = sigTest[np.where(nodeTestUnfold[:,1]==0)]

        x1Inner = np.squeeze(nodeTestInner[:,0])
        x2Inner = np.squeeze(nodeTestInner[:,2])
        muInner = np.squeeze(postMuInner)
        

        sigInner = np.squeeze(sigTestInner)

        upperInner = muInner + 3*sigInner
        lowerInner = muInner - 3*sigInner
        
        
        # # Plot of posterior mean
        # fig = plt.figure(figsize=(8,6))
        # ax = fig.gca(projection='3d')
        # ax.scatter(x1Inner,x2Inner,np.zeros(728),c='k',s=1)
        # ax.plot_trisurf(x1Inner,x2Inner,muInner,color='b',label='',cmap=cm.rainbow,alpha=0.5)
        # plt.xlabel('x',fontsize=16)
        # plt.ylabel('y',fontsize=16)
        # ax.set_zlabel('error',fontsize=16)
        # plt.title('Inner surface Uy error posterior mean',fontsize=22)
        # ax.scatter(nodeTrainInner[:,0], nodeTrainInner[:,2], yTrainInner, c='r',s=10)
        # ax.plot_trisurf(x1Inner,x2Inner,upperInner,color='lightgray',alpha = 0.3)
        # ax.plot_trisurf(x1Inner,x2Inner,lowerInner,color='lightgray',alpha = 0.3)
        
        # Plot of posterior mean
        fig = plt.figure(figsize=(8,6))
        ax = fig.gca(projection='3d')
        ax.scatter(x1Inner,x2Inner,np.zeros(len(x1Inner)),c='k',s=1)
        ax.plot_trisurf(x1Inner,x2Inner,muInner,color='b',label='',cmap=cm.rainbow,alpha=0.5)
        plt.xlabel('x',fontsize=16)
        plt.ylabel('y',fontsize=16)
        ax.set_zlabel('error',fontsize=16)
        plt.title('Inner surface Uy error posterior mean',fontsize=22)
        ax.scatter(nodeTrainInner[:,0], nodeTrainInner[:,2], yTrainInner, c='r',s=10)
        ax.plot_trisurf(x1Inner,x2Inner,upperInner,color='lightgray',alpha = 0.3)
        ax.plot_trisurf(x1Inner,x2Inner,lowerInner,color='lightgray',alpha = 0.3)
        ax.view_init(90,0)
        
        # fig = plt.figure(figsize=(8,6))
        # ax = fig.gca(projection='3d')
        # ax.scatter(x1Outer,x2Outer,np.zeros(286),c='k',s=10)
        # plt.xlabel('x',fontsize=16)
        # plt.ylabel('y',fontsize=16)
        
        
        # fig = plt.figure(figsize=(8,6))
        # ax = fig.gca(projection='3d')
        # ax.scatter(x1Outer,x2Outer,np.zeros(286),c='k',s=10)
        # ax.plot_trisurf(x1Outer,x2Outer,muOuter,color='b',label='',cmap=cm.rainbow,alpha=0.5)
        # plt.xlabel('x',fontsize=16)
        # plt.ylabel('y',fontsize=16)
        # ax.set_zlabel('error',fontsize=16)
        # plt.title('Outer surface posterior mean',fontsize=22)
        # ax.scatter(nodeTrainOuter[:,0], nodeTrainOuter[:,2], yTrainOuter, c='r',s=40)
        # ax.plot_trisurf(x1Outer,x2Outer,upperOuter,color='lightgray',alpha = 0.3)
        # ax.plot_trisurf(x1Outer,x2Outer,lowerOuter,color='lightgray',alpha = 0.3)



        #raise ValueError('Plotting not implemented for 3D continuum model')
            
    
    return ax

def SeparateDistributions(postMu,postCov,nodeLabelTrain,mapDofs):
    
    # Generate the test dofs list
    postBCInd = mapDofs.tolist()
     
    # Training set
    if MD.dim == 2:
        # Training set less constrained nodes
        xNodes = np.setdiff1d(nodeLabelTrain[0],MD.xFixedNodes)
        yNodes = np.setdiff1d(nodeLabelTrain[1],MD.yFixedNodes)
        
        #Training degrees of freedom
        trainingDofs = []
        for node in xNodes:
            trainingDofs.append(node*2)
        for node in yNodes:
            trainingDofs.append(node*2+1)
        trainingDofs.sort()
        
        # Test degrees of freedom
        testDofs = []
        for dof in postBCInd:
            if dof not in trainingDofs:
                testDofs.append(dof)
    
        # Identify x & y dofs from test dofs
        xDof = []; yDof = []; i = 0;
        for dof in testDofs:
            if (dof%2==0):
                xDof.append(i)
                i+=1
            else:
                yDof.append(i)
                i+=1
    
        # Separate the x & y from mean and covariance
        xDof = np.array(xDof); yDof = np.array(yDof);
        xMu = postMu[xDof]; yMu = postMu[yDof];
        xCov = postCov[xDof][:,xDof];
        yCov = postCov[yDof][:,yDof];
        
        # Store in a list
        mu = [xMu,yMu]
        cov = [xCov.toarray(),yCov.toarray()]
        
        return mu,cov
    
    elif MD.dim == 3:
        
        raise ValueError('Separation of distribution not implemented for 3D continuum elements')
        
        
def PlotStochasticPosterior(nodes,postMu,postCov,nodeLabelTrain,errTrain,mapDofs,coord,numDual):
    
    if MD.dim == 2:
        yTrain,trainingDofs,testDofs,trainPostBC,testPostBC = TrainAndTestDofs(errTrain,nodeLabelTrain,mapDofs)
        # unfold the mean and std. dev. vectors into separate x and y dof vectors 
        errorX = []; errorY = []; sigX = []; sigY = [];
        sig = np.sqrt(postCov.diagonal()); i = 0;
        for dof in testDofs:
            if (dof%2 == 0):
                errorX.append(postMu[i])
                sigX.append(sig[i])
                i += 1
            else:
                errorY.append(postMu[i])
                sigY.append(sig[i])
                i += 1
        
        # Create the vectors for x & y comeponents of grid to plot the error against
        x1XTrain=[]; x2XTrain=[]; x1YTrain=[]; x2YTrain=[]; component=0;
        for sublist in nodeLabelTrain:
            for node in sublist:
                if component==0:
                    x1XTrain.append(nodes[node].coordinates[0])
                    x2XTrain.append(nodes[node].coordinates[1])
                else:
                    x1YTrain.append(nodes[node].coordinates[0])
                    x2YTrain.append(nodes[node].coordinates[1])
            component += 1
        
        # Boundary points
        x1XBC=[]; x2XBC=[]; x1YBC=[]; x2YBC=[]; component=0
        errorXBC=[]; errorYBC=[];
        nodeLabelBC = [MD.xFixedNodes,MD.yFixedNodes]
        for sublist in nodeLabelBC:
            for node in sublist:
                if component==0:
                    x1XBC.append(nodes[node].coordinates[0])
                    x2XBC.append(nodes[node].coordinates[1])
                    errorXBC.append(0.)
                else:
                    x1YBC.append(nodes[node].coordinates[0])
                    x2YBC.append(nodes[node].coordinates[1])
                    errorYBC.append(0.)
            component += 1
        
        # Create test set
        x1XTest=[]; x2XTest=[]; x1YTest=[]; x2YTest=[]; component=0;
        for sublist in nodeLabelTrain:
            for node in nodes:
                if node.label not in sublist:
                    if node.label not in nodeLabelBC[component]:
                        if component==0:
                            x1XTest.append(node.coordinates[0])
                            x2XTest.append(node.coordinates[1])
                        else:
                            x1YTest.append(node.coordinates[0])
                            x2YTest.append(node.coordinates[1])
            component += 1
        
        # Extract the x and y training values from yTrain
        yTrainX = []; yTrainY = [];
        for dof in trainingDofs:
            if (dof%2==0):
                yTrainX.append(yTrain[trainingDofs.index(dof)])
            else:
                yTrainY.append(yTrain[trainingDofs.index(dof)])
        
        # Join the test and training set
        x1X = x1XTest + x1XTrain + x1XBC; x2X = x2XTest + x2XTrain + x2XBC;
        x1Y = x1YTest + x1YTrain + x1YBC; x2Y = x2YTest + x2YTrain + x2YBC;
        errorX = errorX + yTrainX + errorXBC; errorY = errorY + yTrainY + errorYBC
        sigX = sigX + len(yTrainX)*[0.] + len(errorXBC)*[0.] 
        sigY = sigY + len(yTrainY)*[0.] + len(errorYBC)*[0.]
        
        # Convert to arrays
        x1X = np.squeeze(np.asarray(x1X))
        x2X = np.squeeze(np.asarray(x2X))
        x1Y = np.squeeze(np.asarray(x1Y))
        x2Y = np.squeeze(np.asarray(x2Y))
        sigX = np.squeeze(np.asarray(sigX))
        sigY = np.squeeze(np.asarray(sigY))
        errorX = np.squeeze(np.asarray(errorX))
        errorY = np.squeeze(np.asarray(errorY))
        x1XTrain = np.squeeze(np.asarray(x1XTrain))
        x2XTrain = np.squeeze(np.asarray(x2XTrain))
        x1YTrain = np.squeeze(np.asarray(x1YTrain))
        x2YTrain = np.squeeze(np.asarray(x2YTrain))
        
        # Plus minus 3 std. dev.
        upperX = errorX + 3*sigX; upperY = errorY + 3*sigY
        lowerX = errorX - 3*sigX; lowerY = errorY - 3*sigY
        
        # Plot of posterior mean
        cm1 = plt.cm.get_cmap('Spectral')
        fig = plt.figure(figsize=(8,6))
        ax = fig.gca(projection='3d')
        ax.plot_trisurf(x1X,x2X,errorX,cmap=cm1,alpha=0.5)
        plt.xlabel('x',fontsize=16)
        plt.ylabel('y',fontsize=16)
        ax.set_zlabel('error',fontsize=16)
#            plt.title('Posterior mean',fontsize=22)
        # Confidence interval
        #ax.plot_trisurf(x1X,x2X,upperX,color='lightgray',alpha = 0.3)
        #ax.plot_trisurf(x1X,x2X,lowerX,color='lightgray',alpha = 0.3)
        # Training points
        ax.scatter(x1XTrain, x2XTrain, yTrainX, c='r',s=40)
        # Boundary points
        ax.scatter(x1XBC, x2XBC, errorXBC, c='black',s=40)
        # Save figure
#            plt.savefig('STMaternUx'+'.png', bbox_inches='tight',dpi=200)
        
        # Plot of posterior mean
        fig = plt.figure(figsize=(8,6))
        ax = fig.gca(projection='3d')
        ax.plot_trisurf(x1Y,x2Y,errorY,color='b',label='',cmap=cm1,alpha=0.5)
        plt.xlabel('x',fontsize=16)
        plt.ylabel('y',fontsize=16)
        ax.set_zlabel('error' ,fontsize=16)
#            plt.title('Posterior mean',fontsize=22)
        # Confidence interval
        #ax.plot_trisurf(x1Y,x2Y,upperY,color='lightgray',alpha = 0.3)
        #ax.plot_trisurf(x1Y,x2Y,lowerY,color='lightgray',alpha = 0.3)
        # Training points
        ax.scatter(x1YTrain, x2YTrain, yTrainY, c='r',s=40)
        # Boundary points
        ax.scatter(x1YBC, x2YBC, errorYBC, c='black',s=40)
        # Save figure
#            plt.savefig('STMaternUy'+'.png', bbox_inches='tight',dpi=200)
    else:
        
        yTrain,trainingDofs,testDofs,trainPostBC,testPostBC = TrainAndTestDofs(errTrain,nodeLabelTrain,mapDofs)
        # Unpack the mean and std. dev. vectors into separate x and y dof vectors 
        errorX = []; errorY = []; errorZ = []; sigX = []; sigY = []; sigZ = [];
        sig = np.sqrt(postCov.diagonal()); i = 0;
        for dof in testDofs:
            if ((dof+2)%3==0):
                errorX.append(postMu[i])
                sigX.append(sig[i])
                i += 1
            elif ((dof+1)%3==0):
                errorY.append(postMu[i])
                sigY.append(sig[i])
                i += 1
            elif ((dof)%3==0):
                errorZ.append(postMu[i])
                sigZ.append(sig[i])
                i += 1 
        
        # Create the vectors for x & y comeponents of grid to plot the error against
        x1XTrain=[]; x2XTrain=[]; x3XTrain=[]; x1YTrain=[]; x2YTrain=[]; x3YTrain=[]; x1ZTrain=[]; x2ZTrain=[]; x3ZTrain=[]; component=0;
        for sublist in nodeLabelTrain:
            for node in sublist:
                if component==0:
                    x1XTrain.append(nodes[node].coordinates[0])
                    x2XTrain.append(nodes[node].coordinates[1])
                    x3XTrain.append(nodes[node].coordinates[2])
                elif component==1:
                    x1YTrain.append(nodes[node].coordinates[0])
                    x2YTrain.append(nodes[node].coordinates[1])
                    x3YTrain.append(nodes[node].coordinates[2])
                elif component==2:
                    x1ZTrain.append(nodes[node].coordinates[0])
                    x2ZTrain.append(nodes[node].coordinates[1])
                    x3ZTrain.append(nodes[node].coordinates[2])                        
            component += 1
        
        # Boundary points
        x1XBC=[]; x2XBC=[]; x3XBC=[]; x1YBC=[]; x2YBC=[]; x3YBC=[]; x1ZBC=[]; x2ZBC=[]; x3ZBC=[];  component=0
        errorXBC=[]; errorYBC=[]; errorZBC=[];
        nodeLabelBC = [MD.xFixedNodes,MD.yFixedNodes, MD.zFixedNodes]
        for sublist in nodeLabelBC:
            for node in sublist:
                if component==0:
                    x1XBC.append(nodes[node].coordinates[0])
                    x2XBC.append(nodes[node].coordinates[1])
                    x3XBC.append(nodes[node].coordinates[2])                    
                    errorXBC.append(0.)
                elif component==1:
                    x1YBC.append(nodes[node].coordinates[0])
                    x2YBC.append(nodes[node].coordinates[1])
                    x3YBC.append(nodes[node].coordinates[2])
                    errorYBC.append(0.)
                elif component ==2:
                    x1ZBC.append(nodes[node].coordinates[0])
                    x2ZBC.append(nodes[node].coordinates[1])
                    x3ZBC.append(nodes[node].coordinates[2])
                    errorZBC.append(0.)                    
            component += 1
        
        # Create test set
        x1XTest=[]; x2XTest=[]; x3XTest=[]; x1YTest=[]; x2YTest=[]; x3YTest=[]; x1ZTest=[]; x2ZTest=[]; x3ZTest=[];  component=0;
        for sublist in nodeLabelTrain:
            for node in nodes:
                if node.label not in sublist:
                    if node.label not in nodeLabelBC[component]:
                        if component==0:
                            x1XTest.append(node.coordinates[0])
                            x2XTest.append(node.coordinates[1])
                            x3XTest.append(node.coordinates[2])
                        elif component==1:
                            x1YTest.append(node.coordinates[0])
                            x2YTest.append(node.coordinates[1])
                            x3YTest.append(node.coordinates[2])
                        elif component==2:
                            x1ZTest.append(node.coordinates[0])
                            x2ZTest.append(node.coordinates[1])
                            x3ZTest.append(node.coordinates[2])                            
            component += 1
        
        # Extract the x and y training values from yTrain
        yTrainX = []; yTrainY = []; yTrainZ=[]
        for dof in trainingDofs:
            if ((dof+2)%3==0):
                yTrainX.append(yTrain[trainingDofs.index(dof)])
            elif ((dof+1)%3==0):
                yTrainY.append(yTrain[trainingDofs.index(dof)])
            elif ((dof)%3==0):
                yTrainZ.append(yTrain[trainingDofs.index(dof)])
        
        # Join the test and training set
        x1X = x1XTest + x1XTrain + x1XBC; x2X = x2XTest + x2XTrain + x2XBC; x3X = x3XTest + x3XTrain + x3XBC;
        x1Y = x1YTest + x1YTrain + x1YBC; x2Y = x2YTest + x2YTrain + x2YBC; x3Y = x3YTest + x3YTrain + x3YBC;
        x1Z = x1ZTest + x1ZTrain + x1ZBC; x2Z = x2ZTest + x2ZTrain + x2ZBC; x3Z = x3ZTest + x3ZTrain + x3ZBC;
        
        errorX = errorX + yTrainX + errorXBC; errorY = errorY + yTrainY + errorYBC; errorZ = errorZ + yTrainZ + errorZBC;
        sigX = sigX + len(yTrainX)*[0.] + len(errorXBC)*[0.] 
        sigY = sigY + len(yTrainY)*[0.] + len(errorYBC)*[0.]
        sigZ = sigZ + len(yTrainZ)*[0.] + len(errorZBC)*[0.]
        
        # Convert to arrays
        x1X = np.squeeze(np.asarray(x1X))
        x2X = np.squeeze(np.asarray(x2X))
        x3X = np.squeeze(np.asarray(x3X))
        x1Y = np.squeeze(np.asarray(x1Y))
        x2Y = np.squeeze(np.asarray(x2Y))
        x3Y = np.squeeze(np.asarray(x3Y))
        x1Z = np.squeeze(np.asarray(x1Z))
        x2Z = np.squeeze(np.asarray(x2Z))
        x3Z = np.squeeze(np.asarray(x3Z))
        
        sigX = np.squeeze(np.asarray(sigX))
        sigY = np.squeeze(np.asarray(sigY))
        sigZ = np.squeeze(np.asarray(sigZ))
        errorX = np.squeeze(np.asarray(errorX))
        errorY = np.squeeze(np.asarray(errorY))
        errorZ = np.squeeze(np.asarray(errorZ))
        x1XTrain = np.squeeze(np.asarray(x1XTrain))
        x2XTrain = np.squeeze(np.asarray(x2XTrain))
        x3XTrain = np.squeeze(np.asarray(x3XTrain))
        x1YTrain = np.squeeze(np.asarray(x1YTrain))
        x2YTrain = np.squeeze(np.asarray(x2YTrain))
        x3YTrain = np.squeeze(np.asarray(x3YTrain))
        x1ZTrain = np.squeeze(np.asarray(x1ZTrain))
        x2ZTrain = np.squeeze(np.asarray(x2ZTrain))
        x3ZTrain = np.squeeze(np.asarray(x3ZTrain))        
        
        # Plus minus 3 std. dev.
        upperX = errorX + 3*sigX; upperY = errorY + 3*sigY; upperZ = errorZ + 3*sigZ;
        lowerX = errorX - 3*sigX; lowerY = errorY - 3*sigY; lowerZ = errorZ - 3*sigZ;
                
        return errorZ
    
        # unfold = coord;
        
        # for x in range(0,572):  
        #     if coord[x,2] == 86:
        #         r = math.sqrt((coord[x,1] - 0)**2 + (coord[x,2] - 81)**2);
                
        #         unfold[x,1] = 0;
        #         unfold[x,2] = 81+r;
                
            
        #     elif coord[x,2] == 88:
        #         r = math.sqrt((coord[x,1] - 0)**2 + (coord[x,2] - 81)**2);
                
        #         unfold[x,1] = -2;
        #         unfold[x,2] = 81+r;
            
        #     if coord[x,1]>0 and coord[x,1]<4:
        #         if coord[x,2]>81 and coord[x,2]<86:
        #             r = math.sqrt((coord[x,1] - 0)**2 + (coord[x,2] - 81)**2); 
        #             unfold[x,1] = 0;
        #             unfold[x,2] = 81+r;
            
        #     if coord[x,1]>-1 and coord[x,1]<0:
        #         if coord[x,2]>86 and coord[x,2]<87:
        #             r = math.sqrt((coord[x,1] - 0)**2 + (coord[x,2] - 81)**2); 
        #             unfold[x,1] = -2;
        #             unfold[x,2]= 81+r;
  
        #     if coord[x,0] == 0:
        #         r = math.sqrt((coord[x,0]--5)**2 + (coord[x,1] - 0)**2);
        #         unfold[x,0] = -5 + r;
        #         unfold[x,1] = 0;
            
        #     if coord[x,0] == 2:
        #         r = math.sqrt((coord[x,0]--5)**2 + (coord[x,1] - 0)**2);
        #         unfold[x,0] = -5 + r;
        #         unfold[x,1] = -2;
            
        #     if coord[x,0]>-4 and coord[x,0]<0:
        #         if coord[x,1]>0 and coord[x,1]<4:
        #             r = math.sqrt((coord[x,0]--5)**2 + (coord[x,1] - 0)**2);
        #             unfold[x,0] = -5 + r;
        #             unfold[x,1] = 0;
            
        #     if coord[x,0]>0 and coord[x,0]<1:
        #         if coord[x,1]>-1 and coord[x,1]<0:
        #             r = math.sqrt((coord[x,0]--5)**2 + (coord[x,1] - 0)**2);
        #             unfold[x,0] = -5 + r;
        #             unfold[x,1] = -2;
        
                    
        
        # nodeTrainUnfold = np.zeros((len(nodeLabelTrain[1]), 3))
        # for x in range(0,(len(nodeLabelTrain[1]))):
        #     nodeTrainUnfold[x,:] = unfold[nodeLabelTrain[1][x]]
        
        # nodeTrainInner = nodeTrainUnfold[np.where(nodeTrainUnfold[:,1]==0)]
        # errTrainInner = errTrain[1][np.where(nodeTrainUnfold[:,1]==0)]
        
        # xy = unfold[np.where(unfold[:,1]==0)]
        # errorYinner = errorY[np.where(unfold[:,1]==0)]
        
        # # Plot of posterior mean
        # fig = plt.figure(figsize=(8,6))
        # ax = fig.gca(projection='3d')
        # ax.scatter(xy[:,0],xy[:,2],np.zeros(286),c='k',s=1)
        # ax.plot_trisurf(xy[:,0],xy[:,2],errorYinner,color='b',label='',cmap=cm.rainbow,alpha=0.5)
        # plt.xlabel('x',fontsize=16)
        # plt.ylabel('y',fontsize=16)
        # ax.set_zlabel('error',fontsize=16)
        # plt.title('Inner surface Uy error posterior mean',fontsize=22)
        # ax.scatter(nodeTrainInner[:,0], nodeTrainInner[:,2], errTrainInner, c='r',s=10)

        # # Plot of posterior mean
        # fig = plt.figure(figsize=(8,6))
        # ax = fig.gca(projection='3d')
        # ax.scatter(xy[:,0],xy[:,2],np.zeros(286),c='k',s=1)
        # ax.plot_trisurf(xy[:,0],xy[:,2],errorYinner,color='b',label='',cmap=cm.rainbow,alpha=0.5)
        # plt.xlabel('x',fontsize=16)
        # plt.ylabel('y',fontsize=16)
        # ax.set_zlabel('error',fontsize=16)
        # plt.title('Inner surface Uy error posterior mean',fontsize=22)
        # ax.scatter(nodeTrainInner[:,0], nodeTrainInner[:,2], errTrainInner, c='r',s=10)
        # ax.view_init(90,0)

