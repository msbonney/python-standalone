import numpy as np

# Read mesh from abaqus input file
with  open('cont_2D_flat_panel.inp') as f:
    data = f.read().splitlines()
    f.close()

# Row number where the nodes and elements start in the input file     
nodeStart = data.index('*Node')
elementStart = data.index('*Element, type=CPS4')

# Read and store all the nodes
nodeLines = data[nodeStart+1:elementStart]

# Read and store all the elements
i = elementStart+1; elementLines = [];
while data[i][0] != '*':
    elementLines.append(data[i])
    i += 1

# Parse all the node information and store in an array
nodeList = [np.array(line.split(','),dtype=np.float32) for line in nodeLines]
nodeArray = np.array(nodeList)

# Parse all the element information and store in an array
elementList = [np.array(line.split(','),dtype=np.int32) for line in elementLines]
elementArray = np.array(elementList)

# Read concentrated force data
forceStart = data.index('*Cload')
forceLines = []
i = forceStart+1
while data[i][0] != '*':
    forceLines.append(data[i])
    i+=1

# Parse force data and store in an array
forceList = [line.split(',') for line in forceLines]
forceArray = np.zeros([len(forceList),3])
for i in range(len(forceList)):
    forceNode = int(forceList[i][0][9:])-1
    forceArray[i,0] = forceNode
    forceArray[i,1] = int(forceList[i][1])
    forceArray[i,2] = np.float(forceList[i][2])

np.savez('FlatPanel.npy.npz',nodes=nodeArray,elements=elementArray,force=forceArray)
