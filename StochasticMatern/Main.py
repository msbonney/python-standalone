"""
This is the main file for the study on Matern Kernel

Created on Friday 29 Mar 2021
Last Updated: 19th October 2021

Version Notes: 
GOEE & GP amended for stabiliser example
    
@author: MK
"""

import matplotlib.pyplot as plt
from matplotlib import cm
import ModelDefinitions as MD
import FiniteElementFunctions as FE
import GOEEFunctions as GOEE
import numpy as np
import GaussianProcessFunctions as GP


# Import mesh from abaqus input file
nodes,elements,coord = FE.ImportMesh()

# Solve primal problem
redK,redM,primalStrain,u,mapDofs = FE.RunPrimal(nodes,elements)


# # #### STANDARD MATERN ####

# # Define QoI and number of dual solutions
# # Note, the dual locations are set as required for the mesh example provided, if changing the mesh, be sure to change these
# QoI = 'uz'; numDual = 2

# # Calculate GOEE
# goeeUz,dualLocZ = GOEE.CalculateGOEE(nodes,elements,primalStrain,redK,u,mapDofs,QoI,numDual)

# # Provide start hyperparameters
# theta = [150,1]; nu=1.5;

# # Perform GP
# muSdz,covSdz,thetaOpt,nodeLabelTest,nodesTrain = GP.GaussianProcess('StandardMatern',dualLocZ,goeeUz,theta,nu)

# # Plot posterior (if required)
# GP.PlotStandardPosterior(muSdz,covSdz,dualLocZ,goeeUz,coord,numDual)

# # Define path of nodes along the stabiliser partition
# path = [62, 461, 460, 54, 416, 56, 16, 166, 20, 167, 21, 172, 171, 170, 169, 168, 11, 12, 114, 9, 286, 287, 288, 37]
# ii = len(path)
# standard_recon = np.zeros(ii)

# # Reconstruct the error field at the path nodes
# for x in range(0,ii):
#     # Correct for the fact that python counts from zero
#     path[x] = path[x] - 1
#     # Reconstruct the error at the node listed
#     standard_recon[x] = muSdz[path[x]]
    
# # # Set end member values of the path to the GOEE training data
# # standard_recon[0] = goeeUz[0]
# # standard_recon[ii-1] = goeeUz[1]

# # #############################################################################
  
#### STOCHASTIC MATERN ####

# Define QOI and numdual for each displacement DOF
# Note, the dual locations are set as required for the mesh example provided, if changing the mesh, be sure to change these
QoI = 'ux'; numDual = 2
goeeUx,dualLocX = GOEE.CalculateGOEE(nodes,elements,primalStrain,redK,u,mapDofs,QoI,numDual)

QoI = 'uy'; numDual = 2
goeeUy,dualLocY = GOEE.CalculateGOEE(nodes,elements,primalStrain,redK,u,mapDofs,QoI,numDual)

QoI = 'uz'; numDual = 2
goeeUz,dualLocZ = GOEE.CalculateGOEE(nodes,elements,primalStrain,redK,u,mapDofs,QoI,numDual)

# Define hpyerparameters and input datasets
theta = [1,40]; goee = [goeeUx,goeeUy,goeeUz]; dualLoc = [dualLocX,dualLocY,dualLocZ];

# Perform GP
muSt,covSt,thetaOpt,trainingDofs,testDofs = GP.GaussianProcess('StochasticMatern',dualLoc,goee,theta,redK,redM,mapDofs)

# Plot posterior (if required)
# GP.PlotStochasticPosterior(nodes,muSt,covSt,dualLoc,goee,mapDofs,coord,numDual)

# Define path of nodes along the stabiliser partition
path = [62, 461, 460, 54, 416, 56, 16, 166, 20, 167, 21, 172, 171, 170, 169, 168, 11, 12, 114, 9, 286, 287, 288, 37]

ii = len(path)
zdof = np.zeros(ii)
stochastic_recon = np.zeros(ii)

# Reconstruct the error field at the path nodes
for x in range(0,ii):
    # Correct for the fact that python counts from zero
    path[x] = path[x] - 1
    # Convert the node value to zdof (error field produced as dof set)
    zdof[x] = (path[x] *3)+2
    if x == 0:
        # Find the location of the zdof in the trainingDof array
        zz = np.where(trainingDofs == zdof[x])
    elif x == ii-1:
        # Find the location of the zdof in the trainingDof array
        zz = np.where(trainingDofs == zdof[x])
    else:
        # Find the location of the zdof in the testDof array
        zz = np.where(testDofs == zdof[x])
        
    # Reconstruct the error at the node listed
    stochastic_recon[x] = muSt[zz]
    
# # Set end member values of the path to the GOEE training data
# stochastic_recon[0] = goeeUz[0]
# stochastic_recon[ii-1] = goeeUz[1]

# #############################################################################