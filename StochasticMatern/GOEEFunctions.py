"""
This file contains the functions related to calculating GOEE

Created on Fri Feb 19 17:45:47 2021
Last Updated:
    
Version Notes: 

@author: MK
"""

import FiniteElementFunctions as FE
import matplotlib.pyplot as plt
import ModelDefinitions as MD
import scipy.sparse.linalg
import numpy as np
import scipy.io as sio



def CalculateGOEE(nodes,elements,primalStrain,redK,u,mapDofs,QoI,numDual):
    
    # ZZ recovery for primal field
    Xt,Yt,Zt,Node,Xi,Yi,Zi,E,Ele,nDOF,ind_ele,EleName = InputForZZ(nodes,elements,primalStrain)
    print('ZZ Recovery of Primal Strain')
    zzPrimalStrain = ZZ_NN3D(Xt,Yt,Zt,Node,Xi,Yi,Zi,E,Ele,nDOF,ind_ele,EleName)
    
    # Constitutive matrix
    C = MD.material.ConstitutiveMatrix(MD.dim)
    
    # Goal oriented error estimation
    # Quantity of Interest
    #QoI = 'uy';
    # Number of dual locations
    #numDual = 1
    # Randomly select dual locations
    
    #THE FOLLOWING LINE HAS BEEN COMMENTED OUT FOR THE PAPER CASE
    #dualLoc = DualLocations(len(nodes),numDual,QoI)
    
    #dualLoc = [62, 461, 460, 54, 416, 56, 16, 166, 20, 167, 21, 172, 171, 170, 169, 168, 11, 12, 114, 9, 286, 287, 288, 37]
    
    dualLoc = [62, 37]

    i = 0
    for i in range(0,2):
        dualLoc[i] = dualLoc[i]-1
        
    #dualLoc = [333]; #[3061]; #[333]
    # Create arrays with node and gaus point locations
    nodeLoc = np.array([Xt,Yt,Zt]).T
    # Initialise GOEE array
    numDofs=len(nodes)*MD.dim; i=0; GOEE=np.zeros(numDual);
    print('Computing error at dual location:')
    for loc in dualLoc:
        print(i+1)
        # Calculate generalised displacement z
        redZ = RunDual(loc,nodes,elements,Ele,redK,ind_ele,numDofs,nodeLoc,QoI,u)
        z = FE.ExpandDofs(redZ,numDofs,mapDofs)
        # Calculate dual strains
        dualStrain = FE.StressRecovery(elements,C,z)[0]
        dualE = dualStrain.reshape(len(Xi),dualStrain.shape[2])
        # ZZ recovery of dual strains
        zzDualStrain = ZZ_NN3D(Xt,Yt,Zt,Node,Xi,Yi,Zi,dualE,Ele,nDOF,ind_ele,EleName)
        # Calculate GOEE using primal and dual solutions
        GOEE[i] = CalculateError(primalStrain,zzPrimalStrain,dualStrain,zzDualStrain,C,elements,ind_ele)
        i+=1
    

    return GOEE,dualLoc


def InputForZZ(nodes,elements,strain):
    
    dim = MD.dim
    if dim==2:
        # Node numbers
        Node = range(1,len(nodes)+1); nDOF = np.array(Node);
        # x,y,z coordinates of nodes
        Xt=np.zeros([len(nodes)]); Yt=np.zeros([len(nodes)]); Zt=np.zeros([len(nodes)]);
        for i in range(len(nodes)):
            Xt[i] = nodes[i].coordinates[0]
            Yt[i] = nodes[i].coordinates[1]
            
        # Element numbers
        EleName = range(1,len(elements)+1)
        ind_ele = [];
        
        # Strains
        E = np.zeros([len(elements)*4,3])
        
        # Gauss integration point location in physical coordinates
        coordNat = np.array([1./np.sqrt(3), -1./np.sqrt(3)]);
        Xi=np.zeros([len(elements)*4]); Yi=np.zeros([len(elements)*4]); Zi=np.zeros([len(elements)*4]);
        gp = 0; Ele = np.zeros([len(elements)*4]);
        # Loop through elements
        for i in range(len(elements)):
            coord = [elements[i].node1.coordinates,
                     elements[i].node2.coordinates,
                     elements[i].node3.coordinates,
                     elements[i].node4.coordinates]
            ind_ele.append([elements[i].node1.label,
                            elements[i].node2.label,
                            elements[i].node3.label,
                            elements[i].node4.label,])
            
            # Loop through Gauss points
            gpLoc = 0;
            for k in range(2):
                for l in range(2):
                    # Gauss coordinates
                    xi = coordNat[k]; eta = coordNat[l];
                    N = FE.ShapeFunctions(xi,eta)
                    xy = np.array([coord[0][0],coord[0][1],coord[1][0],coord[1][1],
                                   coord[2][0],coord[2][1],coord[3][0],coord[3][1]])
                    coordPhy = np.dot(N,xy);
                    Xi[gp] = coordPhy[0]; Yi[gp] = coordPhy[1]; Ele[gp] = i+1;
                    E[gp] = strain[i,gpLoc,:]
                    gp+=1; gpLoc+=1;
    
    elif dim==3:
        # Node numbers
        Node = range(1,len(nodes)+1); nDOF = np.array(Node);
        # x,y,z coordinates of nodes
        Xt = MD.nodes[:,0]; Yt = MD.nodes[:,1]; Zt = MD.nodes[:,2];
        # Element numbers
        EleName = range(1,len(elements)+1)
        ind_ele = MD.elements - 1
        
        # Gauss integration point location in physical coordinates
        coordNat = np.array([1./np.sqrt(3), -1./np.sqrt(3)]);
        Xi=np.zeros([len(elements)*8]); Yi=np.zeros([len(elements)*8]); Zi=np.zeros([len(elements)*8]);
        gp = 0; Ele = np.zeros([len(elements)*8]);
        # Loop through elements
        for i in range(len(elements)):
            xyz = np.array([])
            for n in range(8):
                xyz = np.concatenate((xyz,MD.nodes[MD.elements[i,n]-1]))
            # Loop through Gauss points
            for k in range(2):
                for l in range(2):
                    for m in range(2):
                    # Gauss coordinates
                        xi = coordNat[k]; eta = coordNat[l]; zeta = coordNat[m];
                        N = FE.ShapeFunctions(xi,eta,zeta)
                        coordPhy = np.dot(N,xyz);
                        Xi[gp] = coordPhy[0]; Yi[gp] = coordPhy[1]; Zi[gp] = coordPhy[2]; 
                        Ele[gp] = i+1;
                        gp+=1;
    
    
        E = strain.reshape(len(Xi),strain.shape[2])
    
    return Xt,Yt,Zt,Node,Xi,Yi,Zi,E,Ele,nDOF,ind_ele,EleName


def DualLocations(numNodes,n,QoI):
    """
    numNodes = total number of nodes in the model
    n        = number of node selected for dual problems
    """
    
    # Reset the seed for random number generator
    np.random.seed(0)
    
    # Array with node numbers
    nodes = np.arange(numNodes)
    
    if QoI == 'ux':
        # Remove the nodes with x dof constrained
        nodesRed = np.setdiff1d(nodes,MD.xFixedNodes)
        # Sample nodes randomly from the model for dual locations
        dualLoc = np.random.choice(nodesRed,size=n,replace=False)
    elif QoI == 'uy':
        # Remove the nodes with y dof constrained
        nodesRed = np.setdiff1d(nodes,MD.yFixedNodes)
        # Sample nodes randomly from the model for dual locations
        dualLoc = np.random.choice(nodesRed,size=n,replace=False)
    elif QoI == 'udir':
        # Nodes with x and y both constrained
        xyFixed = np.intersect1d(np.array(MD.xFixedNodes),np.array(MD.yFixedNodes))
        # Remove the nodes with x and y dofs constrained
        nodesRed = np.setdiff1d(nodes,xyFixed)
        # Sample nodes randomly from the model for dual locations
        dualLoc = np.random.choice(nodesRed,size=n,replace=False)
    else:
        # Sampe nodes randomly from the model for dual locations
        dualLoc = np.random.choice(nodes,size=n,replace=False)
    
    
    return np.sort(dualLoc)


def RunDual(dualNode,nodes,elements,ele,redK,ind_ele,numDofs,nodeLoc,QoI,u):
    
    # Gaussian distribution for calculating weights
    center = np.array(nodes[dualNode].coordinates)
    #print(center)
    wHat = GaussWeightQoI(center,nodeLoc,ind_ele)
    
    # Calculate Q vector
    Q = CalculateQ(elements,wHat,ele,ind_ele,numDofs,QoI)
    
    # Apply BCs
    redQ = np.delete(Q,MD.fixedDofs)
    
    # Solve dual problem
    redZ = scipy.sparse.linalg.spsolve(redK,redQ.T)
    
    
    return redZ


def GaussWeightQoI(center,nodeLoc,ind_ele):
    
    # Initialise an array for weights
    numNodes = (nodeLoc.shape)[0]
    gwNodal = np.zeros([numNodes])
    
    # Standard deviation for normal distribution
    elSize = MD.elSize
    sig = (1./2.)*elSize
    
    # Gauss quadrature coordinates and weights
    xi,eta,zeta,wQuad = GaussQuadrature(1)
    
    # Loop through GPs and calculate weight depending on distance from center of distribution
    for i in range(numNodes):
        sqDist = np.sum((nodeLoc[i,0:MD.dim]-center)**2,axis=0)
        gwNodal[i] = np.exp(-0.5*sqDist/sig**2)
        
    numElements = len(ind_ele); numGP = len(xi);
    gw = np.zeros([numElements,numGP])
    for i in range(numElements):
        for j in range(numGP):
            N = FE.ShapeFunctions(xi[j],eta[j],zeta[j])[0,0::MD.dim]
            gw[i,j] = np.dot(N,gwNodal[ind_ele[i]].T)
            
    
    gw[abs(gw)<1e-12] = 0.
    gw = gw/np.sum(gw)
    
    
    return gw
    

def CalculateQ(elements,wHat,ele,ind_ele,numDofs,QoI):
    
    # Number of elements in model
    numElements = len(ind_ele)
    
    # Find the relevant gauss points with non zero weights for QoI
    wHatNonZero = np.argwhere(wHat)
    # Elements which have non-zero gauss weights wHat
    eleQ = set(wHatNonZero[:,0])
    
    # Gauss quadrature coordinates and weights
    xi,eta,zeta,w = GaussQuadrature(1)
    numGP = len(xi)
    
    # Loop through elements and build Q
    Q = np.zeros([1,numDofs]); Ar = np.zeros([numElements,numGP])
    for ele in eleQ:
        # Element 
        element = elements[ele]
        # Pointer matrix to identify dofs
        ehat = PointerMatrix(ind_ele[ele],numDofs)
        for j in range(numGP):
            # Jacobian
            J = FE.JacobianMatrix(element,xi[j],eta[j],zeta[j])
            detJ = np.linalg.det(J)
            # Product of Jacobian and quadrature weight
            Ar[ele,j] = detJ*w[j]
            # Relevant row of N or B depending on QoI
            NorB = SelectQoI(QoI,element,xi[j],eta[j],zeta[j])
            # Calculate Q
            Q = Q + np.dot(NorB,ehat)*Ar[ele,j]*wHat[ele,j]
        
    # Normalise 
    Q = Q/(np.sum(Ar))*len(np.nonzero(Ar)[0])
    
    return Q

def SelectQoI(qoi,element,xi,eta,zeta=0.):
    """
    This function takes in the name of the QoI and returns either the row of
    shape function matrix N or the strain displacement matrix B
    """
    if MD.dim == 2:
        if qoi=='ux':
            NorB = FE.ShapeFunctions(xi,eta,zeta)[0,:]
        elif qoi=='uy':
            NorB = FE.ShapeFunctions(xi,eta,zeta)[1,:]
        elif qoi=='strainXX':
            NorB = FE.StrainDisplacementMatrix(element,xi,eta,zeta)[0][0,:]
        elif qoi=='strainYY':
            NorB = FE.StrainDisplacementMatrix(element,xi,eta,zeta)[0][1,:]
        elif qoi=='strainXY':
            NorB = FE.StrainDisplacementMatrix(element,xi,eta,zeta)[0][2,:]
    
    elif MD.dim == 3:
        Nbar = FE.NbarDilatational(element)
        
        if qoi=='ux':
            NorB = FE.ShapeFunctions(xi,eta,zeta)[0,:]
        elif qoi=='uy':
            NorB = FE.ShapeFunctions(xi,eta,zeta)[1,:]
        elif qoi=='uz':
            NorB = FE.ShapeFunctions(xi,eta,zeta)[2,:]
        elif qoi=='strainXX':
            NorB = FE.StrainDisplacementMatrix(element,xi,eta,zeta,Nbar)[0][0,:]
        elif qoi=='strainYY':
            NorB = FE.StrainDisplacementMatrix(element,xi,eta,zeta,Nbar)[0][1,:]
        elif qoi=='strainZZ':
            NorB = FE.StrainDisplacementMatrix(element,xi,eta,zeta,Nbar)[0][2,:]
        elif qoi=='strainXY':
            NorB = FE.StrainDisplacementMatrix(element,xi,eta,zeta,Nbar)[0][3,:]
        elif qoi=='strainYZ':
            NorB = FE.StrainDisplacementMatrix(element,xi,eta,zeta,Nbar)[0][4,:]
        elif qoi=='strainXZ':
            NorB = FE.StrainDisplacementMatrix(element,xi,eta,zeta,Nbar)[0][5,:]
    
    cols = 8 if MD.dim==2 else 24;
    
    return NorB.reshape([1,cols])
    
def PointerMatrix(elementNodes,numDofs):
    
    # Initialise pointer matrix to activate dofs
    if MD.dim==2:
        ehat = np.zeros([8,numDofs])
        for i in range(4):
            node = elementNodes[i]
            dofX = node*2
            dofY = node*2+1
            ehat[i*2,dofX] = 1.0
            ehat[i*2+1,dofY] = 1.0
    
    elif MD.dim==3:
        ehat = np.zeros([24,numDofs])
        for i in range(8):
            node = elementNodes[i]
            dofX = node*3
            dofY = node*3+1
            dofZ = node*3+2
            ehat[  i*3,dofX] = 1.0
            ehat[i*3+1,dofY] = 1.0
            ehat[i*3+2,dofZ] = 1.0
    
    return ehat

def CalculateError(primalStrain,zzPrimalStrain,dualStrain,zzDualStrain,C,elements,ind_ele):
    
    # Gauss quadrature coordinates and weights
    xi,eta,zeta,w = GaussQuadrature(1)
    
    lz = MD.modelDim.lz if MD.dim==2 else 1.
    numElements = len(ind_ele); numGP = len(xi); goee = np.zeros(numElements)
    for i in range(numElements):
        for j in range(numGP):
            # Shape functions
            N = FE.ShapeFunctions(xi[j],eta[j],zeta[j])[0,0::MD.dim]
            # Jacobian
            J = FE.JacobianMatrix(elements[i],xi[j],eta[j],zeta[j])
            detJ = np.linalg.det(J)
            # Interpolation of nodal zz recovered strain values to GPs
            zzPrimalGP = np.dot(N,zzPrimalStrain[ind_ele[i]])
            zzDualGP   = np.dot(N,  zzDualStrain[ind_ele[i]])
            # Evaluation of GOEE integral
            left  = zzPrimalGP - primalStrain[i,j,:] 
            right =   zzDualGP -   dualStrain[i,j,:]
            goee[i] = goee[i] + np.dot(np.dot(left,C),right.T)*detJ*w[j]*lz
            
    
    return np.sum(goee)


def ZZ_NN3D(Xt,Yt,Zt,Node,Xi,Yi,Zi,E,Ele,nDOF,ind_ele,EleName):
    """
    This function is a generic ZZ recovery method for 1+ quantity of interest. 
    This investigates the closest integration points of the adjoined elements
    and does a length based average for those points. Only takes 1 integration
    point per element.
    
    Note: There are 2 separate naming list that must be carefully input and be 
    consistent. It is easily possible that Node is the same as nDOF (Nn==Nnode)
    
    Created by Matthew Bonney for MARQUESS GOEE - University of Nottingham
    Initially created on: 09/10/2019
    
    Edited on: 18/11/2019  by: Matthew Bonney
    Edited on: 4/12/2019  by: Matthew Bonney to include multi-E
    Edited on: 11/5/2020 by: Matthew Bonney
        Adjusted comments for publication
    =================================Input=====================================
    Xt      = Target X locations (nodes) [Nn,]
    Yt      = Target Y locations (nodes) [Nn,]
    Zt      = Target Z locations (nodes) [Nn,]
    Node    = Node numbering for locations [Nn,]
    Xi      = Integration X locations [Nint,]
    Yi      = Integration Y locations [Nint,]
    Zi      = Integration Z locations [Nint,]
    E       = Integration Values [Nint,Q]
    Ele     = Element numbering for each Integration location [Nint,]
    nDOF    = Full node numbering [Nnode,]
    ind_ele = List of list of indexes for each element: len=Nele
    EleName = Element number for each element [Nele,]
    =================================Output====================================
    ZZ      = The smoothed ZZ recovered values   
    """
    Nn,Nint,Nele=len(Xt),len(Xi),len(ind_ele)
    if np.ndim(E)==1:
        ZZ=np.zeros(Nn)
        Nshape=0
    else:
        ZZ=np.zeros((Nn,E.shape[1]))
        Nshape=E.shape[1]
    # Sweep through target locations
    for i in range(Nn):
        #print(i)
        x,y,z,n=Xt[i],Yt[i],Zt[i],Node[i]
        # Find adjunct elements
        el_list=[] 
        for j in range(Nele):
            if nDOF[ind_ele[j]].tolist().count(n): # Connected element
                el_list.append(j)
        # Find integration points in each element
        int_list=[]
        for j in range(Nint):
            for k in range(len(el_list)):
                if Ele[j]==EleName[el_list[k]]:
                    int_list.append(j)
        # Reduce arrays
        Xi_r,Yi_r,Zi_r=Xi[int_list],Yi[int_list],Zi[int_list]
        Ele_r=Ele[int_list]
        # Find closest integration point in each element
        ind_i,L_i=[],[]
        for j in range(len(el_list)):
            L,ele=np.inf,EleName[el_list[j]]
            # Find integration points in element
            a=np.where(Ele_r==ele)[0]
            # Find Length
            ind=[]
            for k in range(len(a)):
                Li=np.sqrt((Xi_r[a[k]]-x)**2+(Yi_r[a[k]]-y)**2+(Zi_r[a[k]]-z)**2)
                if Li<L:
                    L=Li
                    ind=k
            ind_i.append(a[ind])
            L_i.append(L)
        # Length average values
        if Nshape==0:
            E_r=E[int_list]
            ZZ[i]=np.sum(E_r[ind_i]*L_i)/np.sum(L_i)
        else:
            E_r=E[int_list,:]
            for k in range(E.shape[1]):
                ZZ[i,k]=np.sum(E_r[ind_i,k]*L_i)/np.sum(L_i)
    return(ZZ)
    
def GaussQuadrature(n):
    """ 
    This function returns the locations and weights for 2D Gauss quadrature.
    This uses natural coordinates where range = [-1,1]
    
    Written by Matthew Bonney for MARQUESS GOEE - University of Nottingham
    Initially created on: 24/09/2019
    
    Edited on: 11/5/2020 by: Matthew Bonney
        Adjusted comments for publication
        
    =================================Input=====================================
    n       = The integration order (only 0-2 are programmed). Future work to 
                    make this for any arbitrary order (outside current scope)
    =================================Output====================================
    xi_i    = An array of the natural 'X' locations for integration
    eta_i   = An array of the natural 'Y' locations for integration
    W       = An array of the weights for each location pair
    """
    import numpy as np
    # Switch case for first 3 orders
    if n==0: # 1 integration point
        xi_i=np.array([0])
        eta_i=np.array([0])
        W=np.array([4])
    elif n==1: # 4 integration points
        if MD.dim==2:
            xi_i   = np.sqrt(3)/3*np.array([ 1, 1,-1,-1])
            eta_i  = np.sqrt(3)/3*np.array([ 1,-1, 1,-1])
            zeta_i = [0,0,0,0]
            W=np.array([1,1,1,1])
        elif MD.dim==3:
            xi_i  =np.sqrt(3)/3*np.array([ 1, 1, 1, 1,-1,-1,-1,-1])
            eta_i =np.sqrt(3)/3*np.array([ 1, 1,-1,-1, 1, 1,-1,-1])
            zeta_i=np.sqrt(3)/3*np.array([ 1,-1, 1,-1, 1,-1, 1,-1])
            W=np.array([1,1,1,1,1,1,1,1])
    elif n==2: # 9 integration points
        xi_i=np.sqrt(.6)*np.array([-1,1,1,-1,0,1,0,-1,0])
        eta_i=np.sqrt(.6)*np.array([-1,-1,1,1,-1,0,1,0,0])
        W=25/81.*np.array([1,1,1,1,0,0,0,0,0])+40/81.*np.array([0,0,0,0,1,1,1,1,0])+64/81.*np.array([0,0,0,0,0,0,0,0,1])
    else:
        raise Exception('Order is not programmed')
        return()
    return xi_i,eta_i,zeta_i,W


def PlotErrorField(nodes,elements,primalStrain,u):
    
    goeeUxFile = np.load('GOEE/FlatPanel/GOEEux.npy.npz')
    goeeUyFile = np.load('GOEE/FlatPanel/GOEEuy.npy.npz')
    
    goeeUxRed = goeeUxFile['GOEE']
    dualLocUx = goeeUxFile['nodes']
    goeeUx = np.zeros(len(nodes))
    goeeUx[dualLocUx] = goeeUxRed
    
    goeeUyRed = goeeUyFile['GOEE']
    dualLocUy = goeeUyFile['nodes']
    goeeUy = np.zeros(len(nodes))
    goeeUy[dualLocUy] = goeeUyRed
    
    Xt,Yt,Zt,Node,Xi,Yi,Zi,E,Ele,nDOF,ind_ele,EleName = InputForZZ(nodes,elements,primalStrain)
    
    goeeUxFile.close()
    goeeUyFile.close()
    
    cm = plt.cm.get_cmap('Spectral')
    
    ux = u[0::2]; uy = u[1::2];
    
    uxLevels = np.linspace(min(ux),max(ux),num=100,endpoint=True)
    plt.figure(figsize=(8,6))
    plt.tricontourf(Xt,Yt,ux,levels=uxLevels,cmap=cm)
    cbar = plt.colorbar()
    cbar.ax.tick_params(labelsize=14) 
    plt.xlabel('x',fontsize=16)
    plt.ylabel('y',fontsize=16)
    plt.show()
#    plt.savefig('FlatPanelUx'+'.png', bbox_inches='tight',dpi=200)
    
    uyLevels = np.linspace(min(uy),max(uy),num=100,endpoint=True)
    plt.figure(figsize=(8,6))
    plt.tricontourf(Xt,Yt,uy,levels=uyLevels,cmap=cm)
    cbar = plt.colorbar()
    cbar.ax.tick_params(labelsize=14) 
    plt.xlabel('x',fontsize=16)
    plt.ylabel('y',fontsize=16)
    plt.show()
#    plt.savefig('FlatPanelUy'+'.png', bbox_inches='tight',dpi=200)
    
    
    goeeUxLevels = np.linspace(min(goeeUx),max(goeeUx),num=100,endpoint=True)
    plt.figure(figsize=(8,6))
    plt.tricontourf(Xt,Yt,goeeUx,levels=goeeUxLevels,cmap=cm)
    cbar = plt.colorbar()
    cbar.ax.tick_params(labelsize=14) 
    plt.xlabel('x',fontsize=16)
    plt.ylabel('y',fontsize=16)
    plt.show()
#    plt.savefig('FlatPanelGoeeUx'+'.png', bbox_inches='tight',dpi=200)
    
    goeeUyLevels = np.linspace(min(goeeUy),max(goeeUy),num=100,endpoint=True)
    plt.figure(figsize=(8,6))
    plt.tricontourf(Xt,Yt,goeeUy,levels=goeeUyLevels,cmap=cm)
    cbar = plt.colorbar()
    cbar.ax.tick_params(labelsize=14) 
    plt.xlabel('x',fontsize=16)
    plt.ylabel('y',fontsize=16)
    plt.show()
#    plt.savefig('FlatPanelGoeeUy'+'.png', bbox_inches='tight',dpi=200)
    
    
    x=0;
    return x

#    cm = plt.cm.get_cmap('Spectral')
#    plt.figure()
#    sc1 = plt.scatter(Xi, Yi, c=E[:,1], vmin=min(E[:,1]), vmax=max(E[:,1]), s=60, cmap=cm)
#    plt.colorbar(sc1)
#    plt.show()
#    
#    plt.figure()
#    sc2 = plt.scatter(Xt, Yt, c=zzPrimalStrain[:,1], vmin=min(zzPrimalStrain[:,1]), vmax=max(zzPrimalStrain[:,1]), s=60, cmap=cm)
#    plt.colorbar(sc2)
#    plt.show() 
#    
#    cm = plt.cm.get_cmap('Spectral')
#    plt.figure()
#    sc3 = plt.scatter(Xi, Yi, c=dualE[:,1], vmin=min(dualE[:,1]), vmax=max(dualE[:,1]), s=60, cmap=cm)
#    plt.colorbar(sc3)
#    plt.show()
#    
#    plt.figure()
#    sc4 = plt.scatter(Xt, Yt, c=zzDualStrain[:,1], vmin=min(zzDualStrain[:,1]), vmax=max(zzDualStrain[:,1]), s=60, cmap=cm)
#    plt.colorbar(sc4)
#    plt.show() 