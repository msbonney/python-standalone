"""
Global variables available across all modules

Created on 29 Mar 2021

@author: MK
"""
import numpy as np

class Material:
    
    def __init__(self,E,rho,nu):
        
        self.E   = E
        self.rho = rho
        self.nu  = nu
        
    def ConstitutiveMatrix(self,dim):
        
        E = self.E; nu = self.nu;
        if dim == 2:
            # This function creates the constitutive matrix of plane stress element
            C = np.array([[ 1, nu, 0],
                          [nu,  1, 0],
                          [ 0,  0, (1-nu)/2]])
            
            C = (E/(1-nu**2))*C
            
        else:
            # This function creates the constitutive matrix for a solid element
            C = np.array([[ 1-nu,    nu,   nu,          0,          0,          0],
                          [   nu,  1-nu,   nu,          0,          0,          0],
                          [   nu,    nu, 1-nu,          0,          0,          0],
                          [    0,     0,    0, (1-2*nu)/2,          0,          0],
                          [    0,     0,    0,          0, (1-2*nu)/2,          0],
                          [    0,     0,    0,          0,          0, (1-2*nu)/2]])
             
            C = (E/((1+nu)*(1-2*nu)))*C
        
        return C


class Geometry:
    
    def __init__(self,lx,ly,lz):
        
        self.lx = lx
        self.ly = ly
        self.lz = lz


def GetDofs(dim,nodes,component):
    
    dofs = []
    if dim == 2:
        if component == 'x':
            for node in nodes:    
                dofs.append(node*2)    
        elif  component == 'y':
            for node in nodes:    
                dofs.append(node*2+1)
        elif component == 'xy':
            for node in nodes:    
                dofs.append(node*2)
                dofs.append(node*2+1)
    elif dim == 3:
        if component == 'x':
            for node in nodes:    
                dofs.append(node*3)    
        elif  component == 'y':
            for node in nodes:    
                dofs.append(node*3+1)
        elif component == 'z':
            for node in nodes:    
                dofs.append(node*3+2)
        elif component == 'xy':
            for node in nodes:    
                dofs.append(node*3)
                dofs.append(node*3+1)
        elif component == 'xz':
            for node in nodes:    
                dofs.append(node*3)
                dofs.append(node*3+2)
        elif component == 'yz':
            for node in nodes:    
                dofs.append(node*3+1)
                dofs.append(node*3+2)
        elif component == 'xyz':
            for node in nodes:    
                dofs.append(node*3)
                dofs.append(node*3+1)
                dofs.append(node*3+2)
            
    return dofs

def ImportMesh(meshFileName):
    
    meshFile = np.load(meshFileName)
    nodeArray = meshFile['nodes']
    elementArray = meshFile['elements']
    
    nodes = nodeArray[:,1::]
    elements = elementArray[:,1::]        
    
    meshFile.close()
    
    return nodes,elements


def GetNodesByBoundingBox(dim,nodes,xMin,xMax,yMin,yMax,zMin=0,zMax=0):
    
    numNodes = (nodes.shape)[0]; nodesFound = [];
    if dim == 3:
        for i in range(numNodes):
            if xMin < nodes[i,0] < xMax:
                if yMin < nodes[i,1] < yMax:
                    if zMin < nodes[i,2] < zMax:
                        nodesFound.append(i)
    else:
        for i in range(numNodes):
            if xMin < nodes[i,0] < xMax:
                if yMin < nodes[i,1] < yMax:
                    nodesFound.append(i)
                    
    return nodesFound

# Dimensionality of the model (2-plate, 3-solid)
dim = 3

# Dimensions of the model
modelDim = Geometry(lx=0.5,ly=0.25,lz=0.001)

# Very small number
eps = 1e-3;

# Material properties
material = Material(E=69000,rho=2780,nu=0.33)

# File name for mesh data read from Abaqus
meshFileName = 'Mesh/3D_test/Stabiliser.npy.npz'
nodes,elements= ImportMesh(meshFileName)
nodesList = list(range(nodes.shape[0]))

# Element seed size
elSize = 10

# Constrained dofs for 3D stabiliser model
# xFixedNodes = [40,   41,   46,   47,  305,  306,  307,  308,  309,  310,  311,  312,  313,  314,  315,  316, 
#                317,  318,  345,  346,  347,  348,  349,  350,  351,  352,  353,  354,  355,  356,  357,  358, 
#                1352, 1353, 1354, 1355, 1356, 1357, 1358, 1359, 1382, 1383, 1384, 1385, 1386, 1387, 1388, 1389]
# yFixedNodes = xFixedNodes
# zFixedNodes = xFixedNodes
# xFixedDofs  = GetDofs(dim,xFixedNodes,'x'); 
# yFixedDofs  = GetDofs(dim,yFixedNodes,'y');
# zFixedDofs  = GetDofs(dim,zFixedNodes,'z');
# fixedDofs   = xFixedDofs + yFixedDofs + zFixedDofs;

# Constrained dofs for 3D continuum model - Solid
xFixedNodes = GetNodesByBoundingBox(dim,nodes,3.5-eps,3.5+eps,0-eps,40+eps,0-eps,90+eps)
yFixedNodes = GetNodesByBoundingBox(dim,nodes,3.5-eps,3.5+eps,0-eps,40+eps,0-eps,90+eps)
zFixedNodes = GetNodesByBoundingBox(dim,nodes,3.5-eps,3.5+eps,0-eps,40+eps,0-eps,90+eps)
xFixedDofs  = GetDofs(dim,xFixedNodes,'x'); 
yFixedDofs  = GetDofs(dim,yFixedNodes,'y');
zFixedDofs  = GetDofs(dim,zFixedNodes,'z');
fixedDofs   = xFixedDofs + yFixedDofs + zFixedDofs;

# Constrained dofs for 2D continuum model - Plate
# xFixedNodes = [0];
# yFixedNodes = list(range(51));
# zFixedNodes = []
# xFixedDofs = GetDofs(dim,xFixedNodes,'x'); yFixedDofs = GetDofs(dim,yFixedNodes,'y')
# fixedDofs = xFixedDofs + yFixedDofs;